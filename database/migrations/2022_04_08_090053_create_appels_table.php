<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appels', function (Blueprint $table) {
            $table->id();
            $table->foreignId('activity_id')->constrained('activites');
            $table->foreignId('service_id')->constrained('services');
            $table->foreignId('agent_id')->constrained('agents');
            $table->foreignId('user_id')->constrained('users');
            $table->string('url');
            $table->date('date_appel');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appels');
    }
}
