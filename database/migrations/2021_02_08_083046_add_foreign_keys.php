<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('evaluations', function (Blueprint $table) {
            $table->foreignId('agent_id')->constrained('agents');
            $table->foreignId('qualiticien_id')->constrained('qualiticiens');
        });
        Schema::table('grilles', function (Blueprint $table) {
            $table->foreignId('service_id')->constrained('services');
        });
        Schema::table('agents', function (Blueprint $table) {
            $table->foreignId('service_id')->constrained('services');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
