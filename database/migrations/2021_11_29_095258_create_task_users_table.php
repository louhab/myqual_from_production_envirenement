<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaskUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_users', function (Blueprint $table) {
            $table->id();
            $table->foreignId('task_id')->constrained('task_types');
            $table->foreignId('cat_id')->constrained('tasks');
            $table->foreignId('user_id')->constrained('users');
            $table->foreignId('actvity_id')->constrained('activites');
            $table->string('nom')->nullable();
            $table->longText('commentaire')->nullable();
            $table ->date('startDate');
            $table ->date('endDate');
           
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_users');
    }
}
