<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCostumerFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('costumer_feedback', function (Blueprint $table) {
            $table->id();
            $table->foreignId('activite_id')->constrained('activites');
            $table->foreignId('service_id')->constrained('services');
            $table->date('date_appel');
            $table->string('indice_tel');
            $table->string('nom');
            $table->string('client_id');
            $table->foreignId('qualiticien_id')->references('id')->on('users');
            $table->foreignId('agent_id')->references('id')->on('agents');
            $table->string('type_constat');
            $table->text('commentaire_constat');
            $table->text('retour_superviseur');
            $table->string('constat_url');
            $table->string('retour_sup_url');
            $table->string('file');
            $table->string('site');
            $table->foreignId('sup_id')->references('id')->on('users');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('costumer_feedback');
    }
}
