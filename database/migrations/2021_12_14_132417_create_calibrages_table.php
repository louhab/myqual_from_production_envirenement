<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalibragesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calibrages', function (Blueprint $table) {
            $table->id();
            $table->string('client_id');
            $table->string('indice_tel');
            $table->boolean('satcli')->nullable();
            $table->boolean('oad')->nullable();
            $table->char('pcc',3)->nullable();
            $table->text('exci')->nullable();
            $table->date('date_appel');
            $table->char('semaine',3);
            $table->longText('synthese')->nullable();
            $table->float('note_global')->nullable();
            $table->foreignId('qualiticien_id')->references('id')->on('users');
            $table->foreignId('agent_id')->references('id')->on('agents');
            $table->char('site',4)->nullable();
            $table->foreignId('grille_id')->constrained('grilles');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calibrages');
    }
}
