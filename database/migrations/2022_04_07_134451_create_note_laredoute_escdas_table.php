<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoteLaredouteEscdasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('note_laredoute_escdas', function (Blueprint $table) {
            $table->id();
            $table->char('note',4);
            $table->text('comment')->nullable();
            $table->foreignId('element_id')->constrained('grille_element_grille');
            $table->foreignId('laredoute_escda_id')->constrained('laredoute_escdas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('note_laredoute_escdas');
    }
}
