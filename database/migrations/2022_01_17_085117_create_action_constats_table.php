<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActionConstatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_constats', function (Blueprint $table) {
            $table->id();
            $table->foreignId('sup_id')->references('id')->on('users');
            $table->foreignId('constat_id')->references('id')->on('constats');
            $table->date('deadeline_action');
            $table->text('commentaire_action');
            $table->string('action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('action_constats');
    }
}
