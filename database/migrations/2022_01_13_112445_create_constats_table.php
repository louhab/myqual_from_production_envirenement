<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConstatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('constats', function (Blueprint $table) {
            $table->id();
            $table->foreignId('activite_id')->constrained('activites');
            $table->foreignId('service_id')->constrained('services');
            $table->date('date_appel');
            $table->string('indice_tel');
            $table->string('nom');
            $table->string('client_id');
            $table->foreignId('qualiticien_id')->references('id')->on('users');
            $table->foreignId('agent_id')->references('id')->on('agents');
            $table->string('type_constat');
            $table->string('commentaire_constat');
            $table->date('deadeline_constat');
            // $table->string('action');
            $table->boolean('statu');
            $table->foreignId('sup_id')->references('id')->on('users');
            // $table->date('deadeline_action');
            // $table->string('commentaire_action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('constats');
    }
}
