<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GrilleElementgrille extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grille_element_grille', function (Blueprint $table) {
            $table->id();
            $table->foreignId('grille_id')->constrained('grilles');
            $table->foreignId('element_grille_id')->constrained('element_grilles');
            $table->float('bareme');
            $table->text('ok')->nullable();
            $table->text('ko')->nullable();
            $table->text('si')->nullable();

         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
