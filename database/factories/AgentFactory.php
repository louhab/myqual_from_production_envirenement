<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Agent;
use Faker\Generator as Faker;

$factory->define(Agent::class, function (Faker $faker) {
    return [
        'nom'=>$faker->firstName,
        'prenom'=>$faker->lastName,
        'matricule'=>rand(10000,99999),
        'statut'=>1,
        'service_id' => rand(1,9)
    ];
});
