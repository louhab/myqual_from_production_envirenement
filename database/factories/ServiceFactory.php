<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Service::class, function (Faker $faker) {
    return [
        'nom' => $faker->name,
        'activite_id' => rand(1,4),
        'statut' => 1
    ];
});
