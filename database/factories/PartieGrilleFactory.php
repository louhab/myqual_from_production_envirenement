<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\PartieGrille;
use App\Model;
use Faker\Generator as Faker;

$factory->define(PartieGrille::class, function (Faker $faker) {
    return [
        'nom' => $faker->name,
    ];
});
