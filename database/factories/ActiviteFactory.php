<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Activite;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Activite::class, function (Faker $faker) {
    return [
        'nom' => $faker->company,
        'statut' => 1
    ];
});
