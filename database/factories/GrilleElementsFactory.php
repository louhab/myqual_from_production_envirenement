<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\ElementGrille;
use App\Model;
use Faker\Generator as Faker;

$factory->define(ElementGrille::class, function (Faker $faker) {
    return [
        'nom' => $faker->name,
        'partie_grille_id' => rand(1,4)
    ];
});
