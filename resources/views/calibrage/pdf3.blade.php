<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calibrage</title>
    <style>
            body{
        font-family: 'Helvetica Neue', Helvetica, Arial;
  font-size: 14px;
  line-height: 20px;
  font-weight: 400;
  color: #3b3b3b;
  -webkit-font-smoothing: antialiased;
  font-smoothing: antialiased;

      }
      .wrapper{
  margin: 0 auto;
  padding: 40px;
  max-width: 800px;
      }
        table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;

}
th {
  font-weight: 900;
    color: #ffffff;
    background: #2980b9;
    padding: 0;
      height: 6px;
}
td, th {
  border: 1px solid #dddddd;
  text-align: center;
  padding: 8px;
}



</style>
    </style>
</head>
<body>
    <h2 style="margin: 10px 50px 20px 30%;">Calibrage de l'évaluation :</h2>
<table class="wrapper">
  <thead>
    <tr>
      <th scope="col">La note </th>
      <th scope="col">Evaluateur</th>
      <th scope="col">Service</th>
      <th scope="col">Agent</th>
      <th scope="col">Id Client</th>

      <th scope="col">Date d'appele</th>
      <th scope="col">Date d'evaluation</th>
    </tr>
  </thead>
  <tbody>
  @foreach($calibrages as $cab)
    <tr>
      <td >{{$cab['note']}}</td>
      <td>{{$cab['qualiticien']}}</td>
      <td>{{$cab['service']}}</td>
      <td>{{$cab['agent']}}</td>
      <td>{{$cab['client_id']}}</td>
      <td>{{$cab['date_appel']}}</td>
      <td>{{$cab['date_eval']}}</td>
    </tr>
    @endforeach
  </tbody>


</table>


<div class="elements">

    <ul>
    @foreach($elements as $element)
    
@foreach($element as $elem)
   <h2>{{$elem['partie']}}</h2>
    @foreach($elem['elements'] as $el)
   
    <h4> {{$el['nom']}} :</h4>
   



  <table>
<tr>
<th>note</th>
<th>qualiticien</th>
<th>Commentaire</th>
</tr>
@foreach($notes as $note)

@foreach($note as $n)

@if( $el['id'] == $n['element_id'] )
<tr>
<td> {{$n['note']}}</td>
<td> {{ $n['qualiticien']  }}</td>
<td>{{ $n['comment']  }}</td>
</tr>
@endif
@endforeach
@endforeach
</table>

  

@endforeach
@endforeach
@endforeach

    </ul>
 
</div>
<div class="finale_calib_array">
  
<div style="margin-top:5%">

<table class="wrapper">
<h2 style="margin: 10px 50px 20px 30%;">Note retenue Calibrage :</h2>
  <th scope="col">  </th>
      <th scope="col">Evaluateur</th>
      <th scope="col">Service</th>
      <th scope="col">Agent</th>
      <th scope="col">Id Client</th>

      <th scope="col">Date d'appele</th>
      <th scope="col">Date d'evaluation</th>
    <tr>
      <td>{{$finale_calib['note']}}</td>
      <td>{{$finale_calib['qualiticien']}}</td>
      <td>{{$finale_calib['service']}}</td>
      <td>{{$finale_calib['agent']}}</td>
      <td>{{$finale_calib['client_id']}}</td>
      <td>{{$finale_calib['date_appel']}}</td>
      <td>{{$finale_calib['date_eval']}}</td>
    </tr>
  </table>
</div>

</div>
</body>
</html>