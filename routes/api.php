<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\User;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'myAuth\AuthController@login');
    Route::post('logout', 'myAuth\AuthController@logout');
});


/*
    CORE ROUTES
*/

Route::group(['prefix'=>'test'],function (){

    Route::post('grille','GrilleController@store');
    Route::post('eval','EvaluationController@store');

});
Route::group(['prefix'=>'eval'],function (){
    Route::get('create/{id}','EvaluationController@Create'); // id service
  Route::get('liste/agents/notes' , 'EvaluationController@exportAgentNotes');

});
Route::group(['prefix'=>'grille'],function (){
    // CheckAdminOrQualiticien::class
    Route::middleware(['role:admin|qualiticien|superviseur|client|superviseur|agent'])->group(function () {
      
        Route::get('deleteItem/{GridId}/{ItemId}','GrilleController@DeleteItem');
        Route::get('hasnotgrid/{id}','GrilleController@HasNotGrid'); // id service
        Route::get('getactivities','GrilleController@getAllActivitiesAndTheirServices');
        Route::get('getactivities2','GrilleController@getAllActivitiesAndTheirServices2');
        Route::get('getactivities3','GrilleController@getAllActivitiesAndTheirServices3');
        Route::get('getactivities4','GrilleController@getAllActivitiesAndTheirServices4');
        Route::get('getelements','GrilleController@getAllGridelements');
    });
    // auth:admin-api
    Route::middleware(['role:admin'])->group(function () {
        Route::get('create','GrilleController@Create');
        Route::post('store','GrilleController@store');
        Route::get('getall','GrilleController@gridsListe');
        Route::get('edit/{id}','GrilleController@editGrid');
        Route::post('update','GrilleController@updateGrid');
    });



});
Route::group(['prefix'=>'activite'],function (){
    // CheckAdminOrQualiticien::class
    Route::middleware(['role:admin|qualiticien|client|superviseur'])->group(function () {
        Route::get('/','ActiviteController@index');
        Route::get('edit','ActiviteController@edit');
        Route::get('/all','ActiviteController@allactivities');
        Route::get('/{id}','ActiviteController@getServices');
        
    });
    // auth:admin-api
    Route::middleware(['role:admin'])->group(function () {
        Route::post('store','ActiviteController@store');
        Route::get('/toggle/{id}','ActiviteController@toggleStatus');
    });

});
/* refactor in here and in front too */
//auth:admin-api
Route::group(['prefix'=>'service','middleware' => ['role:admin']],function (){
    Route::get('/{id}','ServiceController@getAgents');
    Route::post('store','ServiceController@store');
   
});


Route::get('/exportServices','ServiceController@exportServices')->middleware(['role:admin']); // auth:admin-api


Route::get('tache/export','TaskUserController@exportTasks')->middleware(['role:admin']);
// auth:admin-api
Route::group(['prefix'=>'elements','middleware' => ['role:admin']],function (){
    Route::post('addelement','ElementGrilleController@addElementToSection');
    Route::post('addsection','ElementGrilleController@addSection');
    Route::post('editelement','ElementGrilleController@addeditElement');
});

Route::group(['prefix'=>'agents'],function (){
    // CheckAdminOrQualiticien::class
//    Route::middleware(['role:admin|qualiticien|superviseur|client|superviseur|agent'])->group(function () {
        Route::get('getall','AgentController@getAllAgents');
  // });

});
//Route::get('eval/{id}','EvaluationController@consultEval');
Route::group(['prefix'=>'eval'],function (){

    // CheckAdminOrQualiticien::clas
    Route::middleware(['role:admin|qualiticien|client|superviseur|agent'])->group(function () {
        Route::get('/grid/{id}','EvaluationController@create');// service id
        Route::post('/save','EvaluationController@store');

        Route::post('calib/delete','CalibrageController@deleteCalib');
        Route::get('calib/consolidation','CalibrageController@consolidation');
        Route::post('/calib/save','CalibrageController@store');
        Route::post('/calib/save2','CalibrageController@store2');
        Route::get('/liste','EvaluationController@getAllEvals');
        Route::get('/calib/liste','CalibrageController@getAllCalib');
        Route::get('/calib/liste/extraire','CalibrageController@extraire');
        Route::get('/calib/mycalib','CalibrageController@getMycalib');
        Route::get('/calib/{id}','CalibrageController@getcalib');
        Route::get('/calib/liste/print/{id}','CalibrageController@getCalibPdf');
        Route::get('/myliste','EvaluationController@getMyEvals'); // return evals added by the user
        Route::get('/{id}','EvaluationController@consultEval');
        Route::get('/calib2/{id}','CalibrageController@consultEval');
        Route::post('/calib/edititem','CalibrageController@editItem');
        Route::get('/liste/extraire','EvaluationController@extraire');
        Route::post('/calib/delete2','CalibrageController@deleteCalib2');
    });

    // auth:admin-api
    Route::middleware(['role:admin|qualiticien'])->group(function () {
        //Route::get('/liste','EvaluationController@getAllEvals');
        //Route::get('/{id}','EvaluationController@consultEval');
        Route::post('/editheader','EvaluationController@editHeader');
        Route::post('/editfooter','EvaluationController@editFooter');
        Route::post('edititem','EvaluationController@editItem');
        Route::post('/delete','EvaluationController@deleteEval');
    });
});

// CheckAdminOrQualiticien::class
Route::group(['prefix'=>'reporting','middleware' => ['role:admin|qualiticien|client|superviseur']],function (){

    Route::get('/home','ReportingController@home');
    Route::get('/global','ReportingController@reportingGlobal');
    
    Route::get('/constat/global','ReportingController@reportingGlobalConsta');
    Route::get('/activity','ReportingController@reportingActivity');
    Route::get('/item','ReportingController@reportingItem');
    Route::get('/item2','ReportingController@reportingItem2');
    
    Route::get('/constat/activity','ReportingController@reportingConstatActivity');
    Route::get('/service','ReportingController@reportingService');
 
    
    Route::get('/constat/service','ReportingController@reportingConstatService');
    Route::get('/agent','ReportingController@reportingTeleconseiller');
    Route::get('constat/agent','ReportingController@reportingConstatTeleconseiller');
    // par bloc new :
    Route::get('/bloc/new','ReportingController@ByBlocNew');
});
Route::group(['prefix'=>'coustumer','middleware' => ['role:admin|qualiticien|client|superviseur']],function (){
    Route::post('/feedback/save', 'CostumerFeedbackController@store');
    
    Route::get('/feedback/myliste','CostumerFeedbackController@myliste');

    Route::get('/feedback/getedetails/{id}','CostumerFeedbackController@show');

});

// auth:admin-api

Route::group(['prefix'=>'staff','middleware' => ['role:admin|qualiticien|client|superviseur']],function (){
    Route::post('/add','StaffController@addStaff');

    Route::get('/agents','StaffController@getAgents');
    Route::get('/qualiticiens','StaffController@getQualiticiens');
    Route::get('/admins','StaffController@getAdmins');
    Route::get('/clients','StaffController@getClients');
    Route::get('/sups','StaffController@getSuperviseurs');
    

    Route::get('/sups2','StaffController@getSupActvitvites');
    Route::get('/agent/{id}','StaffController@getAgent');
    Route::get('/qualiticien/{id}','StaffController@getQualiticien');
    Route::get('/admin/{id}','StaffController@getAdmin');
    Route::get('/user/{id}','StaffController@getUser');
    Route::get('/roles','StaffController@getRole');

    Route::post('editagent','StaffController@updateAgent');
    Route::post('edituser','StaffController@updateUser');
    Route::post('importagents','StaffController@massAgentImport');

    Route::get('exceltemplate','StaffController@excelTemplate');

    //delete agent
    Route::post('deleteagent','AgentController@delete');

    //disable agent
    Route::post('disableagent','AgentController@disable');

    //disable user
    Route::post('disableuser','StaffController@disableUser');

    //export Agents
    Route::get('agentsexport','AgentController@exportAsCsv');
});

Route::group(['prefix'=>'resource','middleware' => ['role:admin']], function () {
    Route::post('uploadresource','ResourceController@store');
    Route::get('delete/{id}','ResourceController@destroy');
    Route::get('details/{id}','ResourceController@show');
});

Route::group(['prefix'=>'resource'],function (){
    Route::middleware(['role:admin|qualiticien|client|superviseur'])->group(function () {
        Route::get('details/{id}','ResourceController@show');
        Route::get('getall','ResourceController@index');
    });
    Route::middleware(['role:admin'])->group(function () {
        Route::post('uploadresource','ResourceController@store');
        Route::get('delete/{id}','ResourceController@destroy');
    });
});
Route::group(['prefix'=>'tache'],function (){
    Route::middleware(['role:admin|qualiticien|'])->group(function () {
        Route::post('/add','TaskUserController@store');
     
       
        
        Route::get('/get/{id}','TaskUserController@show');
        
        Route::get('/delete/{id}','TaskUserController@delete');
        
        Route::get('/upadet/{id}','TaskUserController@update');
        
       
    });
  ;
});
Route::post('/tache/current/get_tasks','TaskUserController@get_tache')->middleware('api');

Route::group(['prefix'=>'tache'],function (){
    Route::middleware(['role:admin|qualiticien'])->group(function () {
       
        Route::get('get_tasks_type','TaskTypeController@index')->middleware('api');
        Route::get('reporting/tasks/activity','TaskUserController@reporting')->middleware('api');
        
       
    });
  ;
});
Route::get('eval/liste/agent','AgentEvalController@myeval_agent')->middleware('api');
Route::get('test', 'RolesTest@createPermissionsAndRoles');
Route::get('getqualiticiens_endpoint','QualiticienController@index')->middleware('api');

Route::get('getQualiticiens','TaskUserController@getQualiticiens')->middleware('api');
Route::post('getreporting/evaluateur','ReportingController@getreporting_evaluateur');
Route::post('getreporting/calibrage/evaluateur','ReportingController@getreporting_evaluateur_calibrage')->middleware('api');


Route::get('getqualiticiens_endpoint_client','StaffController@getclients_user')->middleware('api');
Route::get('myself','StaffController@get_current_user')->middleware('api');
Route::get('createPermissionsAndRoles', 'RolesTest@createPermissionsAndRoles');
Route::get('get_tasks','TaskController@index')->middleware('api');
Route::get('tache/get_task/user','TaskUserController@get_task')->middleware('api');

Route::get('tache/get_tasks/users','TaskUserController@get_tasks_users');


Route::post('tache/update/{id}','TaskUserController@edit');
Route::get('tache/get_task/{id}','TaskUserController@show');

Route::group(['prefix'=>'calibrage','middleware' => ['api']],function (){
       
        Route::get('evaluations','CalibrageController@index');
        
        
       
    });
Route::get('hash',function(){
    return 'oki';
});

Route::group(['prefix'=>'contstat','middleware' => ['role:admin|qualiticien|client|superviseur|agent']],function (){
     Route::get('getedetails/{id}','ConstatController@getconstat');
    Route::post('/save','ConstatController@store');
    Route::get('myliste' ,'ConstatController@mylist');
    Route::get('myliste/copie' ,'ConstatController@mylistCopie');
    Route::get('liste','ConstatController@liste');
    Route::get('/liste/sup','ConstatController@listeSup');

    Route::post('/delete','ConstatController@delete');
    Route::get('count/notification/user','ConstatController@count');
    Route::get('/delete/notification/user','ConstatController@deleteNotification');
    
});
Route::group(['prefix'=>'contstat/action','middleware' => ['role:admin|qualiticien|client|superviseur']],function (){

   Route::post('/save','ActionConstatController@store');
   
});
Route::post('/service2/edit2','ServiceController@edit');
Route::post('mail/send', 'MailController@send');
Route::get('getCountVisiteUsers','UserVisiteController@index');
Route::get('/assingRole','RolesTest@assingRole');

// save 

Route::post('eval/save/escda', 'EscdaController@store' );
Route::post('eval/save/eluescda', 'EluEscdaController@store' );
Route::post('eval/save/laredouteescda', 'LaredouteEscdaController@store' );
// myliste 
Route::get('eval/escda/myliste','EscdaController@myliste');
Route::get('eval/eluescda/myliste','EluEscdaController@myliste');
Route::get('eval/laredouteescda/myliste','LaredouteEscdaController@myliste');
// liste 
Route::get('eval/escda/liste', 'EscdaController@getAllEvals'); //cds
Route::get('eval/eluescda/liste', 'EluEscdaController@getAllEvals'); // cyclocity 
Route::get('eval/laredouteescda/liste', 'LaredouteEscdaController@getAllEvals'); // la redout
// details 
Route::get('/eval/escda/{id}','EscdaController@getEval');
Route::get('/eval/eluescda/{id}','EluEscdaController@getEval');
Route::get('/eval/laredouteescda/{id}','LaredouteEscdaController@getEval');
// delete 
Route::post('eval/escda/delete','EscdaController@delete');
Route::post('eval/eluescda/delete','EluEscdaController@delete');
Route::post('eval/laredouteescda/delete','LaredouteEscdaController@delete');

Route::get('agents/getall/agentEscda', 'AgentController@escdaAgent');



// assigne agent to users 

Route::get('getAllAgent/{id}','StaffController@getAllAgent');

Route::get('constat/liste/extraire','ConstatController@extraire');
Route::get('constat_user/liste/extraire','ConstatController@extraire_user');

Route::group(['prefix'=>'appel','middleware' => ['role:admin|qualiticien|superviseur|client']],function (){
  Route::post('save','AppelController@store');
  Route::get('liste', 'AppelController@index');

 Route::get('detail/{id}', 'AppelController@show');
 Route::post('delete', 'AppelController@delete');
});

Route::group(['prefix'=>'motifs'],function (){
    Route::get('/allmotifs','MotifController@index');

  });


// Route::get('agents/getall/agentEscda2', function(){
//     return 'oki';
// });
Route::get('hash','StaffController@hash');
Route::get('sabrine',function(){
     $user=User::find(52);
     $role = Role::where('name', 'superviseur' )->first();
     $user->assignRole($role);

     $user->save();
});
Route::get('motifAlpique/allmotifs','MotifAlpiqueController@index');