<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\CheckAdminOrQualiticien;
use App\Http\Middleware\MyAuth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('{path}', 'ApplicationController@index')->where('path', '^(.+)?$');
//Route::get('pdf','ApplicationController@pdf');
/*
Route::group(['prefix'=>'test'],function (){

    Route::get('/hello',function(){
        return json_encode('hello');
    });
    Route::post('grille','GrilleController@store');
    Route::post('eval','EvaluationController@store');

});
Route::group(['prefix'=>'eval'],function (){
    Route::get('create/{id}','EvaluationController@Create'); // id service

});
Route::group(['prefix'=>'grille'],function (){

    Route::middleware([CheckAdminOrQualiticien::class])->group(function () {
        Route::get('hasnotgrid/{id}','GrilleController@HasNotGrid'); // id service
        Route::get('getactivities','GrilleController@getAllActivitiesAndTheirServices');
        Route::get('getelements','GrilleController@getAllGridelements');
    });

    Route::middleware(['auth:admin-api'])->group(function () {
        Route::get('create','GrilleController@Create');
        Route::post('store','GrilleController@store');
    });


});
Route::group(['prefix'=>'activite','middleware' => ['auth:admin-api']],function (){
    Route::get('/','ActiviteController@index');
    Route::get('/all','ActiviteController@allactivities');
    Route::get('/{id}','ActiviteController@getServices');
    Route::get('/toggle/{id}','ActiviteController@toggleStatus');

    Route::post('store','ActiviteController@store');
});

Route::group(['prefix'=>'service','middleware' => ['auth:admin-api']],function (){

    Route::get('/{id}','ServiceController@getAgents');
    Route::post('store','ServiceController@store');
});

Route::group(['prefix'=>'elements','middleware' => ['auth:admin-api']],function (){
    Route::post('addelement','ElementGrilleController@addElementToSection');
    Route::post('addsection','ElementGrilleController@addSection');
    Route::post('editelement','ElementGrilleController@addeditElement');
});

Route::group(['prefix'=>'agents'],function (){
    Route::get('getall','AgentController@getAllAgents')->middleware([CheckAdminOrQualiticien::class]);
});

Route::group(['prefix'=>'eval'],function (){

    Route::middleware([CheckAdminOrQualiticien::class])->group(function () {
        Route::get('/grid/{id}','EvaluationController@create');// service id
        Route::post('/save','EvaluationController@store');
    });
    Route::middleware(['auth:admin-api'])->group(function () {
        Route::get('/liste','EvaluationController@getAllEvals');
        Route::get('/{id}','EvaluationController@consultEval');
        Route::post('/editheader','EvaluationController@editHeader');
        Route::post('/editfooter','EvaluationController@editFooter');
        Route::post('edititem','EvaluationController@editItem');
        Route::post('/delete','EvaluationController@deleteEval');
    });
});


Route::group(['prefix'=>'reporting','middleware' => ['auth:admin-api']],function (){
    Route::get('/home','ReportingController@home');
    Route::get('/global','ReportingController@reportingGlobal');
    Route::get('/activity','ReportingController@reportingActivity');
    Route::get('/service','ReportingController@reportingService');
    Route::get('/agent','ReportingController@reportingTeleconseiller');
});

Route::group(['prefix'=>'staff'],function (){
    Route::post('/add','StaffController@addStaff');

    Route::get('/agents','StaffController@getAgents');
    Route::get('/qualiticiens','StaffController@getQualiticiens');
    Route::get('/admins','StaffController@getAdmins');

    Route::get('/agent/{id}','StaffController@getAgent');
    Route::get('/qualiticien/{id}','StaffController@getQualiticien');
    Route::get('/admin/{id}','StaffController@getAdmin');

    Route::post('editagent','StaffController@updateAgent');
    Route::post('edituser','StaffController@updateQualiticienAdmin');
});
*/
Route::get('grille/modifier/api/delete/{GridId}/{ItemId}','GrilleController@DeleteItem');

