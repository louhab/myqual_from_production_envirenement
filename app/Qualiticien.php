<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Qualiticien  extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $attributes = [
        'statut' => "1",
    ];

    protected $fillable = [
        'nom', 'prenom', 'telephone', 'matricule', 'email', 'password', 'statut',
    ];

    protected $hidden = [
        'password',
    ];

    public function evaluations()
    {
        return $this->hasMany('App\Evaluation');
    }

    public function calibrages()
    {
        return $this->belongsTo('App\Calibrage');
    }
    public function constats()
    {
        return $this->hasMany('App\Constat');
    }
}
