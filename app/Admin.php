<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Admin extends Authenticatable
{
    use HasApiTokens, Notifiable;
    //

    protected $fillable = [
        'nom', 'prenom', 'telephone', 'matricule', 'email', 'password', 'statut',
    ];

    protected $hidden = [
        'password',
    ];
    protected $attributes = [
        'statut' => "1",
    ];
}
