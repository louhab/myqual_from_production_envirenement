<?php

namespace App\Exports;
use App\Constat ;
use App\ActionConstat;
use App\User ;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromArray;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithEvents;
use Carbon\Carbon;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\FromCollection;

class ConstatExport implements FromArray,WithHeadings,WithStyles,WithEvents
{
    public function headings(): array
{
    return [
            'Activite',
            'service',
            'QUALITICIEN',
            'TÉLÉCONSEILLER',
            'SUP',
            'TYPE',
            'Commentaire',
            'Commentaire sup',
            'STATUT',
            'Site',
            'ID CLIENT',
            'Date constat'
    ];
}

public function array(): array
{
    

           $constats = Constat::all();
        $data = [];
        foreach($constats as $constat){
            $data[] = [
                $constat->activite->nom,
                $constat->service->nom,
                $constat->user->nom. ' '.$constat->user->prenom,
                $constat->agent->nom .''.$constat->agent->prenom,
                User::find($constat->sup_id)->nom.' '.User::find($constat->sup_id)->prenom,
                $constat->type_constat,
                $constat->commentaire_constat,
                (ActionConstat::where('constat_id',$constat->id)->first())? ActionConstat::where('constat_id',$constat->id)->first()->commentaire_action : '',
                ($constat->statu == 1) ? 'Traité' : 'Non Traité',
                $constat->site,
                $constat->client_id,

                Carbon::createFromFormat('Y-m-d H:i:s', $constat->created_at)->format('d/m/Y')
            ];
        }

   return $data;
}

public function styles(Worksheet $sheet)
{
    
    return [
        // Style the first row as bold text.
        1    => ['font' => ['bold' => true,'color' => ['argb' => 'ffffff']],
       
        ],
        
    ];
    
}
public function registerEvents(): array
{
return [

    AfterSheet::class    => function(AfterSheet $event) {

        $event->sheet->getColumnDimension('A')->setWidth(40);
        $event->sheet->getColumnDimension('B')->setWidth(40);
        $event->sheet->getColumnDimension('C')->setWidth(40);
        $event->sheet->getColumnDimension('D')->setWidth(40);
        $event->sheet->getColumnDimension('E')->setWidth(40);
        $event->sheet->getColumnDimension('F')->setWidth(40);
        $event->sheet->getColumnDimension('G')->setWidth(40);
        $event->sheet->getColumnDimension('H')->setWidth(40);
    }
];
}
}
