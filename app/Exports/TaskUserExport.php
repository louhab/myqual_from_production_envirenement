<?php

namespace App\Exports;

use App\Task_user;
use App\Task;
use App\User;
use App\Task_type;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class TaskUserExport implements  FromArray, WithHeadings, ShouldAutoSize, WithStyles
{
    public function array(): array
    {
        $tasks = Task_user::all();
        $data = [];
        foreach($tasks as $task_user) {
            $data[] =[
                'id'=>$task_user->id,
                'catgorie'=>Task::find($task_user->cat_id)->nom,
'tache'=>
$task_user->nom != '' ?  Task_type::find($task_user->task_id)->nom .'('. $task_user->nom .')' : Task_type::find($task_user->task_id)->nom ,
                'date'=>$task_user->endDate ,
                'endDate'=>$task_user->startDate,

'user'=>User::find($task_user->user_id)->nom . ' '. User::find($task_user->user_id)->prenom
            ];
        }
        return $data;
    } 
    public function headings(): array
    {
        return [
            'id',
            'CATEGORIE',
            'TACHE',
            'DATE DE DÉBUT LA TACHE',
            'DATE DE FIN LA TACHE',
            'QUALITICIEN',
        ];
    }
    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true,'color' => ['argb' => 'ffffff']],
                'fill' => [
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'color' => ['argb' => '3471eb'],
                ],],
        ];
    }
}
