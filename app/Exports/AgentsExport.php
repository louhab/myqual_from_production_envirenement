<?php

namespace App\Exports;

use App\Agent;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class AgentsExport implements FromArray, WithHeadings, ShouldAutoSize, WithStyles
{
    public function array(): array
    {
        $agents = Agent::all('nom','prenom','matricule','statut','service_id')->sortBy('service_id');
        $data = [];
        foreach ( $agents as $agent){
            $data[] = [
                $agent->matricule,
                $agent->nom,
                $agent->prenom,
                $agent->service->activite->nom,
                $agent->service->nom,
                
                ($agent->statut == 1) ? 'activé' : 'désactivé',
            ];
        }
       return $data;
    }
    public function headings(): array
    {
        return [
            'Matricule',
            'Nom',
            'Prénom',
            'Activité',
            'Service',
            'Statut',
        ];
    }
    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true,'color' => ['argb' => 'ffffff']],
                'fill' => [
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'color' => ['argb' => '3471eb'],
                ],],
        ];
    }
}
