<?php

namespace App\Exports;

use App\Service;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ServicesExport implements FromCollection, WithHeadings
{

    public function headings(): array
    {
        return [
            'Id',
            'Service',
            'Activite',
        ];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $services = Service::all();
        $data = [];
        foreach($services as $s){
            $data[] = [$s->id,$s->nom,$s->activite->nom];
        }
        return collect($data);
    }
}
