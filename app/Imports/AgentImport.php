<?php

namespace App\Imports;

use App\Agent;
use App\Service;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class AgentImport implements ToModel, WithHeadingRow
{

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {   // validate row
        if(isset($row['nom'])&&isset($row['prenom'])&&isset($row['email'])&&isset($row['telephone'])&&isset($row['matricule'])&&isset($row['date_dintegration'])&&isset($row['site'])&&isset($row['id_service'])){
            // validate data
            $dataValidated = true;

            // validate date
            if(!is_int($row['date_dintegration'])) $dataValidated = false;

            // validate site
            if (!in_array($row['site'], ['TE','TA','HO'])) $dataValidated = false;

            // validate service id
            $serviceIds = Service::All('id');
            if (!$serviceIds->contains('id', $row['id_service'])) $dataValidated = false;

            //check if email exist
            if(Agent::where('email', $row['email'])->exists()) $dataValidated = false;

            //check if email exist
            if(Agent::where('matricule', $row['matricule'])->exists()) $dataValidated = false;

            if($dataValidated) {
                return  new Agent([
                    'nom'=> $row['nom'],
                    'prenom'=> $row['prenom'],
                    'email'=> $row['email'],
                    'telephone'=> $row['telephone'],
                    'matricule'=> $row['matricule'],
                    'dateInte'=> date("Y-m-d",($row['date_dintegration']-25569)*86400),
                    'site'=> $row['site'],
                    'service_id' => $row['id_service'],
                    'password' => bcrypt($row['nom']),
                ]);
            } else {
                return null;
            }

        }

    }
}
