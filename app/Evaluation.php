<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    //
    protected $dateFormat = 'Y-m-d';

    public function grille()
    {
        return $this->belongsTo('App\Grille');
    }

    public function notes()
    {
        return $this->hasMany('App\Note');
    }

    public function agent()
    {
        return $this->belongsTo('App\Agent');
    }
    public function qualiticien()
    {
        return $this->belongsTo('App\User');
    }
}
