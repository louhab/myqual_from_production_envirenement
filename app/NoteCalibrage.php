<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoteCalibrage extends Model
{
    public function calibrages()
    {
        return $this->belongsTo('App\Calibrage');
    }
}
