<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appel extends Model
{
    public function activite()
    {
        return $this->belongsTo('App\Activite','activity_id');
    }
    public function service()
    {
        return $this->belongsTo('App\Service');
    }
    public function agent()
    {
        return $this->belongsTo('App\Agent');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
