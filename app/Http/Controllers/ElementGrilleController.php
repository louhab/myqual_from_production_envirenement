<?php

namespace App\Http\Controllers;

use App\ElementGrille;
use App\PartieGrille;
use Illuminate\Http\Request;

class ElementGrilleController extends Controller
{
    public function addElementToSection(Request $req){
        $element_name = $req->input('newelement');
        $section_id = $req->input('partieid');
        if($element_name){
            $element = new ElementGrille;
            $element->nom = $element_name;
            $element->partie_grille_id = $section_id;
            $element->save();
            return $element->id;
        }
        return 0;

    }
    public function addSection(Request $req){
        $section_name = $req->input('newsection');
        if($section_name){
            $section = new PartieGrille();
            $section->nom = $section_name;
            $section->save();
            return $section->id;
        }
        return 0;

    }
    public function addeditElement(Request $req){
        $elementtoupdate = $req->input('itemToEdit');
        if($elementtoupdate ['nom']){
            $element = ElementGrille::find($elementtoupdate ['id']);
            $element->nom = $elementtoupdate ['nom'];
            $element->save();
            return 1;
        }
        return 0;
    }
}
