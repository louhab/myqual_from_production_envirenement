<?php

namespace App\Http\Controllers;

use App\Constat;
use App\Agent;
use App\User;
use App\ConstatFile;
use App\Activite;
use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\MessageGoogle;
use App\Mail\MessageGoogleCopie;
use App\ConstatQualCopie;
use App\ConstatSupCopie;
use App\ActionConstat;
use DateTime;
use Storage;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ConstatExport;
use App\Exports\ConstatUsreExport;
use Carbon\Carbon;
use App\Notifications\NewConstat;



class ConstatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 

     

      
    $constat = new Constat();
    $constat->activite_id =$request->activiteId;
    $constat->service_id =$request->serviceId;
    $constat->date_appel = $request->date;
    $constat->indice_tel= $request->indice;
    $constat->client_id= $request->clientId;
    $constat->nom= $request->nom;
    $constat->qualiticien_id = $request->qualiticien;
    $constat->type_constat = $request->constattype;
    $constat->commentaire_constat = $request->commentaireConstat;
    $constat->site = json_decode($request->site)->site ;
    if($request->deadlineConstat =='1 Jour') {
        $constat->deadeline_constat= date("Y-m-d", strtotime("+1 day"));
    }
    if($request->deadlineConstat =='1 Jour') {
        $constat->deadeline_constat= date("Y-m-d", strtotime("+1 day"));
    }
    if($request->deadlineConstat =='2 Jours') {
        $constat->deadeline_constat= date("Y-m-d", strtotime("+2 day"));
    }
    if($request->deadlineConstat =='3 Jours') {
        $constat->deadeline_constat= date("Y-m-d", strtotime("+3 day"));
    }
    if($request->deadlineConstat =='1 Semaine') {
        $constat->deadeline_constat= date("Y-m-d", strtotime("+1 week"));
    }
    if($request->deadlineConstat =='2 Semaines') {
        $constat->deadeline_constat = date("Y-m-d", strtotime("+2 week"));
    }
    if($request->deadlineConstat =='3 Semaines') {
        $constat->deadeline_constat= date("Y-m-d", strtotime("+3 week")); 
    }
    $constat->agent_id= $request->agentId;
    $constat->sup_id =json_decode($request->sup)->id;
    $constat->statu = 0;
    $constat->time_send_consta =  date("Y-m-d H:i:s");
    $constat->save();
    
  

    foreach(json_decode($request->qualiticiensC) as $qualC){
        $constatQualCopie = new ConstatQualCopie();
        $constatQualCopie->constat_id = $constat->id;
        $constatQualCopie->qual_id = $qualC->id;
        $constatQualCopie->save();
    }
   if($request->file !== null){
    $constafile= new ConstatFile();
    $fileName =$request->nom.'_'.time().'.'.$request->file->extension();
    $path = $request->file->storeAs('uploads/conststa/files', $fileName, 'public');
    $constafile->constat_id =$constat->id;
    $constafile->url = $path;
    $constafile->save();
   }
 
  $niveauDimportance = json_decode($request->status)->nom;
    $user= User::find(json_decode($request->sup)->id);

    $user->notify(new NewConstat($constat));
    $data[]=[
     'agent'=>Agent::find($request->agentId)->nom . ' '.Agent::find($request->agentId)->prenom,
     'deadlineConstat'=>$request->deadlineConstat,
     'type'=>$request->constattype,
     'niveauDimportance'=>$niveauDimportance,
        ];
            Mail::to($user->email)->bcc("rkabtour@myopla.com")->queue(new MessageGoogle($data));

            foreach(json_decode($request->supC) as $suC){
                $constatSupCopie = new ConstatSupCopie();
                $constatSupCopie->constat_id = $constat->id;
                $constatSupCopie->sup_id =$suC->id;
                
                $supCopie = User::find($suC->id);
                Mail::to($supCopie->email)->bcc("rkabtour@myopla.com")->queue(new MessageGoogleCopie($data));
                $constatSupCopie->save();
                }

   }

    /**
     * Display the specified resource.
     *
     * @param  \App\Constat  $constat
     * @return \Illuminate\Http\Response
     */
    public function mylist(Request $req)
    {


        $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
        if(isset($req->input('dateRange')[1])) {
            $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
        } else {
            $endDate = $startDate;
        }
        $user = auth('api')->user();

        //  $endDate = date('Y-m-d ', strtotime($startDate. ' +1 day'));
        $constats = Constat::where('qualiticien_id',$user->id)->get();
        if($constats->count()==0) return 0;
                foreach($constats as $constat){
        $data[]=[
            'id'=>$constat->id,
            'activite'=>$constat->activite->nom,
            'agent'=>$constat->agent->nom .''.$constat->agent->prenom,
            'statut'=>$constat->statu,
            'date'=>Carbon::createFromFormat('Y-m-d H:i:s', $constat->created_at)->format('d/m/Y'),
            'client_id'=>$constat->client_id
        ];
                }
        return $data;
    }
    public function listeSup(Request $req) {
       
        $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
        if(isset($req->input('dateRange')[1])) {
            $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
        } else {
            $endDate = $startDate;
        }
        $user = auth('api')->user();

        $constats = Constat::where('sup_id',$user->id)->get();
        if($constats->count()==0) return 0;
                foreach($constats as $constat){
        $data[]=[
            'id'=>$constat->id,
            'activite'=>$constat->activite->nom,
            'agent'=>$constat->agent->nom .''.$constat->agent->prenom,
            'statut'=>$constat->statu,
            'client_id'=>$constat->client_id
        ];
        }
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Constat  $constat
     * @return \Illuminate\Http\Response
     */
    public function edit(Constat $constat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Constat  $constat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Constat $constat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Constat  $constat
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {

        $constaQualCopie = ConstatQualCopie::where('constat_id',$request->id)->delete();
        $constaSupCopie = ConstatSupCopie::where('constat_id',$request->id)->delete();
        $constafile = ConstatFile::where('constat_id',$request->id)->delete();
        $constat = Constat::find($request->id)->delete();

    }
    public function getconstat( $id){
        if(ConstatFile::where('constat_id',$id)->first()){
            $constafile = ConstatFile::where('constat_id',$id)->first();
            $file= Storage::disk('public')->get($constafile->url);
     
            $file = base64_encode($file);
            $constat = Constat::find($id);
            if($constat->statu===1){
                $constatAction = ActionConstat::where('constat_id',$id)->first();
                $data=[
                    'constat_activite'=>$constat->activite,
                    'service'=>Service::find($constat->service_id),
                    'statut'=>$constat->statu,
                    'sup'=>User::find($constat->sup_id),
                    'date_appel'=>$constat->date_appel,
                    'client_id'=>$constat->client_id,
                    'indice_tel'=>$constat->indice_tel,
                    'nom'=>$constat->nom,
                    'agent'=>Agent::find($constat->agent_id),
                    'qualiticien'=>User::find($constat->qualiticien_id),
                    'commentaire_constat'=>$constat->commentaire_constat,
                    'type_constat'=>$constat->type_constat,
                    'deadeline_constat'=>$constat->deadeline_constat,
                    'file'=>$file,
                    'avis'=>$constatAction->avis,
                    'url_file'=>asset(\Storage::url($constafile->url)),
                    'action'=>$constatAction->action,
                    'commentaire_action'=>$constatAction->commentaire_action,
                    'deadeline_action'=>$constatAction->deadeline_action,
                    'date_time_action'=>$constatAction->time_action_constat,
                    'date_time_constat'=>$constat->time_send_consta,
                    'site'=>$constat->site,
                    
                ];
                return $data;
            }

            $data=[
                 'constat_activite'=>$constat->activite,
                 'service'=>Service::find($constat->service_id),
                 'statut'=>$constat->statu,
                 'sup'=>User::find($constat->sup_id),
                 'date_appel'=>$constat->date_appel,
                 'client_id'=>$constat->client_id,
                 'indice_tel'=>$constat->indice_tel,
                 'nom'=>$constat->nom,
                 'agent'=>Agent::find($constat->agent_id),
                 'qualiticien'=>User::find($constat->qualiticien_id),
                 'commentaire_constat'=>$constat->commentaire_constat,
                 'type_constat'=>$constat->type_constat,
                 'deadeline_constat'=>$constat->deadeline_constat,
                 'file'=>$file,
                 'url_file'=>asset(\Storage::url($constafile->url)),
                 'site'=>$constat->site,
                 'date_time_constat'=>$constat->time_send_consta,
                 
             ];
             return $data;
        }
        else {
            $constat = Constat::find($id);
            if($constat->statu===1){ 
                $constatAction = ActionConstat::where('constat_id',$id)->first();
                $data=[
                    'constat_activite'=>$constat->activite,
                    'service'=>Service::find($constat->service_id),
                    'statut'=>$constat->statu,
                    'sup'=>User::find($constat->sup_id),
                    'date_appel'=>$constat->date_appel,
                    'client_id'=>$constat->client_id,
                    'indice_tel'=>$constat->indice_tel,
                    'nom'=>$constat->nom,
                    'avis'=>$constatAction->avis,
                    'agent'=>Agent::find($constat->agent_id),
                    'qualiticien'=>User::find($constat->qualiticien_id),
                    'commentaire_constat'=>$constat->commentaire_constat,
                    'type_constat'=>$constat->type_constat,
                    'deadeline_constat'=>$constat->deadeline_constat,
                    'action'=>$constatAction->action,
                    'commentaire_action'=>$constatAction->commentaire_action,
                    'deadeline_action'=>$constatAction->deadeline_action,
                    'date_time_action'=>$constatAction->time_action_constat,
                    'date_time_constat'=>$constat->time_send_consta,
                    'site'=>$constat->site,
                    
                ];
                return $data;
            }

            $data=[
                 'constat_activite'=>$constat->activite,
                 'service'=>Service::find($constat->service_id),
                 'statut'=>$constat->statu,
                 'sup'=>User::find($constat->sup_id),
                 'date_appel'=>$constat->date_appel,
                 'client_id'=>$constat->client_id,
                 'indice_tel'=>$constat->indice_tel,
                 'nom'=>$constat->nom,
                 'agent'=>Agent::find($constat->agent_id),
                 'qualiticien'=>User::find($constat->qualiticien_id),
                 'commentaire_constat'=>$constat->commentaire_constat,
                 'type_constat'=>$constat->type_constat,
                 'deadeline_constat'=>$constat->deadeline_constat,
                 'site'=>$constat->site,
                 'date_time_constat'=>$constat->time_send_consta,
             
                 
             ];
             return $data;
        }
     
       
       
    }
    public function liste(Request $req){
        $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
        if(isset($req->input('dateRange')[1])) {
            $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
        } else {
            $endDate = $startDate;
        }
        $user = auth('api')->user();
        $constats = Constat::all();
        if($constats->count()==0) return 0;

        foreach($constats as $constat){
    
        $data[]=[
            'id'=>$constat->id,
            'activite'=>$constat->activite->nom,
            'agent'=>$constat->agent->nom .''.$constat->agent->prenom,
            'statut'=>$constat->statu,
            'client_id'=>$constat->client_id,
            'site'=>$constat->site,
            'date'=>Carbon::createFromFormat('Y-m-d H:i:s', $constat->created_at)->format('d/m/Y'),
            'qualiticien'=>$constat->user->nom. ' '.$constat->user->prenom
        ];
                }
                return $data;
    }
    public function count(){
        $user = auth('api')->user();
        $agent =Agent::where('email',$user->email)->first();
        if($agent){
            return $agent->unreadNotifications->count();
        }
        else {
            return $user->unreadNotifications->count();
        }
    }
    public function deleteNotification() {
        $user = auth('api')->user();
        $agent =Agent::where('email',$user->email)->first(); 
   
        if($agent){
            if($agent->notifications()->delete()) {
                return 1;
            }
        }
        else {
            $user->notifications()->delete();
        }
        
    }
    public function mylistCopie(Request $req){
        
        $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
        if(isset($req->input('dateRange')[1])) {
            $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
        } else {
            $endDate = $startDate;
        }
        $user = auth('api')->user();
        if($user->getRoleNames()[0]=='superviseur'){
           
            $constatSupCopie = ConstatSupCopie::Where('sup_id',$user->id)->get();
            if($constatSupCopie->count()==0) return 0;
            foreach($constatSupCopie as $constassup){
                $constat = Constat::where('id',$constassup->constat_id)->first();
                $data[]=[
                    'id'=>$constat->id,
                    'activite'=>$constat->activite->nom,
                    'agent'=>$constat->agent->nom .''.$constat->agent->prenom,
                    'statut'=>$constat->statu,
                    'created_at'=>$constat->created_at,
                    'superviseur_concerne'=>User::find($constat->sup_id)->nom.' '.User::find($constat->sup_id)->prenom,
                    'date'=>Carbon::createFromFormat('Y-m-d H:i:s', $constat->created_at)->format('d/m/Y'),
                    'client_id'=>$constat->client_id,
                    'site'=>$constat->site,
                ];
               }
               return $data;
        }
        else {
            $constatSupCopie = ConstatQualCopie::Where('qual_id',$user->id)->get();
            if($constatSupCopie->count()==0) return 0;
            foreach($constatSupCopie as $constassup){
                $constat = Constat::where('id',$constassup->constat_id)->first();
                $data[]=[
                    'id'=>$constat->id,
                    'activite'=>$constat->activite->nom,
                    'agent'=>$constat->agent->nom .''.$constat->agent->prenom,
                    'statut'=>$constat->statu,
                    'created_at'=>$constat->created_at,
                    'superviseur_concerne'=>User::find($constat->sup_id)->nom.' '.User::find($constat->sup_id)->prenom,
                    'client_id'=>$constat->client_id,
                    'site'=>$constat->site,
                    'date'=>Carbon::createFromFormat('Y-m-d H:i:s', $constat->created_at)->format('d/m/Y'),
                ];
               }
               return $data;
        }
 
    }
    public function extraire(){
        return Excel::download(new ConstatExport, 'ConstatExport.xlsx');
    }
    public function extraire_user(){
        $user = auth('api')->user();
        return Excel::download(new ConstatUsreExport($user->id), 'ConstatExport.xlsx');
    }
}
