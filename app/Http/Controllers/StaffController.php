<?php

namespace App\Http\Controllers;

use App\User;
use App\Admin;
use App\Agent;
use App\Qualiticien;
use App\Imports\AgentImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Response;
use App\Service ;
use Illuminate\Support\Facades\Hash;
/*
 * this controller is mainly used for ( users control on admin side )
 * */
class StaffController extends Controller
{
    public function addStaff(Request $request)
    {   // new version working
        $request->validate([
            'nom' => 'required',
            'prenom' => 'required',
            'email' => 'required|string|email',
            'password' => 'required|string',
            'phone' => 'required',
            'matricule' => 'required',
            'role' => "required",
        ]);

        // adding agent
        if($request->input('role') == 'agent') {
            if ( Agent::where('matricule', $request->matricule )->exists() || Agent::where('email', $request->email )->exists() ) {
                if(Agent::where('matricule', $request->matricule )->exists()) {
                    return response()->json([
                        'message' => 'matricule'
                    ]);
                } else {
                    return response()->json([
                        'message' => 'email'
                    ]);
                }

            } else {
                $agent = new Agent([
                    'nom' => $request->nom,
                    'prenom' => $request->prenom,
                    'telephone' => $request->phone,
                    'matricule' => $request->matricule,
                    'email' => $request->email,
                    'password' => bcrypt($request->password),
                    'service_id' => $request->service,
                    'dateInte' => $request->dateInte,
                    'site' => $request->site,
                ]);
                $agent->save();
                $user = new User([
                    'nom' => $request->nom,
                    'prenom' => $request->prenom,
                    'telephone' => $request->phone,
                    'matricule' => $request->matricule,
                    'statut' =>1,
                    'email' => $request->email,
                    'password' => bcrypt($request->password),
                ]);
                $role = Role::where('name', $request->input('role') )->first();
                $user->assignRole($role);
    
                $user->save();
    
       
                return response()->json([
                    'message' => 'user created!',
                    'role' => 'agent',
                ], 201);
        
            }
        }

        // adding user

        if ( User::where('email', $request->email )->exists() ) {
            return response()->json([
                'message' => 'email'
            ]);

        } else {
            // return $request;
            $user = new User([
                'nom' => $request->nom,
                'prenom' => $request->prenom,
                'telephone' => $request->phone,
                'matricule' => $request->matricule,
                'email' => $request->email,
                'password' => bcrypt($request->password),
            ]);

            $role = Role::where('name', $request->input('role') )->first();
            $user->assignRole($role);

            $user->save();

            if($request->input('role') == 'client' || $request->input('role') == 'superviseur') {
                //$request->activites
                //$user->activite()->attach($request->input('activite'));
                $user->activite()->sync($request->activites);
            }
            return response()->json([
                'message' => 'user created!',
                'role' => $role->name,
            ], 201);
        }

        /*
        $request->validate([
            'nom' => 'required',
            'prenom' => 'required',
            'email' => 'required|string|email',
            'password' => 'required|string',
            'phone' => 'required',
            'matricule' => 'required',
            'role' => ["required" , "in:qualiticien,admin,agent"],
        ]);

        if($request->input('role') == 'admin') {
            $admin = new Admin([
                'nom' => $request->nom,
                'prenom' => $request->prenom,
                'telephone' => $request->phone,
                'matricule' => $request->matricule,
                'email' => $request->email,
                'password' => bcrypt($request->password)
            ]);
            $admin->save();
            return response()->json([
                'message' => 'user created!'
            ], 201);
        }

        if($request->input('role') == 'agent') {
            if ( Agent::where('matricule', $request->matricule )->exists() || Agent::where('email', $request->email )->exists() ) {
                if(Agent::where('matricule', $request->matricule )->exists()) {
                    return response()->json([
                        'message' => 'matricule'
                    ]);
                } else {
                    return response()->json([
                        'message' => 'email'
                    ]);
                }

            } else {
                //return $request;
                $agent = new Agent([
                    'nom' => $request->nom,
                    'prenom' => $request->prenom,
                    'telephone' => $request->phone,
                    'matricule' => $request->matricule,
                    'email' => $request->email,
                    'password' => bcrypt($request->password),
                    'service_id' => $request->service,
                    'dateInte' => $request->dateInte,
                    'site' => $request->site,
                ]);
                $agent->save();
                return response()->json([
                    'message' => 'user created!'
                ], 201);
            }
        }

        if($request->input('role') == 'qualiticien') {
            if ( Qualiticien::where('matricule', $request->matricule )->exists() || Qualiticien::where('email', $request->email )->exists() ) {
                if(Qualiticien::where('matricule', $request->matricule )->exists()) {
                    return response()->json([
                        'message' => 'matricule'
                    ]);
                } else {
                    return response()->json([
                        'message' => 'email'
                    ]);
                }

            } else {
                $agent = new Qualiticien([
                    'nom' => $request->nom,
                    'prenom' => $request->prenom,
                    'telephone' => $request->phone,
                    'matricule' => $request->matricule,
                    'email' => $request->email,
                    'password' => bcrypt($request->password),
                ]);
                $agent->save();
                return response()->json([
                    'message' => 'user created!'
                ], 201);
           }
        }
      */
    }

    public function getAgents(){
        $agents  = Agent::all('id','nom','prenom','matricule','site','statut','service_id');
        $data = [];
        foreach ($agents as $agent){
            $data[] = [
                'id'=>$agent->id,
                'nom'=>$agent->nom,
                'prenom'=>$agent->prenom,
                'matricule'=>$agent->matricule,
                'site'=>$agent->site,
                'statut'=>$agent->statut,
                'service'=> $agent->service->nom
                ];
        }
        return $data;
    }
    public function getQualiticiens(){
       //return Qualiticien::all('id','nom','prenom','matricule','email','statut')->all();
       return User::whereHas("roles", function($q){ $q->where("name", "qualiticien")->orWhere("name", "superviseur")->orWhere("name", "client"); })->get();
    }

    public function getAdmins(){
        // return Admin::all('id','nom','prenom','matricule','email','statut')->all();
        return User::whereHas("roles", function($q){ $q->where("name", "admin"); })->get();
    }

    public function getClients() {
        $clients = User::whereHas("roles", function($q){ $q->where("name", "client"); })->get();
        $data = array();
        foreach($clients as $client) {
            $data[] = [
                'id' => $client->id,
                'nom' => $client->nom.' '.$client->prenom,
                'email' => $client->email,
                'activity' => (!empty($client->activite->toArray()) ? $client->activite[0]->nom : ''),
                'statut' => $client->statut,
            ];
        }
        return $data;
    }

    public function getSuperviseurs() {
        $sups = User::whereHas("roles", function($q){ $q->where("name", "superviseur"); })->get();
        $data = array();
        foreach($sups as $sup) {
          
            $data[] = [
                'id' => $sup->id,
                'nom' => $sup->nom.' '.$sup->prenom,
                'email' => $sup->email,
                'activity' => (!empty($sup->activite->toArray()) ? $sup->activite[0]->nom : ''),
                'statut' => $sup->statut,
            ];
        }
        return $data;
    }
    // 
    public function getSupActvitvites(){
        $sups = User::whereHas("roles", function($q){ $q->where("name", "superviseur"); })->get();
      
        foreach($sups as $sup) {
          
       foreach($sup->activite as $a){
        $activite_user[] = [
            'nom'=>$a->nom,
            'id'=>$a->id
    ];
  
       }
       $activite_user = array_values(array_unique( $activite_user, SORT_REGULAR )); 
       $data[] = [
        'id' => $sup->id,
        'nom' => $sup->nom.' '.$sup->prenom,
        'email' => $sup->email,
        'activitys' => $activite_user ,
        'statut' => $sup->statut,
    ];
    
    //   
    
    }
     
    
          
            
         
        
      
        return $data;
    
        
    }

    public function getAgent($id){
        return Agent::find($id);
    }
    public function getQualiticien($id){
        // return Qualiticien::find($id)->toArray();
        return User::find($id);
    }

    public function getAdmin($id){
        // return Admin::find($id);
        return User::find($id);
    }
    public function getUser($id) {

        $user = User::find($id);
        //  return $user->getRoleNames();
        $data = array();
        $data['role'] = $user->getRoleNames()[0];
        if(($data['role'] == 'client') || ($data['role'] == 'superviseur')) {
            $activiteId = [];
            foreach($user->activite as $a) {
              $activiteId[] = $a->id;
            }
            $data['activity'] = $activiteId;
        }
        $data['user'] = $user->makeHidden(['roles','activite']);
        return $data;

    }
    public function updateAgent(Request $request){
      
        $agent = Agent::find($request->agent_id);
        if(!is_null($request->role_id) && !is_null($request->password)){
            $user = User::where('email', $request->email)->first();
            $user->password = bcrypt($request->password);
            $service = Service::find($request->service);
       
            $activite = $service->activite;
           
            $role = Role::where('id',  $request->role_id )->first();

             DB::table('model_has_roles')->where('model_id',$user->id)->delete();
  

    
         $user->syncRoles([]);
            $user->assignRole($role);
            $user->activite()->sync($activite);
       
            $user->save();
            return 'ok';
        }
        else {
            if ($agent->matricule != $request->matricule){
                if(Agent::where('matricule', $request->matricule )->exists()) return 'matricule';
            }    
            if ($agent->email != $request->email){
                if(Agent::where('email', $request->email )->exists()) return 'email';
            }
            $agent = Agent::find($request->agent_id);
            $agent->nom = $request->nom;
            $agent->prenom = $request->prenom;
            $agent->email = $request->email;
            if(!is_null($request->password)) {
                $agent->password = bcrypt($request->password);
            }
            $agent->service_id = $request->service;
            $agent->telephone = $request->phone;
            $agent->matricule = $request->matricule;
            $agent->dateInte = $request->dateInte;
            $agent->site = $request->site;
            $agent->save();
            $user = User::where('email',$agent->email)->first();
            if($user){
                preg_match_all('!\d+!', $request->matricule, $matricule);
              
               $user = User::where('email',$agent->email)->first();
               $user->nom = $request->nom;
               $user->prenom = $request->prenom;
               $user->email = $request->email;
               $user->password = bcrypt($matricule[0][0]);
               $user->telephone = $request->phone;
               $user->matricule = $request->matricule;
               if(!is_null($request->activity)) $user->activite()->sync($request->activity);
       
                $user->save();
               return 'ok';
           }
           else {

            preg_match_all('!\d+!', $request->matricule, $matricule);
               
                $user =new User();
                $user->nom = $request->nom;
                $user->prenom = $request->prenom;
                $user->email = $request->email;
                if(!is_null($request->password)) $user->password = bcrypt($matricule[0][0]);
                $user->telephone = $request->phone;
                $user->matricule = $request->matricule;
                if(!is_null($request->activity)) $user->activite()->sync($request->activity);
                $role = Role::where('name','agent')->first();
                $user->assignRole($role);
                $user->save();
               
                return 'ok'; 
            }

        }

      

      
    }

    public function updateUser(Request $request){
       
        // $request->validate([
        //     'nom' => 'required',
        //     'prenom' => 'required',
        //     'email' => 'required|string|email',
        //     'phone' => 'required',
        //     'matricule' => 'required',
        //     'user_id' => 'required',
        // ]);
           
        $user = User::find($request->user_id);

        $user->nom = $request->nom;
        $user->prenom = $request->prenom;
        $user->email = $request->email;
        if(!is_null($request->password)) $user->password = bcrypt($request->password);
        $user->telephone = $request->phone;
        $user->matricule = $request->matricule;
        if(!is_null($request->activity)) $user->activite()->sync($request->activity);
      
        
        $user->syncRoles([]);
        $role = Role::where('id',  $request->role )->first();
        
        $user->assignRole($role);
       
        return $user->save();
    }

    public function disableUser(Request $request) {
       
         $u = User::find($request->input('id'));
         if($u->statut == '0' ) {
            $u->statut = '1';
        } else $u->statut = '0';
        $u->save();
    }

    public function massAgentImport(Request $request){

        if($request->hasFile('file')){
            if($request->file('file')->extension() == 'xlsx' || $request->file('file')->extension() == 'xls'){
                $c1 = Agent::All()->count();
                Excel::import(new AgentImport,$request->file('file'));
                $c2 = Agent::All()->count();
                return $c2-$c1;
            }
            else{
                return 'file not compatible';
            }
        }
        else return 'no file';

        Excel::import(new AgentImport,$request->file('file'));
    }

    public function excelTemplate() {
       // return Storage::download('AgentsTemplate.xlsx');
       $xlsx = Storage::disk('public')->get('AgentsTemplate.xlsx');
       return Response($xlsx, 200)->header('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); //application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
    }
    public function getclients_user(){
        $user = auth('api')->user();


        $current_activite_user = DB::table('activite_user')->where('activite_id',$user->activite[0]->id)->get();

    foreach( $current_activite_user as $activite_user){
        $clients[] = User::whereHas("roles", function($q){ $q->where("name", "client"); })->where('id',$activite_user->user_id)->first();
       
    }
$clients= array_filter($clients);

    $data = array();
    foreach($clients as $client) {

        $data[] = [
            'id' => $client->id,
            'nom' => $client->nom.' '.$client->prenom,
            'email' => $client->email,
            'activity' => (!empty($client->activite->toArray()) ? $client->activite[0]->nom : ''),
            'statut' => $client->statut,
        ];
    }
    return $data;
    }
    public function get_current_user(){
      $user = auth('api')->user();
        
        $data[] = [
            'id' => $user->id,
            'nom' => $user->nom.' '.$user->prenom,
            'email' => $user->email,
            'activity' => (!empty($user->activite->toArray()) ? $user->activite[0]->nom : ''),
            'statut' => $user->statut
        ];
        return $data;
    }


    public function getAllAgent($id){
     // 506
     // 575 
   // 504
     // 638
        $agent  = Agent::findOrFail($id);
        if($user=User::where('email',$agent->email)){
            $user->delete();
            $user=  new User([
                'nom' => $agent->nom,
                'prenom' =>$agent->prenom,
                'telephone' => 000000,
                'matricule' => $agent->matricule,
                'email' => $agent->email,
                'password' => bcrypt('password'),
            ]);
            $role = Role::where('name', 'agent' )->first();
            $user->assignRole($role);

            $user->save();
        }
        else {
            $user=  new User([
                'nom' => $agent->nom,
                'prenom' =>$agent->prenom,
                'telephone' => 000000,
                'matricule' => $agent->matricule,
                'email' => $agent->email,
                'password' => bcrypt('0000'),
            ]);
            $role = Role::where('name', 'agent' )->first();
            $user->assignRole($role);

            $user->save();
        }
                   
        // $data = [];
        // foreach ($agents as $agent){
        //     // $data[] = [
        //     //     'id'=>$agent->id,
        //     //     'nom'=>$agent->nom,
        //     //     'prenom'=>$agent->prenom,
        //     //     'matricule'=>$agent->matricule,
        //     //     'site'=>$agent->site,
        //     //     'statut'=>$agent->statut,
        //     //     'email'=> $agent->service->nom
        //     //     ];
        //        $user=  new User([
        //             'nom' => $agent->nom,
        //             'prenom' =>$agent->prenom,
        //             'telephone' => 000000,
        //             'matricule' => $agent->matricule,
        //             'email' => $agent->email,
        //             'password' => bcrypt('password'),
        //         ]);
        //         $role = Role::where('name', 'agent' )->first();
        //         $user->assignRole($role);
    
        //         $user->save();
        // }
        // return $data;

    }
    public function hash(){
      
        return Hash::make('0044');
    }
    public function getRole(){
        $roles = Role::all();
    }
}
