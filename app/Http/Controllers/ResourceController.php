<?php

namespace App\Http\Controllers;

use App\Resource;
use Illuminate\Http\Request;
use Storage;

class ResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Resource::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->file('doc')->getSize();
        // 10M in bytes 10485760
        if($request->file('doc')->getSize() > 10585760){
            return 'size';
        }
        $resource = new Resource();
        $resource->title = $request->input('title');
        $resource->description = $request->input('description');
        $fileName = $request->input('title').'_'.time().'.'.$request->doc->extension();
        $path = $request->file('doc')->storeAs('uploads/resources', $fileName, 'public');
        $resource->url = $path;
        $resource->save();

        return 'added';
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Resource  $resource
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $resource = Resource::find($id);
        $pdf = Storage::disk('public')->get($resource->url);
        $data['resource'] = $resource;
        $data['pdf'] = base64_encode($pdf);
        return $data;
     
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Resource  $resource
     * @return \Illuminate\Http\Response
     */
    public function edit(Resource $resource)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Resource  $resource
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Resource $resource)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Resource  $resource
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $resource = Resource::find($id);
        if(Storage::disk('public')->exists($resource->url)){
            Storage::disk('public')->delete($resource->url);
        }
        $resource->delete();
    }
}
