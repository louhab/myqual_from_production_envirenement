<?php

namespace App\Http\Controllers;

use App\Evaluation;
use App\Note;
use App\Service;
use Carbon\Carbon;
use App\Agent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\EvaluationExport;
use App\Exports\AgentNote;
use App\Notifications\EvalNotification;
use App\User;
use DateTime;


class EvaluationController extends Controller
{

    //show // send elements corresponding to this services's grid
    public function create($id){

        $service = Service::find($id);  //service id
        // test if serve exists then if the 'grille' exists

        //preparing 'grille' elements
        $grille = $service->grille;

        $elpartmap = array();
        foreach($grille->elements as $element){
            $partie = $element->partie->nom;
            $el = ['id'=>$element->pivot->id,'nom'=>$element->nom,'g_ok'=>$element->pivot->ok,'g_pa'=>$element->pivot->pa,
            'g_ko'=>$element->pivot->ko,'g_si'=>$element->pivot->si,'g_na'=>$element->pivot->na,'bareme'=>$element->pivot->bareme];
            $testkey = null;

            foreach ($elpartmap as $key => $val) {
                if ($val['partie'] === $partie) {
                    $testkey = $key;
                    break;
                }
            }
            if(isset($testkey)){
                $elpartmap[$testkey]['elements'][] = $el;
            }else{
                $elpartmap[]=['partie'=>$partie,'elements'=>[$el]];
            }
        }

        //get all agents associated to this service
        // $agents = $service->agents; this should be sent with activities and services
         /* $agents = DB::table('agents')->select('id', 'nom','prenom','matricule')->where([
            ['statut','1'],
            ['service_id', $service->id],
        ])->get(); */

        return  $elpartmap;
    }

    public function store(Request $request) {
    
     
      $agent_user= Agent::find($request->agentId);

        $request->validate([
            // add validation
        ]);
        $val = new Evaluation();

        $serviceId = $request->input('serviceId');

        $items = $request->input('items');
        if($request->MotifAlpique !=null){
            $val->motif_alpique = $request->MotifAlpique['nom'] ;

        }
        else {
            $val->motif_alpique = null ;
        }
      if($request->qualif != null){
          $val->qualif = $request->qualif['nom'];
      }
      else {
        $val->qualif = null;
      }
        $val->grille_id = Service::find($serviceId)->grille->id;
        $val->client_id = $request->input('clientId');
        $val->indice_tel = $request->input('indice');
        $val->pcc = $request->input('pcc');
        $val->satcli = $request->input('satcli');
        $val->date_appel = $request->input('date');
        $val->oad = $request->input('oad');
        $val->exci = $request->input('exci');
        $val->synthese = $request->input('synthese');
        $val->agent_id = $request->input('agentId');
        $val->semaine = $request->input('weekofyear');
        $val->site = $val->agent->site;
        //if($request->input('role') == 'admin')  $val->qualiticien_id = 8 ;
        $val->qualiticien_id = $request->input('qualiticien');
        $val->dap = $request->dap;
        if($request->motifsPoint) $val->moutif = $request->motifsPoint['motifs'];
        if($request->SousmotifsPoint) $val->sousmoutif = $request->SousmotifsPoint ;
        $val->save();
      

        $note_global = 0;
        $bareme_global = 0;
        $bareme_element_where_na =0;
        $bareme_element_where_AQ = 0;
        foreach ($items as $item){
            foreach ($item['elements'] as $element){ 
                $note_array[]= $element['note'];
                if($element['note'] == 'NA')  {
                    $bareme_element_where_na +=$element['bareme'];
                }
                if($element['note'] == 'AQ')  {
                    $bareme_element_where_AQ +=1;
                }
               
            }
        }
        $note_elements = [];
        $note_cal = 0;
        if(  $request->input('activiteId')==4 ) {
            foreach ($items as $item){
                foreach ($item['elements'] as $element){
                    $note = new Note();
                    $note->evaluation_id = $val->id;
                    $note->element_id=$element['id'];
                    $note->note=$element['note'];
                    if( $note->note == 'AQ' ||  $note->note == 'NA'){
                        $note_global += $element['bareme'];
                    }
                    if( $note->note == 'PA' ){
                        $note_global += ($element['bareme']/2);
                    }
                    if(isset($element['comment'])){
                        $note->comment=$element['comment'];
                    }
                    $note->save();
                    $bareme_global += $element['bareme'];
                }
            }    
            if($request->input('sim')==true){
                $val->note_global = 0;
                $val->save();
                if( $agent_user->notify(new EvalNotification($val))){
                    return 1;
                   }
            }
    
            $val->note_global = round($note_global, 2);
            $val->save();
           if( $agent_user->notify(new EvalNotification($val))){
            return 1;
           }
            
            
        }
     else    if(  $request->input('activiteId')==11 ) {
        foreach ($items as $item){
            foreach ($item['elements'] as $element){
        
                $note = new Note();
                $note->evaluation_id = $val->id;
                $note->element_id=$element['id'];
                $note->note=$element['note'];
                if( $note->note == 'EX' ){
                    $note_global += $element['bareme'];
                }
                if( $note->note == 'BON' ){
                    if($element['id']===1258 || $element['id']===1259){
                        
                        $note_global += ($element['bareme']*0.834);
                    }
                    else if($element['id']===1260){
                        $note_global += ($element['bareme']*0.6);
                    }
                    else {
                        $note_global += ($element['bareme']*0.8);
                    }
                    
                }
                if( $note->note == 'MOY' ){
                    if($element['id']===1260){
                        $note_global += ($element['bareme']*0.2); 
                    }
                    else if($element['id']===1259 || $element['id']===1258){
                        $note_global += ($element['bareme']*0.5);
                    }
                    else {
                        $note_global += ($element['bareme']*0.6);
                    }
                   
                }
                if( $note->note == 'MAU' ){
                    $note_global += ($element['bareme']*0);
                }
                if(isset($element['comment'])){
                    $note->comment=$element['comment'];
                }
                $note->save();
                $bareme_global += $element['bareme'];
            }
        }

        // applying malus here
        // if($request->input('activiteId') == 7 ) {
        //     $note_global = $note_global - $request->input('malus')*(100/600);
        // }

        if($request->input('sim')==true){
            $val->note_global = 0;
            $val->save();
            if( $agent_user->notify(new EvalNotification($val))){
                return 1;
               }
        }

        $val->note_global = round($note_global, 2);
        $val->save();
       if( $agent_user->notify(new EvalNotification($val))){
        return 1;
       }
        
        
    }
        
        else if($request->input('activiteId')==20 || $request->input('activiteId')==28 ||$request->input('activiteId')==3  || $request->activiteId==24 || $request->activiteId==7 || $request->activiteId==26)  {
            foreach ($items as $item){
                foreach ($item['elements'] as $element){
                    
            $note = new Note();
            $note->evaluation_id = $val->id;
            $note->element_id=$element['id'];
            $note->note=$element['note'];
                    if($element['note'] != 'NA')  {
                        $note_elements [] = $element['note'];
                     }
                     if(isset($element['comment'])){
                        $note->comment=$element['comment'];
                    }
                    $note->save();
                   
                }
            }
            $note_cal = 100/ count($note_elements) ;
         
         
            foreach($note_elements as $no_cal) {
           
                if($no_cal == 'AQ') {
                      $note_global += $note_cal;
                }
                if($no_cal == 'PA') {
                    $note_global += ($note_cal/2) ;
                }
            }
            $bareme_global += $element['bareme'];
            
        }
      

else {
    foreach ($items as $item){
        foreach ($item['elements'] as $element){

            $note = new Note();
            $note->evaluation_id = $val->id;
            $note->element_id=$element['id'];
            $note->note=$element['note'];
         

            if( $note->note == 'AQ' ){
                if($bareme_element_where_na!=0){
                    $note_global += $element['bareme'] + ($bareme_element_where_na/$bareme_element_where_AQ);
                }
                else {
                    $note_global += $element['bareme'] ;
                }
                // note_global val 
            }
            if( $note->note == 'PA'  ){
                if($bareme_element_where_na!=0){
                $note_global += ($element['bareme']/2) ;
                }
                else {
                    $note_global += ($element['bareme']/2) ;
                }
            }
            if(isset($element['comment'])){
                $note->comment=$element['comment'];
            }
            $note->save();
            $bareme_global += $element['bareme'];
         
        }
    }
    
}

    
      

      


        // applying malus here
        if($request->input('activiteId') == 7 ) {

            $note_global = $note_global - $request->input('malus')*(100/600);
        }

        if($request->input('sim')==true){
            $val->note_global = 0;
            $val->save();
            // if( $agent_user->notify(new EvalNotification($val))){
            //     return 1;
            //    }
        }
 else {
    $val->note_global = round($note_global, 0);
    $val->save();
 }
        // note_global val 
        if( $agent_user->notify(new EvalNotification($val))){
            return 1;
           }
    }
    public function getMyEvals(Request $req){
 
        $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
        if(isset($req->input('dateRange')[1])) {
            $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
        } else {
            $endDate = $startDate;
        }
        $user = auth('api')->user();

        // fetch by 'date_appel' if switchDate is true else fetch by created_at (=date_eval)
        if($req->switchDate == "true"){
            $date_param = 'date_appel';
        } else {
            $date_param = 'created_at';
        }

        $evals = Evaluation::where('qualiticien_id', $user->id)->whereIn('site', $req->input('selectedSites'))->whereBetween($date_param, [ $startDate, $endDate])->get();

        $data = array();
        foreach ($evals as $eval){
            $data[] = [
                'id'=>$eval->id,
                'note'=>$eval->note_global,
                'agent'=>$eval->agent->nom.' '.$eval->agent->prenom,
                'qualiticien'=>$eval->qualiticien->nom.' '.$eval->qualiticien->prenom,
                'activity'=>$eval->grille->service->activite->nom,
                'service'=>$eval->grille->service->nom,
                'client_id'=>$eval->client_id,
                'date_appel'=>Carbon::createFromFormat('Y-m-d', $eval->date_appel)->format('d/m/Y'),//$eval->date_appel,
                'date_eval'=> Carbon::createFromFormat('Y-m-d H:i:s', $eval->created_at)->format('d/m/Y'),
            ];
        }
        return $data;
    }
    // between two dates
    public function getAllEvals(Request $req) {

        $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
        if(isset($req->input('dateRange')[1])) {
            $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
        } else {
            $endDate = $startDate;
        }
        $user = auth('api')->user();
 

        // fetch by 'date_appel' if switchDate is true else fetch by created_at (=date_eval)
        if($req->switchDate == "true"){
            $date_param = 'date_appel';
        } else {
            $date_param = 'created_at';
        }
        // if the user is a sup or a client return only eval associated to his activity
        // if a user is an admin return all eval
        if (empty($user->activite->toArray())) {
            $evals = Evaluation::whereIn('site', $req->input('selectedSites'))->whereBetween($date_param, [ $startDate, $endDate])->get();
        } else {
            // get activity id
            $activity[] = $user->activite;
      
          foreach($activity as $act ){
            foreach($act as $a){
                
                $grilles[] = $a->services->map(function($service) {
                    if($service->grille){
                        return $service->grille->id;
                    }
                });
            }
          }
         foreach(  $grilles as $g){
            $evals2[] = Evaluation::whereIn('grille_id',$g->toArray())->whereIn('site', $req->input('selectedSites'))->whereBetween($date_param, [ $startDate, $endDate])->get();
         }
          
        }

        $data = array();
        if(!empty($evals2) ){
            foreach($evals2 as $eval2){
                foreach ($eval2 as $eval){
                    $data[] = [
                        'id'=>$eval->id,
                        'note'=>$eval->note_global,
                        'agent'=>$eval->agent->nom.' '.$eval->agent->prenom,
                        'qualiticien'=>$eval->qualiticien->nom.' '.$eval->qualiticien->prenom,
                        'service'=>$eval->grille->service->nom,
                        'activity'=>$eval->grille->service->activite->nom,
                        'client_id'=>$eval->client_id,
                        'date_appel'=>Carbon::createFromFormat('Y-m-d', $eval->date_appel)->format('d/m/Y'),//$eval->date_appel,
                        'date_eval'=> Carbon::createFromFormat('Y-m-d H:i:s', $eval->created_at)->format('d/m/Y'),
                        'dap'=>($eval->dap) ? $eval->dap : ' ' ,
                    ];
                }
            }
        }
      
      else {
        foreach ($evals as $eval){
            $data[] = [
                'id'=>$eval->id,
                'note'=>$eval->note_global,
                'agent'=>$eval->agent->nom.' '.$eval->agent->prenom,
                'qualiticien'=>$eval->qualiticien->nom.' '.$eval->qualiticien->prenom,
                'service'=>$eval->grille->service->nom,
                'activity'=>$eval->grille->service->activite->nom,
                'client_id'=>$eval->client_id,
                'date_appel'=>Carbon::createFromFormat('Y-m-d', $eval->date_appel)->format('d/m/Y'),//$eval->date_appel,
                'date_eval'=> Carbon::createFromFormat('Y-m-d H:i:s', $eval->created_at)->format('d/m/Y'),
                'dap'=>($eval->dap) ? $eval->dap : ' ' ,
            ];
        }
      }
        return $data;
    }

    // return eval's response elements
    public function consultEval($id){
      
        $eval = Evaluation::find($id);
    
        $elements = self::create($eval->grille->service->id);


        foreach ($elements as &$item){
            foreach ($item['elements'] as &$element){
              if(  $note = Note::select('note','comment')->where(['evaluation_id'=>$id,'element_id'=>$element['id']])->first()) {
         
                $element['note'] = $note->note;
                $element['comment'] = $note->comment;
              }
              else {
               
                $note = Note::select('note','comment')->where(['evaluation_id'=>$id])->firstOrFail();
            
            

                $element['note'] = $note->note;
                $element['comment'] = $note->comment;
              }

              
            }
        }
        $data = array();
        $qualiticiens = array();
        $data['eval'] = $eval;
        $qualiticiens1 = User::whereHas("roles", function($q){ $q->where("name", "qualiticien")->orWhere("name", "superviseur")->orWhere("name", "client") ; })->get();
        foreach($qualiticiens1 as $qual){
            $qualiticiens[] = [
                'id'=>$qual->id,
                'nom'=>$qual->nom.' '.$qual->prenom
            ];
        }
        $data['qualiticiens'] = $qualiticiens;
        $data['elements'] = $elements;

       return $data;
    }
    public function editHeader(Request $request){
        $eval = Evaluation::find($request->input('evalid'));
        $eval->client_id = $request->input('data')['clientId'];
        $eval->indice_tel = $request->input('data')['indice'];
        $eval->date_appel =  $request->input('data')['dateAppel'];
        $eval->created_at =$request->input('data')['dateEval'];
        $eval->agent_id =$request->input('data')['agentId']['id'];
        $eval->qualiticien_id = $request->input('data')['qualiticien']['id'];
        $eval->save();
    }
    public function editFooter(Request $request){

        $eval = Evaluation::find($request->input('evalid'));
       // return $request;
        $eval->synthese = $request->input('data')['synthese'];
        $eval->exci = $request->input('data')['exci'];
        $eval->oad = $request->input('data')['oad'];
        $eval->satcli = $request->input('data')['satcli'];
        $eval->pcc = $request->input('data')['pcc'];

        if(!(($eval->note_global == 0 && $request->input('data')['sim'])||($eval->note_global != 0 && !$request->input('data')['sim']))){
          if($request->input('data')['sim']){
              $eval->note_global = 0;
          }else{
              $note_global = 0;
              foreach ($request->input('items') as $item){
                  foreach ($item['elements'] as $element){
                      if( $element['note'] == 'AQ' ||  $element['note'] == 'NA'){
                          $note_global += $element['bareme'];
                      }
                      if( $element['note'] == 'PA' ){
                          $note_global += ($element['bareme'])/2;
                      }
                  }
              }
              $eval->note_global = $note_global;
          }
        }

        if($request->input('malus') !== null) {
            $note_global = 0;
            foreach ($request->input('items') as $item){
              foreach ($item['elements'] as $element){
                if( $element['note'] == 'AQ' ||  $element['note'] == 'NA'){
                  $note_global += $element['bareme'];
                }
              }
            }
            $note_global = $note_global - $request->input('malus')*(100/600);
            $eval->note_global = $note_global;
        }
        $eval->note_global = round($eval->note_global, 2);
        if($request->input('data')['sim']){
            $eval->note_global = 0;
        }
        $eval->save();
        return $eval->note_global;
    }
    public function editItem(Request $request){
   
        $note = Note::Where(['element_id'=>$request->input('data')['id'],'evaluation_id'=>$request->input('evalid')])->first();
        $eval = Evaluation::find($request->input('evalid'));
       
        $note->comment = $request->input('data')['comment'];
         if($note->note !== $request->input('data')['note']){
              if($request->input('data')['note'] =='AQ' && $note->note == 'PA' ){
                $eval->note_global = $eval->note_global+ $request->input('data')['bareme']/2;
                
              }
              if($request->input('data')['note'] =='NA' && $note->note == 'PA' ){
                $eval->note_global = $eval->note_global+ $request->input('data')['bareme']/2;
              
                }
              if($request->input('data')['note'] =='NAQ' && $note->note == 'PA' ){
                $eval->note_global = $eval->note_global - $request->input('data')['bareme']/2;
                
                }
              if($request->input('data')['note'] =='SI' && $note->note == 'PA' ){
                $eval->note_global = $eval->note_global - $request->input('data')['bareme']/2;
              
                }
              if($request->input('data')['note'] =='PA' && $note->note == 'AQ' ){
                $eval->note_global = $eval->note_global - $request->input('data')['bareme']/2;
               
                }

              if($request->input('data')['note'] =='NAQ' && $note->note == 'AQ' ){
                $eval->note_global = $eval->note_global - $request->input('data')['bareme'];
                
                } 
               if($request->input('data')['note'] =='SI' && $note->note == 'AQ' ){
                $eval->note_global = $eval->note_global - $request->input('data')['bareme'];
                   
                }
               if($request->input('data')['note'] =='NA' && $note->note == 'AQ' ){
                $eval->note_global = $eval->note_global ;
                        
                }  

              if($request->input('data')['note'] =='AQ' && $note->note == 'NAQ' ){
                $eval->note_global = $eval->note_global + $request->input('data')['bareme'];
              
                }
              if($request->input('data')['note'] =='PA' && $note->note == 'NAQ' ){
                $eval->note_global = $eval->note_global + $request->input('data')['bareme']/2;
                    
                }
              if($request->input('data')['note'] =='NA' && $note->note == 'NAQ' ){
                $eval->note_global = $eval->note_global + $request->input('data')['bareme'];
                        
                }
              if($request->input('data')['note'] =='SI' && $note->note == 'NAQ' ){
                    $eval->note_global = $eval->note_global ;
                          
                }
             if($request->input('data')['note'] =='AQ' && $note->note == 'SI' ){
                $eval->note_global = $eval->note_global + $request->input('data')['bareme'];
                
                }
             if($request->input('data')['note'] =='PA' && $note->note == 'SI' ){
                $eval->note_global = $eval->note_global + $request->input('data')['bareme']/2;
                  
                }
             if($request->input('data')['note'] =='NA' && $note->note == 'SI' ){
                $eval->note_global = $eval->note_global + $request->input('data')['bareme'];
                       
                }
             if($request->input('data')['note'] =='NAQ' && $note->note == 'SI' ){
                $eval->note_global = $eval->note_global ;
                           
                }
         }

      
         
          
        $note->note = $request->input('data')['note'];
           

   
        $eval->save();
        $note->save();
        return $eval->note_global;
    }
    public function deleteEval(Request $request){
        Note::where('evaluation_id',$request->input('id'))->delete();
        Evaluation::find($request->input('id'))->delete();
    }
    public function myeval_agent(Request $req){
        $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
        if(isset($req->input('dateRange')[1])) {
            $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
        } else {
            $endDate = $startDate;
        }
        $user = auth('api')->user();

        // fetch by 'date_appel' if switchDate is true else fetch by created_at (=date_eval)
        if($req->switchDate == "true"){
            $date_param = 'date_appel';
        } else {
            $date_param = 'created_at';
        }
        // if the user is a sup or a client return only eval associated to his activity
        // if a user is an admin return all eval
        $agent=DB::table('agents')->where('email',$req->agentEmail)->first();
      
            $evals = Evaluation::whereIn('site', $req->input('selectedSites'))->where('agent_id',$agent->id)->whereBetween($date_param, [ $startDate, $endDate])->get();
     
        $data = array();
        foreach ($evals as $eval){
            $data[] = [
                'id'=>$eval->id,
                'note'=>$eval->note_global,
                'agent'=>$eval->agent->nom.' '.$eval->agent->prenom,
                'qualiticien'=>$eval->qualiticien->nom.' '.$eval->qualiticien->prenom,
                'service'=>$eval->grille->service->nom,
                'client_id'=>$eval->client_id,
                'date_appel'=>Carbon::createFromFormat('Y-m-d', $eval->date_appel)->format('d/m/Y'),//$eval->date_appel,
                'date_eval'=> Carbon::createFromFormat('Y-m-d H:i:s', $eval->created_at)->format('d/m/Y'),
            ];
        }
        return $data;
    }
    public function extraire(Request $req){
        $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
        if(isset($req->input('dateRange')[1])) {
            $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
        } else {
            $endDate = $startDate;
        }
        $user = auth('api')->user();
        if($req->switchDate == "true"){
            $date_param = 'date_appel';
        } else {
            $date_param = 'created_at';
        }
        if (empty($user->activite->toArray())) {
            $evals = Evaluation::whereIn('site', $req->input('selectedSites'))->whereBetween($date_param, [ $startDate, $endDate])->get();
        } else {
            // get activity id
            $activity = $user->activite[0];
            $grilles = $activity->services->map(function($service) {
                if($service->grille){
                    return $service->grille->id;
                }
            });
            $evals = Evaluation::whereIn('grille_id',$grilles->toArray())->whereIn('site', $req->input('selectedSites'))->whereBetween($date_param, [ $startDate, $endDate])->get();
        }


        $data = array();
        foreach ($evals as $eval){
            $data[] = [
                'id'=>$eval->id,
                'note'=>$eval->note_global,
                'agent'=>$eval->agent->nom.' '.$eval->agent->prenom,
                'qualiticien'=>$eval->qualiticien->nom.' '.$eval->qualiticien->prenom,
                'service'=>$eval->grille->service->nom,
                'client_id'=>$eval->client_id,
                'date_appel'=>Carbon::createFromFormat('Y-m-d', $eval->date_appel)->format('d/m/Y'),//$eval->date_appel,
                'date_eval'=> Carbon::createFromFormat('Y-m-d H:i:s', $eval->created_at)->format('d/m/Y'),
                'dap'=>($eval->dap) ? $eval->dap : ' ' ,
            ];
        }
        
        return Excel::download(new EvaluationExport($data), 'Evaluations.xlsx');
    }
    public function exportAgentNotes(Request $request){
        $data = array();
    foreach($request->data as $agent){
        $agent =  json_decode($agent, true);
    
      $data[]=[
            'agent'=>$agent['agent'],
            'indice_tel'=> $agent['indice_tel'],
            'moyenne'=>$agent['moyenne'],
            'nbr_eval'=> $agent['nbr_eval'],
            'semaine'=> $agent['semaine']
      ];
    }

    return Excel::download(new AgentNote($data), 'agentListeNotes.xlsx');
}
}
