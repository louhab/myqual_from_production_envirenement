<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use App\User;
class MailController extends Controller
{
        public function send(Request $request){
    
         $user= User::find($request->qual_id);
         
            Mail::to($user->email)->queue(new SendMail($request->subject,$request->content));
        }
}
