<?php

namespace App\Http\Controllers;

use App\CostumerFeedback;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use App\Agent;
use Storage;

class CostumerFeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $CostumerFeedback = new CostumerFeedback();
    
       if($request->fileClient !== 'undefined'){
        $fileNameClient =$request->nom.'_'.time().'.'.$request->fileClient->extension();
        $pathClient = $request->fileClient->storeAs('uploads/conststaClient/files', $fileNameClient, 'public');
        $CostumerFeedback->constat_url = $pathClient;
       }
       else {
        $CostumerFeedback->constat_url = '' ;
       }
       
    
     if($request->fileRouteurSup !==  null ){
        $fileNameSup =$request->nom.'_'.time().'.'.$request->fileRouteurSup->extension();
        $pathSup = $request->fileRouteurSup->storeAs('uploads/conststaRetourSup/files', $fileNameSup, 'public');
        $CostumerFeedback->retour_sup_url = $pathSup;
     }
     else {
        $CostumerFeedback->retour_sup_url = ' ' ;
     }
        
 
        $CostumerFeedback->activite_id =$request->activiteId;
        $CostumerFeedback->service_id =$request->serviceId;
        $CostumerFeedback->date_appel = $request->date;

        $CostumerFeedback->indice_tel= $request->indice;
       
        $CostumerFeedback->nom= $request->nom; 

        $CostumerFeedback->qualiticien_id = $request->qualiticien;
        $CostumerFeedback->type_constat = $request->constattype;
        $CostumerFeedback->site = $request->site;


        $CostumerFeedback->agent_id= $request->agentId;
        $CostumerFeedback->sup_id =json_decode($request->sup)->id;
        $CostumerFeedback->commentaire_constat = $request->commentaireConstat;
        $CostumerFeedback->retour_superviseur = $request->commentaireSup;
       
       

    
       
        $CostumerFeedback->save();
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CostumerFeedback  $costumerFeedback
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $CostumerFeedback = CostumerFeedback::find($id);


        return [
            'nom'=>$CostumerFeedback->nom,
            'activite_id'=>$CostumerFeedback->activite,
            'service_id'=>$CostumerFeedback->service,
            'date_appel'=>$CostumerFeedback->date_appel,
            'indice_tel'=>$CostumerFeedback->indice_tel,
            'qualiticien'=>User::find($CostumerFeedback->qualiticien_id),
            'agent'=>Agent::find($CostumerFeedback->agent_id),
            'type_constat'=>$CostumerFeedback->type_constat,
            'commentaire_constat'=>$CostumerFeedback->commentaire_constat,
            'retour_superviseur'=>$CostumerFeedback->retour_superviseur,
            'constat_client_url'=>asset(\Storage::url($CostumerFeedback->constat_url)),
            'retour_sup_url'=>asset(\Storage::url($CostumerFeedback->retour_sup_url)),
            'site'=>$CostumerFeedback->site,
            'client_id'=>$CostumerFeedback->client_id,
            'sup'=>User::find($CostumerFeedback->sup_id),

        ] ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CostumerFeedback  $costumerFeedback
     * @return \Illuminate\Http\Response
     */
    public function edit(CostumerFeedback $costumerFeedback)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CostumerFeedback  $costumerFeedback
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CostumerFeedback $costumerFeedback)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CostumerFeedback  $costumerFeedback
     * @return \Illuminate\Http\Response
     */
    public function destroy(CostumerFeedback $costumerFeedback)
    {
        //
    }
    public function myliste(Request $request){
 
        $CostumerFeedback = CostumerFeedback::all();
        foreach($CostumerFeedback as $feedback){
            $data[]=[
                    'id'=>$feedback->id,
                    'activite'=>$feedback->activite->nom,
                    'service'=>$feedback->service->nom,
                    'nom'=>$feedback->nom,
                    'agent'=>$feedback->agent->nom.' '.$feedback->agent->prenom,
                    'qualiticien'=>$feedback->user->nom.' '.$feedback->user->prenom,
                    'date'=> Carbon::createFromFormat('Y-m-d H:i:s', $feedback->created_at)->format('d/m/Y'),

            ];
        }
        return $data;
    }
}
