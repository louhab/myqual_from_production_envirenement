<?php

namespace App\Http\Controllers;

use App\Task_user;
use App\Task;
use App\Task_type;
use App\User;
use App\Activite;
use App\Exports\TaskUserExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;

class TaskUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if(strlen ( $request->date)>10) {
        $start_day = strtotime( $request->date[0].$request->date[1].'-'.$request->date[3].$request->date[4].'-'.$request->date[6].$request->date[7].$request->date[8].$request->date[9]);
        $end_day = strtotime($request->date[11].$request->date[12].'-'.$request->date[14].$request->date[15].'-'.$request->date[17].$request->date[18].$request->date[19].$request->date[20]);
      
        $user=auth('api')->user();

        $task_user = new Task_user();

        $task_user->task_id= $request->task_type;
        $task_user->cat_id= $request->task;
        $task_user->nom= $request->nom;
        $task_user->user_id = $user->id;
        $task_user->commentaire= $request->comment;
        $task_user->startDate= date('Y-m-d H:i:s',$start_day);

        $task_user->endDate = date('Y-m-d H:i:s',$end_day);
        $task_user->actvity_id= $request->activity;
        
 return  $task_user->save();
    }
    else {
        $start_day = strtotime( $request->date[0].$request->date[1].'-'.$request->date[3].$request->date[4].'-'.$request->date[6].$request->date[7].$request->date[8].$request->date[9]);
      
      
        $user=auth('api')->user();

        $task_user = new Task_user();

        $task_user->task_id= $request->task_type;
        $task_user->cat_id= $request->task;
        $task_user->nom= $request->nom;
        $task_user->user_id = $user->id;
        $task_user->commentaire= $request->comment;
        $task_user->startDate= date('Y-m-d H:i:s',$start_day);

        $task_user->endDate = date('Y-m-d H:i:s',$start_day);
        $task_user->actvity_id= $request->activity;
        
 return  $task_user->save();
    }

        
     
       

      
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task_user  $task_user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
  
       $task =Task_user::where('id',$id)->where('user_id',auth('api')->user()->id)->first();

     

       $data=[
                
        'id'=>$task->id,
'catgorie'=>Task::find($task->cat_id)->id,
'actvite'=>Activite::find($task->actvity_id)->id,
'tache'=>
 Task_type::find($task->task_id)->id,
'date'=>DateTime::createFromFormat('Y-m-d', $task->endDate)->format('d/m/Y') ,
'endDate'=>DateTime::createFromFormat('Y-m-d', $task->startDate)->format('d/m/Y') ,
'comment'=>$task->commentaire
    ];
 return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task_user  $task_user
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $task_user =Task_user::where('id',$id)->where('user_id',auth('api')->user()->id)->first();

        if(strlen ( $request->date)>10) {
            $start_day = strtotime( $request->date[0].$request->date[1].'-'.$request->date[3].$request->date[4].'-'.$request->date[6].$request->date[7].$request->date[8].$request->date[9]);
            $end_day = strtotime($request->date[11].$request->date[12].'-'.$request->date[14].$request->date[15].'-'.$request->date[17].$request->date[18].$request->date[19].$request->date[20]);
          
            $user=auth('api')->user();
    
       
    
            $task_user->task_id= $request->task_type;
            $task_user->cat_id= $request->task;
            $task_user->nom= $request->nom;
            $task_user->user_id = $user->id;
            $task_user->commentaire= $request->comment;
            $task_user->startDate= date('Y-m-d H:i:s',$start_day);
    
            $task_user->endDate = date('Y-m-d H:i:s',$end_day);
            $task_user->actvity_id= $request->activity;
            
     return  $task_user->save();
        }
        else {
            $start_day = strtotime( $request->date[0].$request->date[1].'-'.$request->date[3].$request->date[4].'-'.$request->date[6].$request->date[7].$request->date[8].$request->date[9]);
          
          
            $user=auth('api')->user();

    
            $task_user->task_id= $request->task_type;
            $task_user->cat_id= $request->task;
            $task_user->nom= $request->nom;
            $task_user->user_id = $user->id;
            $task_user->commentaire= $request->comment;
            $task_user->startDate= date('Y-m-d H:i:s',$start_day);
    
            $task_user->endDate = date('Y-m-d H:i:s',$start_day);
            $task_user->actvity_id= $request->activity;
            
     return  $task_user->save();
        }
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task_user  $task_user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        return $request;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task_user  $task_user
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $task =Task_user::find($id);
        if($task->delete()){
            return response()->json([
                '200'
            ]);
        }
    }
    public function get_tache(Request $request){



if($request->activity !==  null &&  $request->startDate !== null ){

    $user=auth('api')->user();
    $start_day = $request->startDate[0].$request->startDate[1].'-'.$request->startDate[3].$request->startDate[4].'-'.$request->startDate[6].$request->startDate[7].$request->startDate[8].$request->startDate[9];
  //  $start_day =  DateTime::createFromFormat('d/m/Y', $request->startDay)->format('Y-m-d');
  //   $end_day = DateTime::createFromFormat('d/m/Y', $request->endDay)->format('Y-m-d');
   $end_day= $request->startDate[11].$request->startDate[12].'-'.$request->startDate[14].$request->startDate[15].'-'.$request->startDate[17].$request->startDate[18].$request->startDate[19].$request->startDate[20] ;
        $task_user = Task_user::where('user_id',$user->id)->where('actvity_id', json_decode($request->activity)->id)->get();
    

}

else {

    $first_day_mont =date('Y-m-01');
 
    $end_day=date('Y-m-d');
 
    $user=auth('api')->user();
    $task_user = Task_user::where('user_id',$user->id)->where('startDate','<=',$first_day_mont)->get();
   
   
}


     
      $data = [];
        
        if($task_user ->count()== 0) return 0;
     
        foreach($task_user as $task ){
       
               
      
            
            
  
                $data[]=[
                
                    'id'=>$task->id,
            'catgorie'=>Task::find($task->cat_id)->nom,
            'actvite'=>Activite::find($task->actvity_id)->nom,
            'tache'=>
            $task->nom != '' ?  Task_type::find($task->task_id)->nom .'('. $task->nom .')' : Task_type::find($task->task_id)->nom ,
            'date'=>$task->endDate ,
            'endDate'=>$task->startDate,
            'comment'=>$task->commentaire
                ];
                $act[] =[
                    'activite'=>Activite::find($task->actvity_id)->nom,
                ]; 
            
          
            
          
        }
        return $data;
    }
    public function get_tasks_users() {
        $tasks_user = Task_user::all();
        foreach($tasks_user as $task_user) {
            $data[] =[
                'id'=>$task_user->id,
                'date'=>$task_user->endDate ,
                'endDate'=>$task_user->startDate,
'catgorie'=>Task::find($task_user->cat_id)->nom,
'tache'=>
$task_user->nom != '' ?  Task_type::find($task_user->task_id)->nom .'('. $task_user->nom .')' : Task_type::find($task_user->task_id)->nom ,
'user'=>User::find($task_user->user_id)->nom . ' '. User::find($task_user->user_id)->prenom
            ];
        }
        return $data;
    } 
    public function get_task(Request $request) {

        $start_day =  DateTime::createFromFormat('d/m/Y', $request->dateRange[0])->format('Y-m-d');
        $end_day = DateTime::createFromFormat('d/m/Y', $request->dateRange[1])->format('Y-m-d');
      
        $qual_id =$request->qualiticien;
        $tasks_user =  Task_user::whereBetween('startDate',[$start_day,$end_day])->where('user_id',$request->qualiticien)->get();


        if($tasks_user->count()== 0) return response()->json('no data found');
        foreach($tasks_user as $task_user) {
            $data[] =[
                'id'=>$task_user->id,
                'date'=>$task_user->endDate ,
                'endDate'=>$task_user->startDate,
'catgorie'=>Task::find($task_user->cat_id)->nom,
'tache'=>
$task_user->nom != '' ?  Task_type::find($task_user->task_id)->nom .'('. $task_user->nom .')' : Task_type::find($task_user->task_id)->nom ,
'user'=>User::find($task_user->user_id)->nom . ' '. User::find($task_user->user_id)->prenom
            ];
        }
        return $data;
       

    }
    public function getQualiticiens() {
        $qualiticiens =Task_user::all();


        foreach($qualiticiens as $qualiticien) {
            $data[]= [

'id'=>$qualiticien->user_id,

            ];
           $data =array_values(array_unique($data, SORT_REGULAR ));
        }
        foreach( $data as $d) {
            $var[]= [
                'id'=>$d['id'],

                'nom' =>User::find($d['id'])->nom.' '.User::find($d['id'])->prenom
            ];
        }
        return $var;
       return array_values(array_unique($data, SORT_REGULAR ));
    
    }
    public function exportTasks(){
        return Excel::download(new TaskUserExport, 'les_taches.xlsx');
    }
   public function reporting(Request $request) {

       $startDate = DateTime::createFromFormat('d/m/Y', $request->input('dateRange')[0])->format('Y-m-d');
       if(isset($request->input('dateRange')[1])) {
        $endDate = DateTime::createFromFormat('d/m/Y', $request->input('dateRange')[1])->format('Y-m-d');
    } else {
        $endDate = $startDate;
    }
       $tasks =Task_user::where('actvity_id',$request->activityId)->whereBetween('startDate',[$startDate,$endDate])->get();
       if($tasks->count()==0) return response()->json(0);
  
    foreach($tasks as $task_user){
        $data[] =[
            'id'=>$task_user->id,
            'date'=>$task_user->endDate ,
            'endDate'=>$task_user->startDate,
            'catgorie'=>Task::find($task_user->cat_id)->nom,
            'tache'=>$task_user->nom != '' ?  Task_type::find($task_user->task_id)->nom .'('. $task_user->nom .')' : Task_type::find($task_user->task_id)->nom ,
            'user'=>User::find($task_user->user_id)->nom . ' '. User::find($task_user->user_id)->prenom
        ];

    }

    $data = collect($data);
   
  $userscharts_task=$this->countd($data);
         $tasks_user = $data->groupBy( function ($task) {
        return $task['user'];
    });
 
  
    return [$tasks_user,$userscharts_task];
   }
  
   public function countd($tasks){
  
        // this function iw working correctly don't touch it
        $tasks_user = $tasks->groupBy( function ($task) {
            return $task['catgorie'];
        });

        $data = [];
        foreach($tasks_user as $key=>$task_user){
        foreach ($task_user as $key=>$task){
            
        $counformation[$task['user']]=0;
        $countpassgeterrain[$task['user']]=0;
        $counttratementrdv[$task['user']] =0;
        $countcomite[$task['user']] = 0;
        $countevaluation[$task['user']] =0;
        $countcalibrage[$task['user']] =0;
        $countautretache[$task['user']] =0;
        if( $task['catgorie']=== 'Formation' ){
            $counformation[$task['user']] +=1;
        
        }
        if( $task['catgorie'] === 'passage terrain et accompagnement ' ){
            $countpassgeterrain[$task['user']] +=1;
        
        }
        if( $task['catgorie'] === 'Traitement des Rdv' ){
            $counttratementrdv[$task['user']] +=1;
        
        }
        if( $task['catgorie'] === 'Comité ' ){
            $countcomite[$task['user']] +=1;
        
        }
        if( $task['catgorie'] === 'Evaluations' ){
            $countevaluation[$task['user']] +=1;
        
        }
        if( $task['catgorie'] === 'Calibrage' ){
            $countcalibrage[$task['user']] +=1;
        
        }
        if( $task['catgorie'] === 'Autres Taches' ){
            $countautretache[$task['user']] +=1;
        
        }

        $countelements[$task['user']][] =[
            
            
     'counformation'=>     $counformation[$task['user']],
         'countpassgeterrain'=>   $countpassgeterrain[$task['user']],
        'counttratementrdv'=>   $counttratementrdv[$task['user']],
      'countcomite'=>  $countcomite[$task['user']],
   'countcalibrage'=>$countcalibrage[$task['user']],
'countautretache'=>$countautretache[$task['user']]
] ;


            
        }
        }
    foreach($countelements as $key=> $elment ){
        
         
           $data2[$key] =[
            array_sum(array_column($elment, 'counformation')),
        array_sum(array_column($elment, 'countpassgeterrain')),
             array_sum(array_column($elment, 'counttratementrdv')),
         array_sum(array_column($elment, 'countcomite')),

           array_sum(array_column($elment, 'countcalibrage')),
            array_sum(array_column($elment, 'countautretache')),
              ];
        
    }
    return $data2;
}
}

