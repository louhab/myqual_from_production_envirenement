<?php

namespace App\Http\Controllers;

use App\EluEscda;
use App\NoteEluEscda;
use App\Agent;
use App\Appel;
use Carbon\Carbon;
use App\Service;
use App\Notifications\EvalNotification;
use Illuminate\Http\Request;
use DateTime;

class EluEscdaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllEvals(Request $req) {

        $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
        if(isset($req->input('dateRange')[1])) {
            $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
        } else {
            $endDate = $startDate;
        }
        $user = auth('api')->user();
        $evals = EluEscda::all();
        // $evals = EluEscda::whereBetween('created_at', [ $startDate, $endDate])->get();

    

        $data = array();
        foreach ($evals as $eval){
            $data[] = [
                'id'=>$eval->id,
                'note'=>$eval->note_global,
                'agent'=>$eval->agent->nom.' '.$eval->agent->prenom,
                'qualiticien'=>$eval->qualiticien->nom.' '.$eval->qualiticien->prenom,
                'service'=>$eval->grille->service->nom,
                'activity'=>$eval->grille->service->activite->nom,
                'client_id'=>$eval->client_id,
                'date_appel'=>Carbon::createFromFormat('Y-m-d', $eval->date_appel)->format('d/m/Y'),//$eval->date_appel,
                'date_eval'=> Carbon::createFromFormat('Y-m-d H:i:s', $eval->created_at)->format('d/m/Y'),
            ];
        }
        return $data;
    }
    public function getAllEvals2(Request $req) {  
        $user = auth('api')->user();
        $evals = Appel::all();
        $data = array();
        foreach ($evals as $eval){
            $data[]= [
                'service'=>$appel->service->nom,
                'activite'=>$appel->activite->nom,
                'agent'=>$appel->agent->nom.''.$appel->agent->prenom,
             ];
        }
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id){

        $service = Service::find($id);  //service id
        // test if serve exists then if the 'grille' exists

        //preparing 'grille' elements
        $grille = $service->grille;

        $elpartmap = array();
        foreach($grille->elements as $element){
            $partie = $element->partie->nom;
            $el = ['id'=>$element->pivot->id,'nom'=>$element->nom,'g_ok'=>$element->pivot->ok,'g_pa'=>$element->pivot->pa,
            'g_ko'=>$element->pivot->ko,'g_si'=>$element->pivot->si,'g_na'=>$element->pivot->na,'bareme'=>$element->pivot->bareme];
            $testkey = null;

            foreach ($elpartmap as $key => $val) {
                if ($val['partie'] === $partie) {
                    $testkey = $key;
                    break;
                }
            }
            if(isset($testkey)){
                $elpartmap[$testkey]['elements'][] = $el;
            }else{
                $elpartmap[]=['partie'=>$partie,'elements'=>[$el]];
            }
        }

        //get all agents associated to this service
        // $agents = $service->agents; this should be sent with activities and services
         /* $agents = DB::table('agents')->select('id', 'nom','prenom','matricule')->where([
            ['statut','1'],
            ['service_id', $service->id],
        ])->get(); */

        return  $elpartmap;
    }
    public function getEval($id){
        $eval = EluEscda::find($id);
        $elements = self::create($eval->grille->service->id);


        foreach ($elements as &$item){
            foreach ($item['elements'] as &$element){
              if(  $note = NoteEluEscda::select('note','comment')->where(['escda_id'=>$id,'element_id'=>$element['id']])->first()) {
         
                $element['note'] = $note->note;
                $element['comment'] = $note->comment;
              }
              else {
               
                $note = NoteEluEscda::select('note','comment')->where(['escda_id'=>$id])->firstOrFail();
            
            

                $element['note'] = $note->note;
                $element['comment'] = $note->comment;
              }

              
            }
        }
        $data = array();
        $data['eval'] = $eval;
        $data['elements'] = $elements;

       return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $agent_user= Agent::find($request->agentId);
  
          $request->validate([
              // add validation
          ]);
          $val = new EluEscda();
  
          $serviceId = $request->input('ervice_id');
  
          $items = $request->input('items');
    
          $val->grille_id = Service::find($serviceId)->grille->id;
          $val->client_id = $request->input('clientId');
          $val->indice_tel = $request->input('indice');
          $val->pcc = $request->input('pcc');
          $val->satcli = $request->input('satcli');
          $val->date_appel = $request->input('date');
          $val->oad = $request->input('oad');
          $val->exci = $request->input('exci');
          $val->synthese = $request->input('synthese');
          $val->agent_id = $request->input('agentId');
          $val->semaine = $request->input('weekofyear');
          $val->site = $agent_user->site;
          //if($request->input('role') == 'admin')  $val->qualiticien_id = 8 ;
          $val->qualiticien_id = $request->input('qualiticien');
          $val->save();
  
          $note_global = 0;
          $bareme_global = 0;
          $bareme_element_where_na =0;
          $bareme_element_where_AQ = 0;
          foreach ($items as $item){
              foreach ($item['elements'] as $element){ 
                  $note_array[]= $element['note'];
                  if($element['note'] == 'NA')  {
                      $bareme_element_where_na +=$element['bareme'];
                  }
                  if($element['note'] == 'AQ')  {
                      $bareme_element_where_AQ +=1;
                  }
                 
              }
          }
          $note_elements = [];
          $note_cal = 0;
          

      foreach ($items as $item){
          foreach ($item['elements'] as $element){
  
              $note = new NoteEluEscda();
              $note->escda_id = $val->id;
              $note->element_id=$element['id'];
              $note->note=$element['note'];
           
  
              if( $note->note == 'AQ' ){
                  if($bareme_element_where_na!=0){
                      $note_global += $element['bareme'] + ($bareme_element_where_na/$bareme_element_where_AQ);
                  }
                  else {
                      $note_global += $element['bareme'] ;
                  }
                  // note_global val 
              }
              if( $note->note == 'PA'  ){
                  if($bareme_element_where_na!=0){
                  $note_global += ($element['bareme']/2) ;
                  }
                  else {
                      $note_global += ($element['bareme']/2) ;
                  }
              }
              if(isset($element['comment'])){
                  $note->comment=$element['comment'];
              }
              $note->save();
              $bareme_global += $element['bareme'];
           
          }
      }
      
  
  
      
        
  
        
  
  
 
  
          if($request->input('sim')){
              $val->note_global = 0;
              $val->save();
              if( $agent_user->notify(new EvalNotification($val))){
                  return 1;
                 }
          }
  
          $val->note_global = round($note_global, 2);
          $val->save(); // note_global val 
          if( $agent_user->notify(new EvalNotification($val))){
              return 1;
             }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EluEscda  $eluEscda
     * @return \Illuminate\Http\Response
     */
    public function show(EluEscda $eluEscda)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EluEscda  $eluEscda
     * @return \Illuminate\Http\Response
     */
    public function edit(EluEscda $eluEscda)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EluEscda  $eluEscda
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EluEscda $eluEscda)
    {
        //
    }
    public function myliste(Request $req){
        $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
        if(isset($req->input('dateRange')[1])) {
            $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
        } else {
            $endDate = $startDate;
        }
        $user = auth('api')->user();
        // ->whereBetween('created_at', [ $startDate, $endDate])
        $evals = EluEscda::where('qualiticien_id', $user->id)->whereIn('site', $req->input('selectedSites'))->get();
      

        $data = array();
        foreach ($evals as $eval){
            $data[] = [
                'id'=>$eval->id,
                'note'=>$eval->note_global,
                'agent'=>$eval->agent->nom.' '.$eval->agent->prenom,
                'qualiticien'=>$eval->qualiticien->nom.' '.$eval->qualiticien->prenom,
                'activity'=>$eval->grille->service->activite->nom,
                'service'=>$eval->grille->service->nom,
                'client_id'=>$eval->client_id,
                'date_appel'=>Carbon::createFromFormat('Y-m-d', $eval->date_appel)->format('d/m/Y'),//$eval->date_appel,
                'date_eval'=> Carbon::createFromFormat('Y-m-d H:i:s', $eval->created_at)->format('d/m/Y'),
            ];
        }
        return $data; 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EluEscda  $eluEscda
     * @return \Illuminate\Http\Response
     */
    public function destroy(EluEscda $eluEscda)
    {
        //
    }
}
