<?php

namespace App\Http\Controllers;

use App\NoteCalibrage;
use Illuminate\Http\Request;

class NoteCalibrageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\NoteCalibrage  $noteCalibrage
     * @return \Illuminate\Http\Response
     */
    public function show(NoteCalibrage $noteCalibrage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NoteCalibrage  $noteCalibrage
     * @return \Illuminate\Http\Response
     */
    public function edit(NoteCalibrage $noteCalibrage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\NoteCalibrage  $noteCalibrage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NoteCalibrage $noteCalibrage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NoteCalibrage  $noteCalibrage
     * @return \Illuminate\Http\Response
     */
    public function destroy(NoteCalibrage $noteCalibrage)
    {
        //
    }
}
