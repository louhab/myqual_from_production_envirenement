<?php

namespace App\Http\Controllers;

use App\LaredouteEscda;
use App\NoteLaredouteEscda;
use App\Service;
use App\Agent;
use DateTime;
use Carbon\Carbon;
use App\Notifications\EvalNotification;
use Illuminate\Http\Request;

class LaredouteEscdaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function getAllEvals(Request $req) {

        $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
        if(isset($req->input('dateRange')[1])) {
            $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
        } else {
            $endDate = $startDate;
        }
        $user = auth('api')->user();
 

        // fetch by 'date_appel' if switchDate is true else fetch by created_at (=date_eval)
        if($req->switchDate == "true"){
            $date_param = 'date_appel';
        } else {
            $date_param = 'created_at';
        }
        // if the user is a sup or a client return only eval associated to his activity
        // if a user is an admin return all eval
        if (empty($user->activite->toArray())) {
            $evals = LaredouteEscda::all();
        } else {
            // get activity id
            $activity[] = $user->activite;
      
          foreach($activity as $act ){
            foreach($act as $a){
                
                $grilles[] = $a->services->map(function($service) {
                    if($service->grille){
                        return $service->grille->id;
                    }
                });
            }
          }
         foreach(  $grilles as $g){
            $evals2[] = LaredouteEscda::all();
         }
          
        }

        $data = array();
        if(!empty($evals2) ){
            foreach($evals2 as $eval2){
                foreach ($eval2 as $eval){
                    $data[] = [
                        'id'=>$eval->id,
                        'note'=>$eval->note_global,
                        'agent'=>$eval->agent->nom.' '.$eval->agent->prenom,
                        'qualiticien'=>$eval->qualiticien->nom.' '.$eval->qualiticien->prenom,
                        'service'=>$eval->grille->service->nom,
                        'activity'=>$eval->grille->service->activite->nom,
                        'client_id'=>$eval->client_id,
                        'date_appel'=>Carbon::createFromFormat('Y-m-d', $eval->date_appel)->format('d/m/Y'),//$eval->date_appel,
                        'date_eval'=> Carbon::createFromFormat('Y-m-d H:i:s', $eval->created_at)->format('d/m/Y'),
                    ];
                }
            }
        }
      
      else {
        foreach ($evals as $eval){
            $data[] = [
                'id'=>$eval->id,
                'note'=>$eval->note_global,
                'agent'=>$eval->agent->nom.' '.$eval->agent->prenom,
                'qualiticien'=>$eval->qualiticien->nom.' '.$eval->qualiticien->prenom,
                'service'=>$eval->grille->service->nom,
                'activity'=>$eval->grille->service->activite->nom,
                'client_id'=>$eval->client_id,
                'date_appel'=>Carbon::createFromFormat('Y-m-d', $eval->date_appel)->format('d/m/Y'),//$eval->date_appel,
                'date_eval'=> Carbon::createFromFormat('Y-m-d H:i:s', $eval->created_at)->format('d/m/Y'),
            ];
        }
      }
        return $data;
    }
    public function getEval($id){
        $eval = LaredouteEscda::find($id);
        $elements = self::create($eval->grille->service->id);


        foreach ($elements as &$item){
            foreach ($item['elements'] as &$element){
              if(  $note = NoteLaredouteEscda::select('note','comment')->where(['laredoute_escda_id'=>$id,'element_id'=>$element['id']])->first()) {
         
                $element['note'] = $note->note;
                $element['comment'] = $note->comment;
              }
              else {
               
                $note = NoteLaredouteEscda::select('note','comment')->where(['laredoute_escda_id'=>$id])->firstOrFail();
            
            

                $element['note'] = $note->note;
                $element['comment'] = $note->comment;
              }

              
            }
        }
        $data = array();
        $data['eval'] = $eval;
        $data['elements'] = $elements;

       return $data;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id){

        $service = Service::find($id);  //service id
        // test if serve exists then if the 'grille' exists

        //preparing 'grille' elements
        $grille = $service->grille;

        $elpartmap = array();
        foreach($grille->elements as $element){
            $partie = $element->partie->nom;
            $el = ['id'=>$element->pivot->id,'nom'=>$element->nom,'g_ok'=>$element->pivot->ok,'g_pa'=>$element->pivot->pa,
            'g_ko'=>$element->pivot->ko,'g_si'=>$element->pivot->si,'g_na'=>$element->pivot->na,'bareme'=>$element->pivot->bareme];
            $testkey = null;

            foreach ($elpartmap as $key => $val) {
                if ($val['partie'] === $partie) {
                    $testkey = $key;
                    break;
                }
            }
            if(isset($testkey)){
                $elpartmap[$testkey]['elements'][] = $el;
            }else{
                $elpartmap[]=['partie'=>$partie,'elements'=>[$el]];
            }
        }

        //get all agents associated to this service
        // $agents = $service->agents; this should be sent with activities and services
         /* $agents = DB::table('agents')->select('id', 'nom','prenom','matricule')->where([
            ['statut','1'],
            ['service_id', $service->id],
        ])->get(); */

        return  $elpartmap;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $agent_user= Agent::find($request->agentId);
  
          $request->validate([
              // add validation
          ]);
          $val = new LaredouteEscda();
  
          $serviceId = $request->input('ervice_id');
  
          $items = $request->input('items');
    
          $val->grille_id = Service::find($serviceId)->grille->id;
          $val->client_id = $request->input('clientId');
          $val->indice_tel = $request->input('indice');
          $val->pcc = $request->input('pcc');
          $val->satcli = $request->input('satcli');
          $val->date_appel = $request->input('date');
          $val->oad = $request->input('oad');
          $val->exci = $request->input('exci');
          $val->synthese = $request->input('synthese');
          $val->agent_id = $request->input('agentId');
          $val->semaine = $request->input('weekofyear');
          $val->site = $agent_user->site;
          //if($request->input('role') == 'admin')  $val->qualiticien_id = 8 ;
          $val->qualiticien_id = $request->input('qualiticien');
          $val->dap = $request->dap;
          $val->save();
  
          $note_global = 0;
          $bareme_global = 0;
          $bareme_element_where_na =0;
          $bareme_element_where_AQ = 0;
   
          $note_elements = [];
          $note_cal = 0;
          

      foreach ($items as $item){
          foreach ($item['elements'] as $element){
  
              $note = new NoteLaredouteEscda();
              $note->laredoute_escda_id = $val->id;
              $note->element_id=$element['id'];
              $note->note=$element['note'];
           
  
              if( $note->note == 'AQ' ){
                $note_global += $element['bareme'] ;     
              }
              if( $note->note == 'PA'  ){
                  $note_global += 12 ; 
              }
              if(  $note->note == 'NAQ'){
                $note_global += 9;
            }
        
            if(isset($element['comment'])){
                $note->comment=$element['comment'];
            }
            $note->save();
            $bareme_global += $element['bareme'];
              if(isset($element['comment'])){
                  $note->comment=$element['comment'];
              }
              $note->save();
              $bareme_global += $element['bareme'];
           
          }
      }
      
  
  
      
        
  
        
  
  
 
  
          if($request->input('sim')){
              $val->note_global = 0;
              $val->save();
              if( $agent_user->notify(new EvalNotification($val))){
                  return 1;
                 }
          }
  
          $val->note_global = round($note_global, 2);
          $val->save(); // note_global val 
          if( $agent_user->notify(new EvalNotification($val))){
              return 1;
             }    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LaredouteEscda  $laredouteEscda
     * @return \Illuminate\Http\Response
     */
    public function show(LaredouteEscda $laredouteEscda)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LaredouteEscda  $laredouteEscda
     * @return \Illuminate\Http\Response
     */
    public function edit(LaredouteEscda $laredouteEscda)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LaredouteEscda  $laredouteEscda
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LaredouteEscda $laredouteEscda)
    {
        //
    }
    public function myliste(Request $req){
        $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
        if(isset($req->input('dateRange')[1])) {
            $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
        } else {
            $endDate = $startDate;
        }
        $user = auth('api')->user();
        // ->whereBetween('created_at', [ $startDate, $endDate])
        $evals = LaredouteEscda::where('qualiticien_id', $user->id)->whereIn('site', $req->input('selectedSites'))->get();
      

        $data = array();
        foreach ($evals as $eval){
            $data[] = [
                'id'=>$eval->id,
                'note'=>$eval->note_global,
                'agent'=>$eval->agent->nom.' '.$eval->agent->prenom,
                'qualiticien'=>$eval->qualiticien->nom.' '.$eval->qualiticien->prenom,
                'activity'=>$eval->grille->service->activite->nom,
                'service'=>$eval->grille->service->nom,
                'client_id'=>$eval->client_id,
                'date_appel'=>Carbon::createFromFormat('Y-m-d', $eval->date_appel)->format('d/m/Y'),//$eval->date_appel,
                'date_eval'=> Carbon::createFromFormat('Y-m-d H:i:s', $eval->created_at)->format('d/m/Y'),
            ];
        }
        return $data; 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LaredouteEscda  $laredouteEscda
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request){
        NoteLaredouteEscda::where('laredoute_escda_id',$request->input('id'))->delete();
        LaredouteEscda::find($request->input('id'))->delete();
    }
}
