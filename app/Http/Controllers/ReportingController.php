<?php

namespace App\Http\Controllers;
use App\Escda;
use App\Activite;
use App\Agent;
use App\Evaluation;
use App\EluEscda;
use App\LaredouteEscda;
use App\User;
use App\Service;
use App\ElementGrille;
use App\Grille;
use App\Note;
use App\Calibrage;
use App\Constat;
use Carbon\Carbon;
use App\NoteEscda;
use Illuminate\Http\Request;
use App\NoteCalibrage;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use DateTime;

class ReportingController extends Controller
{
    /*
     * make a function for each
     * 'page'
     */
    // home page
    public function home(){

        // add where with date
        // for the home page we will always show the data for the current week
        // /* created_at between monday and friday */

        $thismonday = date('Y-m-d', strtotime('Monday this week'));
        $thissunday = date('Y-m-d', strtotime('Sunday this week'));

        $first_day_mont =date('Y-m-01');
        $a_date = $first_day_mont;
        $last_dat_mont =date("Y-m-t", strtotime($a_date));
        // $evals = Evaluation::whereBetween('created_at', [$thismonday, $thissunday])->get();
        $evals = Evaluation::whereBetween('created_at', [$first_day_mont, $last_dat_mont])->get();

        $eval_ef = $evals->where('note_global','!=','0')->count();
        $eval_sim = $evals->where('note_global','0')->count();
        $eval_par_activite = $this->evalParActivite($evals);
        $moyenne_par_activite =$this->moyenneParActivite($evals);
        $moyenne = $this->moyenne($evals);
        $data = ['eval_ef'=>$eval_ef,'eval_sim'=>$eval_sim,'moyenne'=> $moyenne,
            'moyenne_par_activite'=>$moyenne_par_activite,'eval_par_activite'=>$eval_par_activite];

        return $data;
    }

     // reporting - > global page
    public function reportingGlobal(Request $req){
        // get date
        $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
        if(isset($req->input('dateRange')[1])) {
            $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
        } else {
            $endDate = $startDate;
        }
        $users =  User::whereHas("roles", function($q){ $q->where("name", "qualiticien"); })->get();
        foreach($users as $user){
            $idqualiticiens[]= $user->id;
        }
        if($req->switchDate == "true"){
            $date_param = 'date_appel';
        } else {
            $date_param = 'created_at';
        }
        if($req->switchQuality == "true"){
            $evals = Evaluation::whereIn('site', $req->input('selectedSites'))->whereIn('qualiticien_id',$idqualiticiens)->whereBetween($date_param, [ $startDate, $endDate])->get();
        } else {
            $evals = Evaluation::whereIn('site', $req->input('selectedSites'))->whereBetween($date_param, [ $startDate, $endDate])->get();
        }

        $eval_ef = $evals->where('note_global','!=','0')->count();
        $eval_sim = $evals->where('note_global','0')->count();
        $eval_par_activite = $this->evalParActivite($evals);
        $moyenne_par_activite =$this->moyenneParActivite($evals);
        $moyenne = $this->moyenne($evals);
        $eval_par_qualiticien = $this->evalParQualiticien($evals);

       
    
        $data = [
            'eval_ef'=>$eval_ef,
            'eval_sim'=>$eval_sim,
            'moyenne'=> $moyenne,
            'moyenne_par_activite'=>$moyenne_par_activite,
            'eval_par_activite'=>$eval_par_activite,
             'eval_par_qualiticien' => $eval_par_qualiticien,
            
          ];

        return $data;
    }

    // reporting - Activity page
    public function reportingActivity(Request $req){
     
        $user = auth('api')->user();
        $activiteId = (!empty($user->activite->toArray()) ? $user->activite[0]->id : null);
        // if a sup or client try to get reporting of an other activity return only his activity's reporting
        if($activiteId == null) {
            // not a client nor a sup
            $activity = Activite::findOrFail($req->input('activityId'));
        } else {
            $activity = Activite::findOrFail($activiteId);
        }
        $services = [];
        $grille = [];
        foreach($activity->services as $service){
            if($service->grille){
                $grille[] = $service->grille->id;
            }
            if($service->nom!== 'ALC'){
                $services[] = $service->nom;
            }
           
        }
        $users =  User::whereHas("roles", function($q){ $q->where("name", "qualiticien"); })->get();
        foreach($users as $user){
            $idqualiticiens[]= $user->id;
        }

        // check if range date or year
        $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
        if(isset($req->input('dateRange')[1])) {
            $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
        } else {
            $endDate = $startDate;
        }
        // fetch by 'date_appel' if switchDate is true else fetch by created_at (=date_eval)
        if($req->switchDate == "true"){
            $date_param = 'date_appel';
        } else {
            $date_param = 'created_at';
        }
        if($req->switchQuality == "true"){
            // // if($req->activityId ==3){
            //     $evals2 = Escda::whereIn('site', $req->input('selectedSites'))->whereBetween($date_param , [ $startDate, $endDate])->get();
             
            // }
            // else {
            //     $evals2 = null;
            // }
            $evals = Evaluation::whereIn('site', $req->input('selectedSites'))->whereIn('grille_id', $grille)->whereIn('qualiticien_id',$idqualiticiens)->whereBetween($date_param, [ $startDate, $endDate])->get();
        } else {
            // if($req->activityId ==3){
            //     $evals2 = Escda::whereIn('site', $req->input('selectedSites'))->whereBetween($date_param , [ $startDate, $endDate])->get();
           
            // }
            // else {
            //     $evals2 = null;
            // }
            $evals = Evaluation::whereIn('site', $req->input('selectedSites'))->whereIn('grille_id', $grille)->whereBetween($date_param, [ $startDate, $endDate])->get();
        }
  
        
       
        $moyenne = $this->moyenne($evals);
        $eval_ef = $evals->where('note_global','!=','0')->count();
        $eval_sim = $evals->where('note_global','0')->count();
        $eval_par_qualiticien = $this->evalParQualiticien($evals);

        $groupedbyService = $evals->groupBy( function ($eval) {
            // if($eval->grille->service->nom !== 'ALC')
            return $eval->grille->service->nom;
        });
        // if($evals2) {
        //     $groupedbyServiceEscda = $evals2->groupBy( function ($eval) {
        //         // if($eval->grille->service->nom !== 'ALC')
        //         return $eval->grille->service->nom;
        //     });   
        //     unset($groupedbyService['ALC']);
        // }
        // else {
        //     $groupedbyServiceEscda = null;
        // }
    

        // return $groupedbyService;
        $eval_par_service = $groupedbyService->map(function ($item, $key) {
            return collect($item)->count();
        });
        // if($groupedbyServiceEscda){
        //     $eval_par_service_escda = $groupedbyServiceEscda->map(function ($item, $key) {
        //         return collect($item)->count();
        //     });
        // }

       

        $groupedbySite = $evals->groupBy( function ($eval) {
            return $eval->site;
        });
        $eval_par_site = $groupedbySite->map(function ($item, $key) {
            return collect($item)->count();
        });

        $moyenne_par_service = $groupedbyService->map(function ($item, $key) {
            $local_total = $item->count();
            $local_eval_note_global = $item->sum('note_global');
            if($local_eval_note_global == 0 ) return 0;
            else return round($local_eval_note_global/$local_total, 2);
        });
        
        // escda
        // if($groupedbyServiceEscda){
        //     $moyenne_par_service_escda = $groupedbyServiceEscda->map(function ($item, $key) {
        //         $local_total = $item->count();
        //         $local_eval_note_global = $item->sum('note_global');
        //         if($local_eval_note_global == 0 ) return 0;
        //         else return round($local_eval_note_global/$local_total, 2);
        //     });
        // }
        // else {
        //     $moyenne_par_service_escda = null;
        // }

// if($moyenne_par_service_escda){
//     $moyenne_par_service['ESCDA  CDS ']= $moyenne_par_service_escda[' ESCDA CDS'];
// }

// // return $moyenne_par_service;
//        unset($moyenne_par_service->ALC);
   
//         foreach($services as $s){
//             if(!isset($eval_par_service[$s])){
//                 $eval_par_service[$s]= 0;
//             }
//             if(!isset($moyenne_par_service[$s])){
//                 $moyenne_par_service[$s]= 0;
//             }
//         }

        // semaine
        $parsem = $this->nbrevalMoyenneParSem($evals);
        $eval_par_semaine = $parsem[0];
        $moyenne_par_semaine = $parsem[1];

        // mois
        $parmois = $this->nbrevalMoyenneParMois($evals);
        $eval_par_mois = $parmois[0];
        $moyenne_par_mois = $parmois[1];

        $data = [
            'eval_ef'=> $eval_ef,
            'eval_sim'=> $eval_sim,
        
            'moyenne'=> $moyenne,
            'eval_par_qualiticien' => $eval_par_qualiticien,
            'eval_par_service'=> $eval_par_service,
            'moyenne_par_service' => $moyenne_par_service,
            'moyenne_par_semaine' => $moyenne_par_semaine,
            'eval_par_semaine' => $eval_par_semaine,
            'eval_par_mois' => $eval_par_mois,
            'moyenne_par_mois' => $moyenne_par_mois,
            'eval_par_site' => $eval_par_site,
        ];

        return $data;

    }
    public function reportingConstatService(Request $req){
        $service = Service::find($req->input('service'));
            
        $user = auth('api')->user();

        $users =  User::all();
        foreach($users as $user){
            $idqualiticiens[]= $user->id;
        }

        // check if range date or year
        $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
        if(isset($req->input('dateRange')[1])) {
            $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
        } else {
            $endDate = $startDate;
        }
        // fetch by 'date_appel' if switchDate is true else fetch by created_at (=date_eval)
      $constats = Constat::where('service_id',$service->id)->whereBetween('created_at', [ $startDate, $endDate])->get();
      $constatstraite = Constat::where('service_id',$service->id)->where('statu','1')->whereBetween('created_at', [ $startDate, $endDate])->get();
      $constatsnontraite = Constat::where('service_id',$service->id)->where('statu','0')->whereBetween('created_at', [ $startDate, $endDate])->get();
     
      $groupedbyService = $constats->groupBy( function ($eval) {
        return $eval->service->nom;
    });
 
    $constat_par_service = $groupedbyService->map(function ($item, $key) {
        return collect($item)->count();
    });
  $constats_per_qual=  $this->constaperUser($constats);

  $constats_par_type = $this->constatsParType($constats);
  
      $data = [
        'countnontraite'=> $constatsnontraite->count(),
        'counttraite'=>$constatstraite->count(),
      
        'constats_per_qual'=>$constats_per_qual,
       
        'constats_par_type'=>$constats_par_type,


    ];

    return $data;

    }

    // reporting - service page
    public function reportingService(Request $req){

        $service = Service::find($req->input('service'));
       if($service->id==56){
        if(!isset($service->grille)){
            return 'does not exist';
        }
        $grille_id = $service->grille->id;
        $grille = Grille::find($grille_id);
        $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
        if(isset($req->input('dateRange')[1])) {
            $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
        } else {
            $endDate = $startDate;
        }
        if($req->switchDate == "true"){
            $date_param = 'date_appel';
        } else {
            $date_param = 'created_at';
        }
        $users =  User::whereHas("roles", function($q){ $q->where("name", "qualiticien"); })->get();
        foreach($users as $user){
            $idqualiticiens[]= $user->id;
        }
        if($req->switchQuality == "true"){
   
            $evals = Escda::where('grille_id', $grille_id)->whereIn('qualiticien_id',$idqualiticiens)->whereIn('site', $req->input('selectedSites'))->whereBetween($date_param , [ $startDate, $endDate])->get();
        } else {
            $evals = Escda::where('grille_id', $grille_id)->whereIn('site', $req->input('selectedSites'))->whereBetween($date_param , [ $startDate, $endDate])->get();
        }
        $moyenne = $this->moyenne($evals);
        $eval_ef = $evals->where('note_global','!=','0')->count();
        $eval_sim = $evals->where('note_global','0')->count();
        $eval_total = $evals->count();
        $parsem = $this->nbrevalMoyenneParSem($evals);
        $eval_par_semaine = $parsem[0];
        $moyenne_par_semaine = $parsem[1];
        $parmois = $this->nbrevalMoyenneParMois($evals);
        $eval_par_mois = $parmois[0];
        $moyenne_par_mois = $parmois[1];
  
        $itemsAndNotes2 = $this->itemsNotes($evals);
        $noteNoteArray = $itemsAndNotes[0];
        $items = $itemsAndNotes[1];
        $noteNoteArray2 = $itemsAndNotes2[0];
        $items2 = $itemsAndNotes2[1];
        $groupbyAgent = $evals->groupBy( function ($eval) {
            return $eval->agent->matricule;
        });
        

        $eval_par_agent = $groupbyAgent->map(function ($item) {
            return collect($item)->count();
        });
        $moyenne_par_agent = $groupbyAgent->map(function ($item) {
            $local_total = $item->count();
            $local_eval_note_global = $item->sum('note_global');
            if($local_eval_note_global == 0 ) return 0;
            else return round($local_eval_note_global/$local_total, 2);
        });
        $appel_indice_par_agent = $groupbyAgent->map(function ($items) {
            return $items->map(function ($item) {
                return $item->indice_tel;
            });
        });
        $allAgent = array();
        foreach($evals as $eval){
            if(!in_array($eval->agent, $allAgent)) $allAgent[] = $eval->agent;
        }
        $agents = [];
        foreach($allAgent as $a){
            if(!isset($eval_par_agent[$a->matricule])){
              /*  $aname = null;
                $aname = $a->nom.' '.$a->prenom;
                $agents[]= ['agent'=> $aname , 'nbr_eval' => 0 , 'moyenne' => 0];*/
            }else{
                $aname = null;
                $aname = $a->nom.' '.$a->prenom;
                $agents[] = ['agent'=> $aname , 'nbr_eval' => $eval_par_agent[$a->matricule] , 'moyenne' => $moyenne_par_agent[$a->matricule], 'indice_tel' => implode(" - ",$appel_indice_par_agent[$a->matricule]->toArray())];

            }
        }
        $groupbyQualiticien = $evals->groupBy( function ($eval) {
            return $eval->qualiticien->matricule;
        });

        $eval_par_qualiticien = $groupbyQualiticien->map(function ($item) {
            return collect($item)->count();
        });
        $moyenne_par_qualiticien = $groupbyQualiticien->map(function ($item) {
            $local_total = $item->count();
            $local_eval_note_global = $item->sum('note_global');
            if($local_eval_note_global == 0 ) return 0;
            else return round($local_eval_note_global/$local_total, 2);
        });

        $allQualiticien = array(); // Qualiticien::all('nom','prenom','matricule'); // and where('service_id',$serviceId);
        foreach($evals as $eval){
            if(!in_array($eval->qualiticien, $allQualiticien)) $allQualiticien[] = $eval->qualiticien;
        }
        $qualiticiens = [];
        foreach($allQualiticien as $a){
           /* if(!isset($eval_par_qualiticien[$a->matricule])){
                $aname = null;
                $aname = $a->nom.' '.$a->prenom;
                $qualiticiens[]= ['agent'=> $aname , 'nbr_eval' => 0 , 'moyenne' => 0];
            }else{*/
                $aname = null;
                $aname = $a->nom.' '.$a->prenom;
                $qualiticiens[] = ['agent'=> $aname , 'nbr_eval' => $eval_par_qualiticien[$a->matricule] , 'moyenne' => $moyenne_par_qualiticien[$a->matricule]];

           // }
        }
  
        $data = [
            'eval_ef'=> $eval_ef,
            'eval_sim'=> $eval_sim,
            'moyenne'=> $moyenne,
            'moyenne_par_semaine' => $moyenne_par_semaine,
            'moyenne_par_mois' => $moyenne_par_mois,
            'eval_par_semaine' => $eval_par_semaine,
            'eval_par_mois' => $eval_par_mois,
            'noteNoteArray' => $noteNoteArray, // AQ % NAQ %....
            'itemsNames'=> $items,
            'noteNoteArray2' => $noteNoteArray2, // AQ % NAQ %....
            'itemsNames2'=> $items2,
            'agents' => $agents,
            'qualiticiens' => $qualiticiens,
            'escda'=>'true'
        ];
        return $data;
       }
        if(!isset($service->grille)){
            return 'does not exist';
        }
        $grille_id = $service->grille->id;
        $grille = Grille::find($grille_id);
        $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
        if(isset($req->input('dateRange')[1])) {
            $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
        } else {
            $endDate = $startDate;
        }
        if($req->switchDate == "true"){
            $date_param = 'date_appel';
        } else {
            $date_param = 'created_at';
        }
        $users =  User::whereHas("roles", function($q){ $q->where("name", "qualiticien"); })->get();
        foreach($users as $user){
            $idqualiticiens[]= $user->id;
        }
        if($req->switchQuality == "true"){
   
            $evals = Evaluation::where('grille_id', $grille_id)->whereIn('qualiticien_id',$idqualiticiens)->whereIn('site', $req->input('selectedSites'))->whereBetween($date_param , [ $startDate, $endDate])->get();
        } else {
            $evals = Evaluation::where('grille_id', $grille_id)->whereIn('site', $req->input('selectedSites'))->whereBetween($date_param , [ $startDate, $endDate])->get();
        }
        

        $moyenne = $this->moyenne($evals);
        
        $eval_ef = $evals->where('note_global','!=','0')->count();
        $eval_sim = $evals->where('note_global','0')->count();
        $eval_total = $evals->count();

        // semaine
        $parsem = $this->nbrevalMoyenneParSem($evals);
        $eval_par_semaine = $parsem[0];
        $moyenne_par_semaine = $parsem[1];

        // mois
        $parmois = $this->nbrevalMoyenneParMois($evals);
        $eval_par_mois = $parmois[0];
        $moyenne_par_mois = $parmois[1];
        $itemsAndNotes = [];
        if($req->activity ==11){
            // partie element
            $itemsAndNotes = $this->itemsNotesLaredoute($evals);
            // element
            $itemsAndNotes2 = $this->itemsNotes2Laredoute($evals);

        }
        else {
            $itemsAndNotes = $this->itemsNotes($evals);
            $itemsAndNotes2 = $this->itemsNotes2($evals);
        }
        
 

        $noteNoteArray = $itemsAndNotes[0];
        $items = $itemsAndNotes[1];
        $noteNoteArray2N = $itemsAndNotes2[0];
        $items2N = $itemsAndNotes2[1];

        // grouped by agents
        $groupbyAgent = $evals->groupBy( function ($eval) {
            return $eval->agent->matricule;
        });

        $eval_par_agent = $groupbyAgent->map(function ($item) {
            return collect($item)->count();
        });
        $moyenne_par_agent = $groupbyAgent->map(function ($item) {
            $local_total = $item->count();
            $local_eval_note_global = $item->sum('note_global');
            if($local_eval_note_global == 0 ) return 0;
            else return round($local_eval_note_global/$local_total, 2);
        });
        $appel_indice_par_agent = $groupbyAgent->map(function ($items) {
            return $items->map(function ($item) {
                return $item->indice_tel;
            });
        });
        $semainee_par_agent = $groupbyAgent->map(function ($items) {
            return $items->map(function ($item) {
                return $item->semaine;
            });
        });
      
        // replace this eloquent / get agents with relationship
        //$allAgent = Agent::where('service_id',$req->input('service'))->whereIn('site', $req->input('selectedSites'))->get(); //all('nom','prenom','matricule'); // and
        $allAgent = array();
        foreach($evals as $eval){
            if(!in_array($eval->agent, $allAgent)) $allAgent[] = $eval->agent;
        }
        $agents = [];
        foreach($allAgent as $a){
            if(!isset($eval_par_agent[$a->matricule])){
              /*  $aname = null;
                $aname = $a->nom.' '.$a->prenom;
                $agents[]= ['agent'=> $aname , 'nbr_eval' => 0 , 'moyenne' => 0];*/
            }else{
                $aname = null;
                $aname = $a->nom.' '.$a->prenom;
                $agents[] = ['agent'=> $aname , 'nbr_eval' => $eval_par_agent[$a->matricule] , 'moyenne' => $moyenne_par_agent[$a->matricule], 'indice_tel' => implode(" - ",$appel_indice_par_agent[$a->matricule]->toArray()), 'semaine'=>'S'.$semainee_par_agent[$a->matricule][0]];

            }
        }

        // grouped by qualiticien
        $groupbyQualiticien = $evals->groupBy( function ($eval) {
            return $eval->qualiticien->matricule;
        });

        $eval_par_qualiticien = $groupbyQualiticien->map(function ($item) {
            return collect($item)->count();
        });
        $moyenne_par_qualiticien = $groupbyQualiticien->map(function ($item) {
            $local_total = $item->count();
            $local_eval_note_global = $item->sum('note_global');
            if($local_eval_note_global == 0 ) return 0;
            else return round($local_eval_note_global/$local_total, 2);
        });

        $allQualiticien = array(); // Qualiticien::all('nom','prenom','matricule'); // and where('service_id',$serviceId);
        foreach($evals as $eval){
            if(!in_array($eval->qualiticien, $allQualiticien)) $allQualiticien[] = $eval->qualiticien;
        }
        $qualiticiens = [];
        foreach($allQualiticien as $a){
           /* if(!isset($eval_par_qualiticien[$a->matricule])){
                $aname = null;
                $aname = $a->nom.' '.$a->prenom;
                $qualiticiens[]= ['agent'=> $aname , 'nbr_eval' => 0 , 'moyenne' => 0];
            }else{*/
                $aname = null;
                $aname = $a->nom.' '.$a->prenom;
                $qualiticiens[] = ['agent'=> $aname , 'nbr_eval' => $eval_par_qualiticien[$a->matricule] , 'moyenne' => $moyenne_par_qualiticien[$a->matricule]];

           // }
        }
  
        $data = [
            'eval_ef'=> $eval_ef,
            'eval_sim'=> $eval_sim,
            'moyenne'=> $moyenne,
            'moyenne_par_semaine' => $moyenne_par_semaine,
            'moyenne_par_mois' => $moyenne_par_mois,
            'eval_par_semaine' => $eval_par_semaine,
            'eval_par_mois' => $eval_par_mois,
            'noteNoteArray' => $noteNoteArray, // AQ % NAQ %....
            'itemsNames'=> $items,
            'noteNoteArray2N' => $noteNoteArray2N, // AQ % NAQ %....
            'itemsNames2N'=> $items2N,
            'agents' => $agents,
            'qualiticiens' => $qualiticiens,
        ];
        return $data;
    }
    public function reportingTeleconseiller(Request $req){
        // $groupedbySemaine = $evals->sortBy('semaine')->groupBy('semaine');
        $service = Service::find($req->input('service'));
        if(!isset($service->grille)){
            return 'does not exist';
        }
        $grille_id = $service->grille->id;
        $agent_id = $req->input('agent');
        // check if range date or year
        $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
        if(isset($req->input('dateRange')[1])) {
            $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
        } else {
            $endDate = $startDate;
        }
 
        $evals = Evaluation::where(['agent_id'=> $agent_id,
            'grille_id'=> $grille_id])->whereBetween('created_at', [ $startDate, $endDate])->get();
        // moyenne 
        $moyenne = $this->moyenne($evals);
        $eval_ef = $evals->where('note_global','!=','0')->count();
        $eval_sim = $evals->where('note_global','0')->count();

        // semaine
        $parsem = $this->nbrevalMoyenneParSem($evals);
        $eval_par_semaine = $parsem[0];
        $moyenne_par_semaine = $parsem[1];

        // mois
        $parmois = $this->nbrevalMoyenneParMois($evals);
        $eval_par_mois = $parmois[0];
        $moyenne_par_mois = $parmois[1];

        // grid items
        $itemsAndNotes = $this->itemsNotes($evals);
        $noteNoteArray = $itemsAndNotes[0];
        $items = $itemsAndNotes[1];
       // grid items by week
    //    $itemsAndNotesbyweek = $this->itemsNotesbyweek($evals);
    //    $noteNoteArraybyweek = $itemsAndNotesbyweek[0];
    //    $itemsbyweek = $itemsAndNotesbyweek[1];
    
    
        $data = [
            'eval_ef'=> $eval_ef,
            'eval_sim'=> $eval_sim,
            'moyenne'=> $moyenne,
            'moyenne_par_semaine' => $moyenne_par_semaine,
            'moyenne_par_mois' => $moyenne_par_mois,
            'eval_par_semaine' => $eval_par_semaine,
            'eval_par_mois' => $eval_par_mois,
            'noteNoteArray' => $noteNoteArray, // AQ % NAQ %....
            'itemsNames'=> $items,
        ];
        return $data;
    }

    public function moyenneParActivite($evals){

        $groupedbyactivity = $evals->groupBy( function ($eval) {
            return $eval->grille->service->activite->nom;
        });

        $moyenne_par_activite = $groupedbyactivity->map(function ($item, $key) {
            $local_total = $item->count();
            $local_eval_note_global = $item->sum('note_global');
            if($local_eval_note_global == 0 ) return 0;
            else return round($local_eval_note_global/$local_total, 2);
        });

        $allactivities = Activite::where('statut',1)->get();


        foreach($allactivities as $a){
            if(!isset($moyenne_par_activite[$a->nom])){
                $moyenne_par_activite[$a->nom]= 0;
            }

        }
        return $moyenne_par_activite;

    }

    public function evalParActivite($evals){
        $groupedbyactivity = $evals->groupBy( function ($eval) {
            return $eval->grille->service->activite->nom;
        });
        $eval_par_activite = $groupedbyactivity->map(function ($item, $key) {
            return collect($item)->count();
        });
       $allactivities = Activite::where('statut',1)->get();
        foreach($allactivities as $a){
            if(!isset($eval_par_activite[$a->nom])){
                $eval_par_activite[$a->nom]= 0;
            }
        }
        return $eval_par_activite;
    }
    public function constatParActivite($constats){
        $groupedbyactivity = $constats->groupBy( function ($eval) {
            return $eval->activite->nom;
        });
       
        $eval_par_activite = $groupedbyactivity->map(function ($item, $key) {
            return collect($item)->count();
        });
       $allactivities = Activite::where('statut',1)->get();
        foreach($allactivities as $a){
            if(!isset($eval_par_activite[$a->nom])){
                $eval_par_activite[$a->nom]= 0;
            }
        }
        return $eval_par_activite;
    }

    public function moyenne($evals){
       
        // the value return from this function and the some return from moyenneParActivite are not the same they should be the same / or not i dont know anymore hhhhhhhhhhhhhhhhhhhhhhhhhhh
       $total = $evals->count() ; 
       if($total == 0) return 0;

      else return round($evals->sum('note_global')/$total, 2);
    }

    public function evalParQualiticien($evals){

        $groupedbyqualiticien = $evals->groupBy( function ($eval) {
            return $eval->qualiticien->nom;
        });

        $eval_par_qualiticien = $groupedbyqualiticien->map(function ($item) {
            return collect($item)->count();
        });

        return $eval_par_qualiticien;
        // $allqualiticiens = User::whereHas("roles", function($q){ $q->where("name", "qualiticien"); })->get(); //User::role('admin')->get();
        //$allqualiticiens = Qualiticien::all('nom','prenom','matricule');
        //dd($eval_par_qualiticien);
        /*foreach($allqualiticiens as $a){
            if(!isset($eval_par_qualiticien[$a->matricule])){
                $qname = null;
                $qname = $a->nom.' '.$a->prenom;
                $eval_par_qualiticien[$qname]= 0;
            }else{
                $qname = null;
                $qname = $a->nom.' '.$a->prenom;
                $eval_par_qualiticien[$qname] = $eval_par_qualiticien[$a->matricule];
                unset($eval_par_qualiticien[$a->matricule]);
            }
        }
        return $eval_par_qualiticien;*/
    }
    public function constaperUser($constats){

        $groupedbyqualiticien = $constats->groupBy( function ($eval) {
            return $eval->user->nom;
        });

        $constat_per_user = $groupedbyqualiticien->map(function ($item) {
            return collect($item)->count();
        });

        return $constat_per_user;
      
    }
    public function itemsNotes4($evals){
     
        // this function iw working correctly don't touch it
        $notegroups = $evals->map(function ($item) {
            return $item->notes;
        });
        $notesArray = [];
        foreach($notegroups as $notes_in_eval){
            foreach($notes_in_eval as $note){
                $id = $note->element_id ;
                $notesArray[$id][] = $note->note;
            }
        }

        $noteNoteArray = [];
        foreach($notesArray as $note){
            $counted = array_count_values($note);
            if(!isset($counted['AQ'])){
                $counted['AQ'] = 0;
            }
            if(!isset($counted['NAQ'])){
                $counted['NAQ'] = 0;
            }
            if(!isset($counted['NA'])){
                $counted['NA'] = 0;
            }
            if(!isset($counted['SI'])){
                $counted['SI'] = 0;
            }
            if(!isset($counted['PA'])){
                $counted['PA'] = 0;
            }
            $total = $counted['AQ'] + $counted['NAQ'] + $counted['NA'] +$counted['SI'];
            $noteNoteArray['OK'][] = round((100*$counted['AQ'])/$total,2);
            $noteNoteArray['KO'][] = round((100*$counted['NAQ'])/$total,2);
            $noteNoteArray['na'][] = round((100*$counted['NA'])/$total,2);
            $noteNoteArray['pa'][] = round((100*$counted['PA'])/$total,2);
            $noteNoteArray['si'][] = round((100*$counted['SI'])/$total,2);
        }

        $items = [] ;
        foreach($notesArray as $key => $val){
            $id = DB::table('grille_element_grille')->select('element_grille_id as id')->where('id',$key)->get();
            $el = ElementGrille::find($id[0]->id);//select('nom','id')->where('id',$key)->get();
            $items[] = $el->nom;


            // $items[] = $el->partie->nom;
            // $items =array_values(array_unique( $items, SORT_REGULAR ));
        }
        // return  $el->partie;
        return [$noteNoteArray,$items];
    }
    public function itemsNotesLaredoute($evals){
     
        // this function iw working correctly don't touch it
        $notegroups = $evals->map(function ($item) {
            return $item->notes;
        });
        $notesArray = [];
        foreach($notegroups as $notes_in_eval){
            foreach($notes_in_eval as $note){
                $id = $note->element_id ;
                $notesArray[$id][] = $note->note;
            }
        }

        $noteNoteArray = [];
        foreach($notesArray as $note){
            $counted = array_count_values($note);
            if(!isset($counted['EX'])){
                $counted['EX'] = 0;
            }
            if(!isset($counted['BON'])){
                $counted['BON'] = 0;
            }
            if(!isset($counted['PA'])){
                $counted['PA'] = 0;
            }
            if(!isset($counted['NAQ'])){
                $counted['NAQ'] = 0;
            }
            if(!isset($counted['MOY'])){
                $counted['MOY'] = 0;
            }
            if(!isset($counted['MAU'])){
                $counted['MAU'] = 0;
            }

            $total = $counted['EX'] + $counted['BON'] + $counted['MOY'] +$counted['MAU'] + $counted['NAQ'] + $counted['PA'];
            $noteNoteArray['EX'][] = round((100*$counted['EX'])/$total,2);
            $noteNoteArray['BON'][] = round((100*$counted['BON'])/$total,2);
            $noteNoteArray['MOY'][] = round(((100*$counted['MOY']) + (100*$counted['PA']))/$total,2);
            $noteNoteArray['MAU'][] = round(((100*$counted['MAU']) + (100*$counted['NAQ']))/$total,2);

        }

        $items = [] ;
        foreach($notesArray as $key => $val){
            $id = DB::table('grille_element_grille')->select('element_grille_id as id')->where('id',$key)->get();
            $el = ElementGrille::find($id[0]->id);//select('nom','id')->where('id',$key)->get();
            $items[] = $el->nom;


            // $items[] = $el->partie->nom;
            // $items =array_values(array_unique( $items, SORT_REGULAR ));
        }
        // return  $el->partie;
        return [$noteNoteArray,$items];
    }
    // return [itemsname , notes]
    public function itemsNotes($evals){
     
        // this function iw working correctly don't touch it
        $notegroups = $evals->map(function ($item) {
            return $item->notes;
        });
        $notesArray = [];
        foreach($notegroups as $notes_in_eval){
            foreach($notes_in_eval as $note){
                $id = $note->element_id ;
                $notesArray[$id][] = $note->note;
            }
        }

        $noteNoteArray = [];
        foreach($notesArray as $note){
            $counted = array_count_values($note);
            if(!isset($counted['AQ'])){
                $counted['AQ'] = 0;
            }
            if(!isset($counted['NAQ'])){
                $counted['NAQ'] = 0;
            }
            if(!isset($counted['NA'])){
                $counted['NA'] = 0;
            }
            if(!isset($counted['SI'])){
                $counted['SI'] = 0;
            }
            if(!isset($counted['PA'])){
                $counted['PA'] = 0;
            }
            $total = $counted['AQ'] + $counted['NAQ'] + $counted['NA'] +$counted['SI']+ $counted['PA'];
            if($total !== 0){
                $noteNoteArray['aq'][] = round((100*$counted['AQ'])/$total,2);
                $noteNoteArray['naq'][] = round((100*$counted['NAQ'])/$total,2);
                $noteNoteArray['na'][] = round((100*$counted['NA'])/$total,2);
                $noteNoteArray['pa'][] = round((100*$counted['PA'])/$total,2);
                $noteNoteArray['si'][] = round((100*$counted['SI'])/$total,2);
            }
            else {
                $noteNoteArray['aq'][] =0;
                $noteNoteArray['naq'][] =0;
                $noteNoteArray['na'][] =0;
                $noteNoteArray['pa'][] = 0;
                $noteNoteArray['si'][] = 0;
            }
          
        }

        $items = [] ;
        foreach($notesArray as $key => $val){
            $id = DB::table('grille_element_grille')->select('element_grille_id as id')->where('id',$key)->get();
            $el = ElementGrille::find($id[0]->id);//select('nom','id')->where('id',$key)->get();
            $items[] = $el->nom;


            // $items[] = $el->partie->nom;
            // $items =array_values(array_unique( $items, SORT_REGULAR ));
        }
        // return  $el->partie;
        return [$noteNoteArray,$items];
    }


    // items by week 
    public function itemsNotesbyweek($evals){
      

        $groupedbySemaine = $evals->groupBy( function ($eval) {
            return $eval->semaine;
        });
        foreach($groupedbySemaine as $key=>$notebyweek){
            $notegroups['S'.$key] = $notebyweek->map(function ($item) {
                return $item->notes;
            });
            
        }

        $notesArray = [];
        foreach($notegroups as $key=> $notes_in_eval){
            
            foreach($notes_in_eval as $note){
                foreach ($note as $n){
                   
                         $id = $n->element_id ;
                $notesArray[$key][$id][] = $n->note;
                }
          
                
            }
        }

        $noteNoteArray = [];
        foreach($notesArray as $key=>$note){
        foreach($note as $n){
            $counted[$key] = array_count_values($n);
            if(!isset($counted[$key]['AQ'])){
                $counted[$key]['AQ'] = 0;
            }
            if(!isset($counted[$key]['NAQ'])){
                $counted[$key]['NAQ'] = 0;
            }
            if(!isset($counted[$key]['NA'])){
                $counted[$key]['NA'] = 0;
            }
            if(!isset($counted[$key]['SI'])){
                $counted[$key]['SI'] = 0;
            }
            if(!isset($counted[$key]['PA'])){
                $counted[$key]['PA'] = 0;
            }
        }
            
           
            $total[$key] = $counted[$key]['AQ'] + $counted[$key]['NAQ'] + $counted[$key]['NA'] +$counted[$key]['SI'];

            $noteNoteArray[$key]['aq'][] = round((100*$counted[$key]['AQ'])/$total[$key],2);
            $noteNoteArray[$key]['naq'][] = round((100*$counted[$key]['NAQ'])/$total[$key],2);
            $noteNoteArray[$key]['na'][] = round((100*$counted[$key]['NA'])/$total[$key],2);
            $noteNoteArray[$key]['pa'][] = round((100*$counted[$key]['PA'])/$total[$key],2);
            $noteNoteArray[$key]['si'][] = round((100*$counted[$key]['SI'])/$total[$key],2);
        }
     
        $items = [] ;
        foreach($notesArray as $key => $val){
            return $key;
            // $id = DB::table('grille_element_grille')->select('element_grille_id as id')->where('id',$key)->get();
            // $el = ElementGrille::find($id[0]->id);//select('nom','id')->where('id',$key)->get();
            // $items[] = $el->nom;


            // $items[] = $el->partie->nom;
            // $items =array_values(array_unique( $items, SORT_REGULAR ));
        }
        // return  $el->partie;
        return [$noteNoteArray,$items];
    }
    // items by week end 
    public function itemsNotes2($evals){
        // this function iw working correctly don't touch it
        
        $notegroups = $evals->map(function ($item) {
            return $item->notes;
        });
  
        $notesArray = [];
        $items = [] ;

        $grille_element_grille_array=[];
        foreach($notegroups as $notes_in_eval){
            foreach($notes_in_eval as $note){
               // $id = $note->element_id; // grille_element_grille
                $grille_element_grille= DB::table('grille_element_grille' )->where('id',$note->element_id)->first();
                $grille_element_grille_array[]=$grille_element_grille;
                // $element_grilles=DB::table('element_grilles')->where('id',$grille_element_grille->element_grille_id )
                $el = ElementGrille::find($grille_element_grille->element_grille_id);
             $id = $el->partie->id;
                $notesArray[ $id ][] = $note->note;
                $items[] = $el->partie->nom;
                $items =array_values(array_unique( $items, SORT_REGULAR ));
                

            }
               

        }

        $noteNoteArray = [];
        foreach($notesArray as $note){
            $counted = array_count_values($note);
            if(!isset($counted['AQ'])){
                $counted['AQ'] = 0;
            }
            if(!isset($counted['NAQ'])){
                $counted['NAQ'] = 0;
            }
            if(!isset($counted['NA'])){
                $counted['NA'] = 0;
            }
            if(!isset($counted['SI'])){
                $counted['SI'] = 0;
            }
            if(!isset($counted['PA'])){
                $counted['PA'] = 0;
            }
            $total = $counted['AQ'] + $counted['NAQ'] + $counted['NA'] +$counted['SI'] + $counted['PA'];
           
            $noteNoteArray['aq'][] = round((100*$counted['AQ'])/$total,2);
            $noteNoteArray['naq'][] = round((100*$counted['NAQ'])/$total,2);
            $noteNoteArray['na'][] = round((100*$counted['NA'])/$total,2);
            $noteNoteArray['si'][] = round((100*$counted['SI'])/$total,2);
            $noteNoteArray['pa'][] = round((100*$counted['PA'])/$total,2);
        }
       
   

        return [$noteNoteArray,$items];
    }
    public function itemsNotes2Laredoute($evals){
        // this function iw working correctly don't touch it
        
        $notegroups = $evals->map(function ($item) {
            return $item->notes;
        });
  
        $notesArray = [];
        $items = [] ;

        $grille_element_grille_array=[];
        foreach($notegroups as $notes_in_eval){
            foreach($notes_in_eval as $note){
               // $id = $note->element_id; // grille_element_grille
                $grille_element_grille= DB::table('grille_element_grille' )->where('id',$note->element_id)->first();
                $grille_element_grille_array[]=$grille_element_grille;
                // $element_grilles=DB::table('element_grilles')->where('id',$grille_element_grille->element_grille_id )
                $el = ElementGrille::find($grille_element_grille->element_grille_id);
             $id = $el->partie->id;
                $notesArray[ $id ][] = $note->note;
                $items[] = $el->partie->nom;
                $items =array_values(array_unique( $items, SORT_REGULAR ));
                

            }
               

        }

        $noteNoteArray = [];
        foreach($notesArray as $note){
            $counted = array_count_values($note);
            if(!isset($counted['EX'])){
                $counted['EX'] = 0;
            }
            if(!isset($counted['BON'])){
                $counted['BON'] = 0;
            }
            if(!isset($counted['MOY'])){
                $counted['MOY'] = 0;
            }
            if(!isset($counted['PA'])){
                $counted['PA'] = 0;
            }
            if(!isset($counted['NAQ'])){
                $counted['NAQ'] = 0;
            }
            if(!isset($counted['MAU'])){
                $counted['MAU'] = 0;
            }

            $total = $counted['EX'] + $counted['BON'] + $counted['MOY'] +$counted['MAU'] ;
           
            $noteNoteArray['EX'][] = round((100*$counted['EX'])/$total,2);
            $noteNoteArray['BON'][] = round((100*$counted['BON'])/$total,2);
            $noteNoteArray['MOY'][] = round(((100*$counted['MOY'])+ (100*$counted['PA']))/$total,2);
            $noteNoteArray['MAU'][] = round(((100*$counted['MAU']) + (100*$counted['NAQ']))/$total,2);

        }
       
   

        return [$noteNoteArray,$items];
    }
    public function itemsNotes3($evals){
        // this function iw working correctly don't touch it
        
        $notegroups = $evals->map(function ($item) {
            return $item->notes;
        });
  
        $notesArray = [];
        $items = [] ;

        $grille_element_grille_array=[];
        foreach($notegroups as $notes_in_eval){
            foreach($notes_in_eval as $note){
               // $id = $note->element_id; // grille_element_grille
                $grille_element_grille= DB::table('grille_element_grille' )->where('id',$note->element_id)->first();
                $grille_element_grille_array[]=$grille_element_grille;
                // $element_grilles=DB::table('element_grilles')->where('id',$grille_element_grille->element_grille_id )
                $el = ElementGrille::find($grille_element_grille->element_grille_id);
             $id = $el->partie->id;
                $notesArray[ $id ][] = $note->note;
                $items[] = $el->partie->nom;
                $items =array_values(array_unique( $items, SORT_REGULAR ));
                

            }
               

        }

        $noteNoteArray = [];
        foreach($notesArray as $note){
            $counted = array_count_values($note);
            if(!isset($counted['AQ'])){
                $counted['AQ'] = 0;
            }
            if(!isset($counted['NAQ'])){
                $counted['NAQ'] = 0;
            }
            if(!isset($counted['NA'])){
                $counted['NA'] = 0;
            }
            if(!isset($counted['SI'])){
                $counted['SI'] = 0;
            }
            if(!isset($counted['PA'])){
                $counted['PA'] = 0;
            }
            $total = $counted['AQ'] + $counted['NAQ'] + $counted['NA'] +$counted['SI'] + $counted['PA'];
            $noteNoteArray['OK'][] = round((100*$counted['AQ'])/$total,2);
            $noteNoteArray['KO'][] = round((100*$counted['NAQ'])/$total,2);
            $noteNoteArray['na'][] = round((100*$counted['NA'])/$total,2);
            $noteNoteArray['si'][] = round((100*$counted['SI'])/$total,2);
            $noteNoteArray['pa'][] = round((100*$counted['PA'])/$total,2);
        }

   

        return [$noteNoteArray,$items];
    }

    // return [ $eval_par_semaine , $moyenne_par_semaine  ]
    public function nbrevalMoyenneParSem($evals){
        $groupedbySemaine = $evals->sortBy('semaine')->groupBy('semaine');
        $sStart = array_key_first($groupedbySemaine->toArray());
        $sEnd = array_key_last($groupedbySemaine->toArray());

        $eval_par_semaine = $groupedbySemaine->map(function ($item) {
            return collect($item)->count();
        });

        for($i = $sStart ; $i <= $sEnd ; $i++ ) {
            if(!isset($eval_par_semaine[$i])){
                $eval_par_semaine[$i]=0;
            }
        }
        $eval_par_semaine = $eval_par_semaine->toArray();
        ksort($eval_par_semaine);

        $moyenne_par_semaine = $groupedbySemaine->map(function ($item, $key) {
            $local_total = $item->count();
            $local_eval_note_global = $item->sum('note_global');
            if($local_eval_note_global == 0 ) return 0;
            else return round($local_eval_note_global/$local_total, 2);
        });

        for($i = $sStart ; $i <= $sEnd ; $i++ ) {
            if(!isset($moyenne_par_semaine[$i])){
                $moyenne_par_semaine[$i]=0;
            }
        }

        $moyenne_par_semaine = $moyenne_par_semaine->toArray();
        ksort($moyenne_par_semaine);

        foreach($moyenne_par_semaine as $s => $val){
            $moyenne_par_semaine['S'.$s] = $val;
            unset($moyenne_par_semaine[$s]);
        }
        return [$eval_par_semaine, $moyenne_par_semaine];
    }
    public function nbrconstatMoyenneParSem($constats){
        $groupedbySemaine = $constats->sortBy('semaine')->groupBy('semaine');
        return $groupedbySemaine;
        $sStart = array_key_first($groupedbySemaine->toArray());
        $sEnd = array_key_last($groupedbySemaine->toArray());

        $eval_par_semaine = $groupedbySemaine->map(function ($item) {
            return collect($item)->count();
        });

        for($i = $sStart ; $i <= $sEnd ; $i++ ) {
            if(!isset($eval_par_semaine[$i])){
                $eval_par_semaine[$i]=0;
            }
        }
        $eval_par_semaine = $eval_par_semaine->toArray();
        ksort($eval_par_semaine);

        $moyenne_par_semaine = $groupedbySemaine->map(function ($item, $key) {
            $local_total = $item->count();
            $local_eval_note_global = $item->sum('note_global');
            if($local_eval_note_global == 0 ) return 0;
            else return round($local_eval_note_global/$local_total, 2);
        });

        for($i = $sStart ; $i <= $sEnd ; $i++ ) {
            if(!isset($moyenne_par_semaine[$i])){
                $moyenne_par_semaine[$i]=0;
            }
        }

        $moyenne_par_semaine = $moyenne_par_semaine->toArray();
        ksort($moyenne_par_semaine);

        foreach($moyenne_par_semaine as $s => $val){
            $moyenne_par_semaine['S'.$s] = $val;
            unset($moyenne_par_semaine[$s]);
        }
        return [$eval_par_semaine, $moyenne_par_semaine];
    }

    // return [ $eval_par_mois , $moyenne_par_mois  ]
    public function nbrevalMoyenneParMois($evals){

        setlocale(LC_TIME, "fr_FR");
        $groupedbymonth = $evals->groupBy(function($item) {
            return $item->created_at->format('M-Y');
        });

        $eval_par_mois = $groupedbymonth->map(function ($item) {
            return collect($item)->count();
        });

        $moyenne_par_mois = $groupedbymonth->map(function ($item, $key) {
            $local_total = $item->count();
            $local_eval_note_global = $item->sum('note_global');
            if($local_eval_note_global == 0 ) return 0;
            else return round($local_eval_note_global/$local_total, 2);
        })->all();

        return [$eval_par_mois,$moyenne_par_mois];

        // fix sorting
        /*
        uksort($moyenne_par_mois, function($time1, $time2) {
            if (strtotime($time1) < strtotime($time2))
                return 1;
            else if (strtotime($time1) > strtotime($time2))
                return -1;
            else
                return 0;
        }
        );
        */

    }
    public function getreporting_evaluateur_calibrage(Request $req) {
       

        if($req->switchDate == "true"){
            $date_param = 'date_appel';
        } else {
            $date_param = 'created_at';
        }
        $startDate = DateTime::createFromFormat('d/m/Y', $req->date[0])->format('Y-m-d');
        $endDate = DateTime::createFromFormat('d/m/Y', $req->date[1])->format('Y-m-d');
       
        // if a sup or client try to get reporting of an other activity return only his activity's reporting
        
            // not a client nor a sup
            $activity = Activite::findOrFail($req->input('activityId'));
      
           
        
        $services = [];
        $grille = [];
        foreach($activity->services as $service){
            if($service->grille){
                $grille[] = $service->grille->id;
            }
            if($service->nom!== 'ALC'){
                $services[] = $service->nom;
            }
           
        } 
        
        $evaluations = Calibrage::whereBetween($date_param ,[$startDate,$endDate])->whereIn('grille_id', $grille)->where('qualiticien_id',$req->evaluateur)->get();
       
        if($evaluations->count()==0) return response()->json(['no data found']);
        $moyenne = $this->moyenne($evaluations); 
        // moyenne des evaluations
        $eval_ef = $evaluations->where('note_global','!=','0')->count(); // ef
        $eval_sim = $evaluations->where('note_global','0')->count(); // sim 
        $eval_par_qualiticien = $this->evalParQualiticien($evaluations); // nombr des evaluation 
        
// semaine
        $parsem = $this->nbrevalMoyenneParSem($evaluations);
        $eval_par_semaine = $parsem[0];
        $moyenne_par_semaine = $parsem[1];
       
        // mois
        $parmois = $this->nbrevalMoyenneParMois($evaluations);
        $eval_par_mois = $parmois[0];
        $moyenne_par_mois = $parmois[1];
        
        //
        $groupbyAgent = $evaluations->groupBy( function ($eval) {
            return $eval->agent->matricule;
        });

        $eval_par_agent = $groupbyAgent->map(function ($item) {
            return collect($item)->count();
        });
        //
        

        $moyenne_par_agent = $groupbyAgent->map(function ($item) {
            $local_total = $item->count();
            $local_eval_note_global = $item->sum('note_global');
            if($local_eval_note_global == 0 ) return 0;
            else return round($local_eval_note_global/$local_total, 2);
        });
        
        $appel_indice_par_agent = $groupbyAgent->map(function ($items) {
            return $items->map(function ($item) {
                return $item->indice_tel;
            });
        });

        $allAgent = array();
        foreach($evaluations as $eval){
            if(!in_array($eval->agent, $allAgent)) $allAgent[] = $eval->agent;
        }
        
        
        $agents = [];
        foreach($allAgent as $a){
            if(!isset($eval_par_agent[$a->matricule])){
              /*  $aname = null;
                $aname = $a->nom.' '.$a->prenom;
                $agents[]= ['agent'=> $aname , 'nbr_eval' => 0 , 'moyenne' => 0];*/
            }else{
                $aname = null;
                $aname = $a->nom.' '.$a->prenom;
                $agents[] = ['agent'=> $aname , 'nbr_eval' => $eval_par_agent[$a->matricule] , 'moyenne' => $moyenne_par_agent[$a->matricule], 'indice_tel' => implode(" - ",$appel_indice_par_agent[$a->matricule]->toArray())];

            }
        }
      // 
    //   $itemsAndNotes = $this->itemsNotes($evaluations);
      
 
    //      $noteNoteArray = $itemsAndNotes[0];
    //      $items = $itemsAndNotes[1];
         
      
         
        return $data=[
            'moyenne'=>$moyenne,
            'eval_ef'=>$eval_ef,
            'eval_sim'=>$eval_sim,
            'eval_par_qualiticien'=>$eval_par_qualiticien,
            'eval_par_semaine'=>$eval_par_semaine,
            'eval_par_mois'=>$eval_par_mois,
            // 'noteNoteArray' => $noteNoteArray, // AQ % NAQ %....
            // 'itemsNames'=> $items,
            'moyenne_par_mois'=>$moyenne_par_mois,
            'agents'=>$agents

        ];
    }
    public function getreporting_evaluateur(Request $req) {
        if($req->switchDate == "true"){
            $date_param = 'date_appel';
        } else {
            $date_param = 'created_at';
        }
        $startDate = DateTime::createFromFormat('d/m/Y', $req->date[0])->format('Y-m-d');
        $endDate = DateTime::createFromFormat('d/m/Y', $req->date[1])->format('Y-m-d');
        $evaluations = Evaluation::whereBetween($date_param ,[$startDate,$endDate])->where('qualiticien_id',$req->evaluateur)->get();
        if($evaluations->count()==0) return response()->json(['no data found']);
        $moyenne = $this->moyenne($evaluations); // moyenne des evaluations
        $eval_ef = $evaluations->where('note_global','!=','0')->count(); // ef
        $eval_sim = $evaluations->where('note_global','0')->count(); // sim 
        $eval_par_qualiticien = $this->evalParQualiticien($evaluations); // nombr des evaluation 
        
// semaine
        $parsem = $this->nbrevalMoyenneParSem($evaluations);
        $eval_par_semaine = $parsem[0];
        $moyenne_par_semaine = $parsem[1];

        // mois
        $parmois = $this->nbrevalMoyenneParMois($evaluations);
        $eval_par_mois = $parmois[0];
        $moyenne_par_mois = $parmois[1];
        //
        $groupbyAgent = $evaluations->groupBy( function ($eval) {
            return $eval->agent->matricule;
        });

        $eval_par_agent = $groupbyAgent->map(function ($item) {
            return collect($item)->count();
        });
        //


        $moyenne_par_agent = $groupbyAgent->map(function ($item) {
            $local_total = $item->count();
            $local_eval_note_global = $item->sum('note_global');
            if($local_eval_note_global == 0 ) return 0;
            else return round($local_eval_note_global/$local_total, 2);
        });
        $appel_indice_par_agent = $groupbyAgent->map(function ($items) {
            return $items->map(function ($item) {
                return $item->indice_tel;
            });
        });


        $allAgent = array();
        foreach($evaluations as $eval){
            if(!in_array($eval->agent, $allAgent)) $allAgent[] = $eval->agent;
        }
        $agents = [];
        foreach($allAgent as $a){
            if(!isset($eval_par_agent[$a->matricule])){
              /*  $aname = null;
                $aname = $a->nom.' '.$a->prenom;
                $agents[]= ['agent'=> $aname , 'nbr_eval' => 0 , 'moyenne' => 0];*/
            }else{
                $aname = null;
                $aname = $a->nom.' '.$a->prenom;
                $agents[] = ['agent'=> $aname , 'nbr_eval' => $eval_par_agent[$a->matricule] , 'moyenne' => $moyenne_par_agent[$a->matricule], 'indice_tel' => implode(" - ",$appel_indice_par_agent[$a->matricule]->toArray())];

            }
        }
      // 
    //   $itemsAndNotes = $this->itemsNotes($evaluations);


        //  $noteNoteArray = $itemsAndNotes[0];
        //  $items = $itemsAndNotes[1];
         
      
         
        return $data=[
            'moyenne'=>$moyenne,
            'eval_ef'=>$eval_ef,
            'eval_sim'=>$eval_sim,
            'eval_par_qualiticien'=>$eval_par_qualiticien,
            'eval_par_semaine'=>$eval_par_semaine,
            'eval_par_mois'=>$eval_par_mois,
            // 'noteNoteArray' => $noteNoteArray, // AQ % NAQ %....
            // 'itemsNames'=> $items,
            'moyenne_par_mois'=>$moyenne_par_mois,
            'agents'=>$agents

        ];
    }
    public function storetask(Request $req){
        return auth('api')->user();
    }
    public function reportingGlobalConsta(Request $req){
        // get date
        $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
        if(isset($req->input('dateRange')[1])) {
            $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
        } else {
            $endDate = $startDate;
        }
        $users =  User::all();
        foreach($users as $user){
            $idqualiticiens[]= $user->id;
        }
        $constats = Constat::whereBetween('created_at', [ $startDate, $endDate])->get();
// foreach($constats as $consta){
//     return $consta->activite;
// }
       $constat_per_user= $this->constaperUser($constats);
     $constat_per_activite =  $this->constatParActivite($constats);
 
       $data = [ 'constats_per_qual' => $constat_per_user, 'constat_per_activite'=>$constat_per_activite];
       return $data;
   
    }
    public function reportingConstatActivity(Request $req){
      
        $user = auth('api')->user();

        $users =  User::all();
        foreach($users as $user){
            $idqualiticiens[]= $user->id;
        }

        // check if range date or year
        $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
        if(isset($req->input('dateRange')[1])) {
            $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
        } else {
            $endDate = $startDate;
        }
        // fetch by 'date_appel' if switchDate is true else fetch by created_at (=date_eval)
      $constats = Constat::where('activite_id',$req->activityId)->whereBetween('created_at', [ $startDate, $endDate])->get();
      $constatstraite = Constat::where('activite_id',$req->activityId)->where('statu','1')->whereBetween('created_at', [ $startDate, $endDate])->get();
      $constatsnontraite = Constat::where('activite_id',$req->activityId)->where('statu','0')->whereBetween('created_at', [ $startDate, $endDate])->get();
     
      $groupedbyService = $constats->groupBy( function ($eval) {
        return $eval->service->nom;
    });
 
    $constat_par_service = $groupedbyService->map(function ($item, $key) {
        return collect($item)->count();
    });
  $constats_per_qual=  $this->constaperUser($constats);
  $parmois = $this->nbrevalMoyenneParMois($constats);
  $parmois = $this->nbrevalMoyenneParMois($constats);
  $constats_par_type = $this->constatsParType($constats);
  $constats_par_mois = $parmois[0];
      $data = [
        'countnontraite'=> $constatsnontraite->count(),
        'counttraite'=>$constatstraite->count(),
        'constat_per_service'=>$constat_par_service,
        'constats_per_qual'=>$constats_per_qual,
        'constats_par_mois'=>$constats_par_mois,
        'constats_par_type'=>$constats_par_type,


    ];

    return $data;

       

    }
    public function constatsParType($constats){
        $groupedbySite = $constats->groupBy( function ($eval) {
            return $eval->type_constat;
        });
        $eval_par_site = $groupedbySite->map(function ($item, $key) {
            return collect($item)->count();
        });
        return $eval_par_site;
    }
    public function reportingConstatTeleconseiller(Request $req){

        $agent_id = $req->input('agent');
        // check if range date or year
        $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
        if(isset($req->input('dateRange')[1])) {
            $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
        } else {
            $endDate = $startDate;
        }


        $constats = Constat::where('agent_id',$agent_id)->whereBetween('created_at', [ $startDate, $endDate])->get();
        $constatstraite = Constat::where('agent_id',$agent_id)->where('statu','1')->whereBetween('created_at', [ $startDate, $endDate])->get();
        $constatsnontraite = Constat::where('agent_id',$agent_id)->where('statu','0')->whereBetween('created_at', [ $startDate, $endDate])->get();
      
            $parmois = $this->nbrevalMoyenneParMois($constats);

            $constats_par_type = $this->constatsParType($constats);
            $constats_par_mois = $parmois[0];
          ;
        $data = [
        'countnontraite'=>$constatsnontraite->count(),
        'counttraite'=>$constatstraite->count(),
        'constats_par_type'=> $constats_par_type,
        'constats_par_mois'=> $constats_par_mois,
    
        ];
        return $data;
    }
    public function reportingItem(Request $req){
     
        $service = Service::find(json_decode($req->serviceId)->id);
        $grille_id = $service->grille->id;
      
        $grille = Grille::find($grille_id);
        
        $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
        if(isset($req->input('dateRange')[1])) {
            $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
        } else {
            $endDate = $startDate;
        }
        if( json_decode($req->serviceId)->id==56){
            $evaluations = Escda::whereBetween('created_at' ,[$startDate,$endDate])->where('agent_id',json_decode($req->agent)->id)->where('grille_id',$grille->id)->get();
           
            if( count($evaluations)==0) return 0 ;
            $groupbySemaine = $evaluations->groupBy( function ($eval) {
                return $eval->semaine;
            });
            foreach($groupbySemaine as $key=>$evaluations_by_semaine){
                $reporItemByweek ['Semaine '.$key] =  $this->itemsNotes3($evaluations_by_semaine);
            }
        
            $itemsNotesWeek=$this->itemsNotesWeek2($groupbySemaine);
         
            $escda = true;
           
        }
        else {
            $evaluations = Evaluation::whereBetween('created_at' ,[$startDate,$endDate])->where('agent_id',json_decode($req->agent)->id)->where('grille_id',$grille->id)->get();
            if( count($evaluations)==0) return 0 ;
            $groupbySemaine = $evaluations->groupBy( function ($eval) {
                return $eval->semaine;
            });
         foreach($groupbySemaine as $key=>$evaluations_by_semaine){
            $reporItemByweek ['Semaine '.$key] =  $this->itemsNotes($evaluations_by_semaine);
        }
        $itemsNotesWeek=$this->itemsNotesWeek($groupbySemaine);
       
        $escda = false;
        }

         foreach($reporItemByweek as $key=>$itemsAndNotes){
            $noteNoteArray[$key] = $itemsAndNotes[0];
            $items = $itemsAndNotes[1];
           
         }
         $agent = Agent::find(json_decode($req->agent)->id);
        
            return response()->json([
                'notes'=>$noteNoteArray,
                'items'=>$itemsAndNotes,
                'itemsNotesWeek'=>$itemsNotesWeek,
                'agent'=>$agent,
                'escda'=>$escda
                
            ]);
                 

    }
    public function reportingItem2(Request $req){
        
        $service = Service::find($req->serviceId);
        $grille_id = $service->grille->id;
      
        $grille = Grille::find($grille_id);
        
        $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
        if(isset($req->input('dateRange')[1])) {
            $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
        } else {
            $endDate = $startDate;
        }
        if( $req->serviceId==56){
            $evaluations = Escda::whereBetween('created_at' ,[$startDate,$endDate])->where('grille_id',$grille->id)->get();
           
            if( count($evaluations)==0) return 'no data found';
        
           
        }
        else {
            $evaluations = Evaluation::whereBetween('created_at' ,[$startDate,$endDate])->where('grille_id',$grille->id)->get();
            if( count($evaluations)==0) return 'no data found' ;        
        }
        $agent = [];
        $found =true;
      foreach($evaluations as $evaluation){
          $found= false;
         foreach($evaluation->notes as $note){
            if($note->note === json_decode($req->note) && $note->element_id === $req->element_id)
            $found =true;
         }
         if($found){
             $agent[] =[
               'nom'=> $evaluation->agent->nom,
               'prenom'=> $evaluation->agent->prenom,
               'matricule'=> $evaluation->agent->matricule
             ];
         }
      }
      if( count($agent)==0) return 'no data found' ; 
      return $agent;
    }
    public function itemsNotesWeek($evals){
               
        foreach($evals as $key=>$eval){
            $notegroupsByweek['Semaine'.$key] = $eval->map(function($item){
                return $item->notes;
            });
        }

        foreach($notegroupsByweek as $key=>$notebyweek){
            
            foreach( $notebyweek as $note){
                $GroupByElement[$key][] = $note->groupBy( function ($eval) {
                    return $eval->element_id;
                });
            }
        }
      
        foreach($notegroupsByweek as $key=> $notes_in_eval){
            foreach($notes_in_eval as $note){
              foreach($note as $n){
                $id = DB::table('grille_element_grille')->select('element_grille_id as id')->where('id',$n->element_id)->get();
               
                $el = ElementGrille::find($id[0]->id);
              
                $notesArray[$key][$el->nom][] = $n->note;
              }
            }
        }


 
       foreach($notesArray as $key=>$note){
        $xyaxis[] = $key;
// key : semaine
foreach($note as $key2=>$n){
    $counted[$key2][$key] = array_count_values($n);
      if(!isset($counted[$key2][$key]['AQ'])){
                $counted[$key2][$key]['AQ'] = 0;
            }
           
         
           
            
            if(  !isset($counted[$key2][$key]['NAQ'] )){
                $counted[$key2][$key]['NAQ'] = 0;
            }
            if(!isset($counted[$key2][$key]['NA'] )){
                $counted[$key2][$key]['NA'] = 0;
            }
            if(!isset($counted[$key2][$key]['SI'] )){
                $counted[$key2][$key]['SI'] = 0;
            }
            if(!isset($counted[$key2][$key]['PA'] )){
                $counted[$key2][$key]['PA'] = 0;
            }
            $total[$key2][$key] = $counted[$key2][$key]['AQ'] + $counted[$key2][$key]['NAQ'] + $counted[$key2][$key]['PA'] + $counted[$key2][$key]['SI'] + $counted[$key2][$key]['NA'] ;
                $noteNoteArray[$key2][$key]['aq'][] = round((100*(int)$counted[$key2][$key]['AQ'])/(int)$total[$key2][$key],2);
                $noteNoteArray[$key2][$key]['naq'][] = round((100*(int)$counted[$key2][$key]['NAQ'])/(int)$total[$key2][$key],2);
                $noteNoteArray[$key2][$key]['na'][] = round((100*(int)$counted[$key2][$key]['NA'])/(int)$total[$key2][$key],2);
                $noteNoteArray[$key2][$key]['pa'][] = round((100*(int)$counted[$key2][$key]['PA'])/(int)$total[$key2][$key],2);
                $noteNoteArray[$key2][$key]['si'][] = round((100*(int)$counted[$key2][$key]['SI'])/(int)$total[$key2][$key],2);
}
     
           


            
    
       
       }
       
  return $noteNoteArray;

     
   
    }
    public function itemsNotesWeek2($evals) {
        foreach($evals as $key=>$eval){
            $notegroupsByweek['Semaine'.$key] = $eval->map(function($item){
                return $item->notes;
            });
        }

        foreach($notegroupsByweek as $key=>$notebyweek){
            
            foreach( $notebyweek as $note){
                $GroupByElement[$key][] = $note->groupBy( function ($eval) {
                    return $eval->element_id;
                });
            }
        }
      
        foreach($notegroupsByweek as $key=> $notes_in_eval){
            foreach($notes_in_eval as $note){
              foreach($note as $n){
                $id = DB::table('grille_element_grille')->select('element_grille_id as id')->where('id',$n->element_id)->get();
               
                $el = ElementGrille::find($id[0]->id);
              
                $notesArray[$key][$el->nom][] = $n->note;
              }
            }
        }


 
       foreach($notesArray as $key=>$note){
        $xyaxis[] = $key;
// key : semaine
foreach($note as $key2=>$n){
    $counted[$key2][$key] = array_count_values($n);
      if(!isset($counted[$key2][$key]['AQ'])){
                $counted[$key2][$key]['AQ'] = 0;
            }
           
         
           
            
            if(  !isset($counted[$key2][$key]['NAQ'] )){
                $counted[$key2][$key]['NAQ'] = 0;
            }
            if(!isset($counted[$key2][$key]['NA'] )){
                $counted[$key2][$key]['NA'] = 0;
            }
            if(!isset($counted[$key2][$key]['SI'] )){
                $counted[$key2][$key]['SI'] = 0;
            }
            if(!isset($counted[$key2][$key]['PA'] )){
                $counted[$key2][$key]['PA'] = 0;
            }
            $total[$key2][$key] = $counted[$key2][$key]['AQ'] + $counted[$key2][$key]['NAQ'] + $counted[$key2][$key]['PA'] + $counted[$key2][$key]['SI'] + $counted[$key2][$key]['NA'] ;
                $noteNoteArray[$key2][$key]['OK'][] = round((100*(int)$counted[$key2][$key]['AQ'])/(int)$total[$key2][$key],2);
                $noteNoteArray[$key2][$key]['KO'][] = round((100*(int)$counted[$key2][$key]['NAQ'])/(int)$total[$key2][$key],2);
              
}
     
           


            
    
       
       }
       
  return $noteNoteArray;

     
    }

        public function ByBlocNew(Request $req){
            $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
            if(isset($req->input('dateRange')[1])) {
                $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
            } else {
                $endDate = $startDate;
            }
            $service = Service::find($req->serviceId);
            $grille_id = $service->grille->id;
            $grille = Grille::find($grille_id);
            $evaluations = Evaluation::whereBetween('created_at' ,[$startDate,$endDate])->where('grille_id',$grille->id)->get();
            $groupbyAgent = $evaluations->groupBy( function ($eval) {
                return $eval->agent->nom .' '.  $eval->agent->prenom;
            });
            $data = [];
            foreach($groupbyAgent as $key=> $evals_agent){
                $data[$key] = $this->itemsNotes($evals_agent) ;
            }
            return $data;
            
            

        }
}
