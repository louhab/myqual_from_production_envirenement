<?php

namespace App\Http\Controllers;

use App\UserVisite;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserVisiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countVisiteUsers = UserVisite::all();
        $data = [];
        foreach($countVisiteUsers as $visite){
            $data[]=[
               'user'=>$visite->user->nom. ''.$visite->user->prenom,
               'count'=>$visite->count_visite,
               
               'date'=>Carbon::createFromFormat('Y-m-d H:i:s', $visite->created_at)->format('Y-m-d')
            ];
        }
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserVisite  $userVisite
     * @return \Illuminate\Http\Response
     */
    public function show(UserVisite $userVisite)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserVisite  $userVisite
     * @return \Illuminate\Http\Response
     */
    public function edit(UserVisite $userVisite)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserVisite  $userVisite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserVisite $userVisite)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserVisite  $userVisite
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserVisite $userVisite)
    {
        //
    }
}
