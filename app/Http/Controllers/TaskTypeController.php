<?php

namespace App\Http\Controllers;

use App\Task_type;
use Illuminate\Http\Request;

class TaskTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks_type = Task_type::all();
        return $tasks_type;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task_type  $task_type
     * @return \Illuminate\Http\Response
     */
    public function show(Task_type $task_type)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task_type  $task_type
     * @return \Illuminate\Http\Response
     */
    public function edit(Task_type $task_type)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task_type  $task_type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task_type $task_type)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task_type  $task_type
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task_type $task_type)
    {
        //
    }
}
