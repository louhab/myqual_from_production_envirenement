<?php

namespace App\Http\Controllers;

use App\Appel;
use App\Motif;
use App\SousMotif;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Storage ;
use DateTime ;
class AppelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {

       
        $user = auth('api')->user();
        $roles = $user->getRoleNames();
        $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
        if( sizeof($req->input('dateRange'))==1){
            $endDate = $startDate;
        }
        else {
            $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
        }
       
      
        if($roles[0]==='client') {
            
            $appels = Appel::where('activity_id', $user->activite[0]->id)->get();
        }
        else {
            $appels = Appel::whereBetween('created_at',[$startDate." 00:00:00", $endDate." 23:59:59"])->get(); 
        }

       $data = [];
        foreach($appels as $appel){
            $data[]= [
                'id'=>$appel->id,
               'service'=>$appel->service->nom,
               'activite'=>$appel->activite->nom,
               'agent'=>$appel->agent->nom.''.$appel->agent->prenom,
               'clientId'=>$appel->clientID,
               'indice'=>$appel->indice,
               'qualiticien'=>$appel->user->nom.' '.$appel->user->prenom,
            ];
        }
        return $data ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        $appel = new Appel();
        $appel->activity_id = $request->activiteId;
        $appel->service_id = $request->serviceId;
        $appel->date_appel = $request->date;
        $appel->agent_id  = $request->agentId ;
        $appel->user_id = auth('api')->user()->id;
        $appel->clientID = $request->clientId;
        $appel->indice = $request->indice;
        $fileName = Str::random(6).'_'.time().'.'.$request->file->extension();

        $path = $request->file->storeAs('uploads/appel/files', $fileName, 'public');
        $appel->url = $path;
        if($request->motifsPoint!='undefined') {$appel->moutif =Motif::where('id',$request->motifsPoint)->first()->nom;}
        if($request->SousmotifsPoint!='undefined') {$appel->sousmoutif =SousMotif::where('id',$request->SousmotifsPoint)->first()->nom;}
        if($request->motifAlpique!='undefined') {$appel->motif_alpique = $request->motifAlpique ;}
        $appel->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Appel  $appel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $appel = Appel::find($id);
      
            $data = [
                'id'=>$appel->id,
               'service'=>$appel->service,
               'activite'=>$appel->activite,
               'date'=>$appel->date_appel,
               'agent'=>$appel->agent,
               'url_file'=>asset(\Storage::url($appel->url)),
               'indice'=>$appel->indice,
               'clientId'=>$appel->clientID,
               'moutif'=>$appel->moutif,
               'sousmoutif'=>$appel->sousmoutif,
               'qualiticien'=>$appel->user->nom.' '.$appel->user->prenom,
            ];
        
        return $data ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Appel  $appel
     * @return \Illuminate\Http\Response
     */
    public function edit(Appel $appel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Appel  $appel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Appel $appel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Appel  $appel
     * @return \Illuminate\Http\Response
     */
    public function delete( Request $request)
    {
     

    

        $appel = Appel::find($request->id);
        Storage::url($appel->url);
        $appel->delete();
     
        
   
    }
}
