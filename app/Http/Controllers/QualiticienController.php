<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Qualiticien;
use App\Evaluation;
use App\User;

class QualiticienController extends Controller
{
    public function index(){
$evaluations_user = Evaluation::all('qualiticien_id');
$evaluations_user = array_values(array_unique( $evaluations_user->toArray(), SORT_REGULAR ));
 foreach($evaluations_user as $qal_user){
$user =User::where('id', $qal_user)->first();
$qualiticien_array[]=[
    'id'=>$user->id,
    'nom'=> $user->nom. ' '.$user->prenom
];
 }

        return $qualiticien_array;
    }
    public function getQualiticiens(){
        $qualiticiens = Qualiticien::all();
        $data = [];
        foreach($qualiticiens as $qualiticien) {
            $data[]= [
                'id'=>$qualiticien->id,
'nom'=> $qualiticien->nom .' '. $qualiticien->prenom,

            ];
        }
        return $data;
    }
}
