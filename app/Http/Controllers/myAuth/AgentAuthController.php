<?php

namespace App\Http\Controllers\myAuth;

use App\Agent;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AgentAuthController extends Controller
{

    /**
     * Create
     *
     * @param
     * @return [string] message
     */
    public function register(Request $request) {

        $request->validate([
            'nom' => 'required|string',
            'prenom' => 'required|string',
            'telephone' => 'required|string',
            'matricule' => 'required|string',
            'email' => 'required|string|email',
            'password' => 'required|string',
            'service_id' => 'required|string'
        ]);

        $agent = new Agent([
            'nom' => $request->nom,
            'prenom' => $request->prenom,
            'telephone' => $request->telephone,
            'matricule' => $request->matricule,
            'email' => $request->email,
            'service_id' => $request->service_id,
            'password' => bcrypt($request->password)
        ]);

        $agent->save();

        return response()->json([
            'message' => 'agent created!'
        ], 201);
    }

    /**
     * Login
     *
     * @param
     * @return [string] token
     */
    public function login(Request $request){


        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

         $credentials = request(['email', 'password']);

        if(!Auth::guard('agent')->attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);

        $user = $request->user('agent');
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;

        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);

        $token->save();

        return response()->json([
            'user' => $user,
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'user_role' => 'agent',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);

    }
}
