<?php

namespace App\Http\Controllers\myAuth;

use App\Http\Controllers\Controller;
use App\Qualiticien;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class QualiticienAuthController extends Controller
{



    /**
     * Create Qualiticien
     *
     * @param
     * @return [string] message
     */
    public function register(Request $request) {

        $request->validate([
            'nom' => 'required|string',
            'prenom' => 'required|string',
            'telephone' => 'required|string',
            'matricule' => 'required|string',
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);

        $qualiticien = new Qualiticien([
            'nom' => $request->nom,
            'prenom' => $request->prenom,
            'telephone' => $request->telephone,
            'matricule' => $request->matricule,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        $qualiticien->save();

        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }

    /**
     * login
     *
     * @param
     * @return [string] token
     */
    public function login(Request $request){

        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = request(['email', 'password']);

        if(!Auth::guard('qualiticien')->attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);

        $user = $request->user('qualiticien');
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;

        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);

        $token->save();

        return response()->json([
            'user' => $user,
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'user_role' => 'qualiticien',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);

    }
}
