<?php

namespace App\Http\Controllers\myAuth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use App\UserVisite;
use Carbon\Carbon;

class AuthController extends Controller
{
    public function login(Request $request) {

        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);
        $credentials = request(['email', 'password']);

        if(!Auth::guard('web')->attempt($credentials)) return response()->json(['message' => 'incorrect']); //status removed response()->json(['message' => 'incorrect'], 401);
        else $user = $request->user('web');
    

        if($user->statut !== "1") {
            return response()->json([
                'message' => 'disabled'
            ]); // status removed
        }

       $user_role = $user->getRoleNames()[0];
       if($user_role === 'client') {
        if($user_visite = UserVisite::where('user_id',$user->id)->first()){
          $user_visite = UserVisite::where('user_id',$user->id)->first();
          $user_visite->count_visite = $user_visite->count_visite +1 ;
          $user_visite->save();
        }
        else {
            $user_visite = new UserVisite();
            $user_visite->user_id = $user->id;
            $user_visite->count_visite = 1 ;
            $user_visite->save();
        }
    }
  
     
        
        $permissions = [];
        foreach( $user->getAllPermissions() as $permission) {
            $permissions[] = ['action' => 'manage','subject'=>$permission->name];
        }
     

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        return response()->json([
            'message' => 'authorized',
            'user' => ['id' => $user->id, 'email' => $user->email, 'name' => $user->nom.' '.$user->prenom],
            'user_role' => $user_role,
            'ability' => $permissions,
            'token_type' => 'Bearer',
            'access_token' => $tokenResult->accessToken,
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);

        //return $user_role;
        //$user->hasRole('admin');

        /*
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'role' => 'required|string'
        ]);

        $credentials = request(['email', 'password']);

        if($request->input('role') == 'admin') {
            if(!Auth::guard('admin')->attempt($credentials))
                return response()->json([
                    'message' => 'Unauthorized'
                ], 401);
            $user = $request->user('admin');
        } elseif ($request->input('role') == 'qualiticien') {

            if(!Auth::guard('qualiticien')->attempt($credentials))
                return response()->json([
                    'message' => 'Unauthorized'
                ], 401);

            $user = $request->user('qualiticien');
        } else {
            if(!Auth::guard('agent')->attempt($credentials))
                return response()->json([
                    'message' => 'Unauthorized'
                ], 401);

            $user = $request->user('agent');
        }

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        return response()->json([
            'user' => $user,
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'user_role' => $request->input('role'),
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
         */
    }
    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->validate([
            'role' => "required" ,
        ]);

        if($request->input('role') == 'agent') {
            $request->user('agent-api')->token()->revoke();
            return response()->json([
                'message' => 'agent Successfully logged out'
            ]);
        } else {
            $request->user('api')->token()->revoke();
            return response()->json([
                'message' => $request->input('role').' Successfully logged out'
            ]);
        }
        /*
        $request->validate([
        'role' => ["required" , "in:qualiticien,admin,agent"]
        ]);

        if($request->input('role') == 'qualiticien') {
            $request->user('qualiticien-api')->token()->revoke();

            return response()->json([
                'message' => 'qual  Successfully logged out'
            ]);
        }

        if($request->input('role') == 'admin') {
            $request->user('admin-api')->token()->revoke();

            return response()->json([
                'message' => 'admin Successfully logged out'
            ]);
        }

        if($request->input('role') == 'agent') {
            $request->user('agent-api')->token()->revoke();

            return response()->json([
                'message' => 'agent Successfully logged out'
            ]);
        }

        return $request->input('role');
        */
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }
    public function hash(){
        $hashed = Hash::make('0115');
        return $hashed;
    }

}
