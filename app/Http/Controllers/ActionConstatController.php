<?php

namespace App\Http\Controllers;

use App\ActionConstat;
use Illuminate\Http\Request;
use App\Constat;
use App\User;
use App\Agent;
use Illuminate\Support\Facades\Mail;
use App\Mail\MessageGoogleAction;

class ActionConstatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $constat = Constat::find($request->constatId);
   {

        
        $actionConstat = new ActionConstat();
        $actionConstat->sup_id = $request->supId;
        $actionConstat->constat_id = $request->constatId;
  
     
        if($request->deadlineAction =='1 Jour') {
            $actionConstat->deadeline_action = date("Y-m-d", strtotime("+1 day"));
        }
        if($request->deadlineAction =='2 Jours') {
            $actionConstat->deadeline_action = date("Y-m-d", strtotime("+2 day"));
        }
        if($request->deadlineAction =='2 Jours') {
            $actionConstat->deadeline_action = date("Y-m-d", strtotime("+3 day"));
        }
        if($request->deadlineAction =='1 Semaine') {
            $actionConstat->deadeline_action= date("Y-m-d", strtotime("+1 week"));
        }
        if($request->deadlineAction =='2 Semaines') {
            $actionConstat->deadeline_action = date("Y-m-d", strtotime("+2 week"));
        }
        if($request->deadlineAction =='3 Semaines') {
            $actionConstat->deadeline_action = date("Y-m-d", strtotime("+3 week")); 
    
        }
        $actionConstat->commentaire_action = $request->commentaireAction;
        $actionConstat->action = $request->action;
        $actionConstat->avis = $request->partage;
        $actionConstat->time_action_constat = date("Y-m-d H:i:s");
        $actionConstat->save();
       
        $constat->statu = '1';
        $constat->save();
        $qualtcien = User::find($constat->qualiticien_id);
        $sup = User::find($request->supId)->nom . ' ' .User::find($request->supId)->prenom ;
        $data[]=[
        'sup'=>$sup,
        'agent'=>Agent::find($constat->agent_id)->nom . ' '.Agent::find($constat->agent_id)->prenom,
        'deadlineAction'=>$actionConstat->deadeline_action,

        ];
        
         Mail::to($qualtcien->email)->bcc("rkabtour@myopla.com")->queue(new MessageGoogleAction($data));

    

    }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\ActionConstat  $actionConstat
     * @return \Illuminate\Http\Response
     */
    public function show(ActionConstat $actionConstat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ActionConstat  $actionConstat
     * @return \Illuminate\Http\Response
     */
    public function edit(ActionConstat $actionConstat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ActionConstat  $actionConstat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ActionConstat $actionConstat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ActionConstat  $actionConstat
     * @return \Illuminate\Http\Response
     */
    public function destroy(ActionConstat $actionConstat)
    {
        //
    }
}
