<?php

namespace App\Http\Controllers;

use App\SousMotif;
use Illuminate\Http\Request;

class SousMotifController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SousMotif  $sousMotif
     * @return \Illuminate\Http\Response
     */
    public function show(SousMotif $sousMotif)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SousMotif  $sousMotif
     * @return \Illuminate\Http\Response
     */
    public function edit(SousMotif $sousMotif)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SousMotif  $sousMotif
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SousMotif $sousMotif)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SousMotif  $sousMotif
     * @return \Illuminate\Http\Response
     */
    public function destroy(SousMotif $sousMotif)
    {
        //
    }
}
