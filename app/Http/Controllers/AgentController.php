<?php

namespace App\Http\Controllers;

use App\Agent;
use Illuminate\Http\Request;
use App\Exports\AgentsExport;
use Maatwebsite\Excel\Facades\Excel;

class AgentController extends Controller
{
    public function getAllAgents(){
        $agents = Agent::where('statut',1)->get();
        $data = array();
        foreach ($agents as $agent){
            $data[] = ['id'=>$agent->id,'nom'=> $agent->nom.' '.$agent->prenom.' '.$agent->matricule,'matricule'=> $agent->matricule,'phone'=>$agent->telephone,'email'=> $agent->email, 'service'=> $agent->service_id];
        }
        return $data;
    }
    public function delete(Request $request){
        Agent::find($request->input('id'))->delete();
    }

    public function disable(Request $request){
        // this function toggle agent's status
        $a = Agent::find($request->input('id'));
        if($a->statut == '0' ) {
            $a->statut = '1';
        } else $a->statut = '0';
        $a->save();
    }

    public function exportAsCsv() {
        return Excel::download(new AgentsExport, 'agents.xlsx');
    }

    public function updateAgentsMat() {
        $agents = Agent::all();
        foreach ($agents as  $agent) {
            if($agent->site == 'TA') $agent->matricule = 'ITO_'.$agent->matricule;
            if($agent->site == 'TE') $agent->matricule = 'TET_'.$agent->matricule;
            if($agent->site == 'HO') $agent->matricule = 'MAH_'.$agent->matricule;
            $agent->save();
        }
    }
    public function escdaAgent() {
        $agents = Agent::where('service_id',24)->get();
        $data = array();
        foreach ($agents as $agent){
            $data[] = ['id'=>$agent->id,'nom'=> $agent->nom.' '.$agent->prenom.' '.$agent->matricule,'matricule'=> $agent->matricule,'phone'=>$agent->telephone,'email'=> $agent->email, 'service'=> $agent->service_id];
        }
        return $data;
    }
}
