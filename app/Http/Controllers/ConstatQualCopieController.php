<?php

namespace App\Http\Controllers;

use App\ConstatQualCopie;
use Illuminate\Http\Request;

class ConstatQualCopieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ConstatQualCopie  $constatQualCopie
     * @return \Illuminate\Http\Response
     */
    public function show(ConstatQualCopie $constatQualCopie)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ConstatQualCopie  $constatQualCopie
     * @return \Illuminate\Http\Response
     */
    public function edit(ConstatQualCopie $constatQualCopie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ConstatQualCopie  $constatQualCopie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ConstatQualCopie $constatQualCopie)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ConstatQualCopie  $constatQualCopie
     * @return \Illuminate\Http\Response
     */
    public function destroy(ConstatQualCopie $constatQualCopie)
    {
        //
    }
}
