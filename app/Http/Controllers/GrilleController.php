<?php

namespace App\Http\Controllers;

use App\Activite;
use App\ElementGrille;
use App\Grille;
use App\PartieGrille;
use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class GrilleController extends Controller
{
    public function getAllActivitiesAndTheirServices(){

        $user = auth('api')->user();
        //$activiteId = (!empty($user->activite->toArray()) ? $user->activite[0]->id : null);
        if(empty($user->activite->toArray())) {
            $activiteId = null;
        } else {
            $activiteId = [];
           foreach($user->activite as $a) {
            $activiteId[] = $a->id;
           }
        }
        if($activiteId == null) {
            // not a client nor a sup
            $activities = Activite::all();
        } else {
            $activities = Activite::findOrFail($activiteId);
        }

        //$activities = Activite::where('statut',1)->get();
        $data = array();
        foreach ($activities as $activity){
            $temp= array();
            $temp['id'] = $activity->id;
            $temp['activite'] = $activity->nom;
            $temp['services'] = $activity->services;
            $data[] = $temp;
        }
        return $data;
    }
    public function getAllActivitiesAndTheirServices2(){

        
     
           
            $activity = Activite::find(3);
        
 
        $data = array();
       
            
            $data = ['id'=>$activity->id,'activity'=>$activity->nom,'services'=> $activity->services] ;
          
        
    
        return $data;
    }
    public function getAllActivitiesAndTheirServices3(){
        $activity = Activite::find(29);
        
 
        $data = array();
       
            
            $data = ['id'=>$activity->id,'activity'=>$activity->nom,'services'=> $activity->services] ;
          
        
    
        return $data;
    }
    public function getAllActivitiesAndTheirServices4(){
        $activity = Activite::find(11);
        
 
        $data = array();
       
            
            $data = ['id'=>$activity->id,'activity'=>$activity->nom,'services'=> $activity->services] ;
          
        
    
        return $data;
    }
    public function getAllGridelements(){
        $parties = PartieGrille::all('id','nom');
        $data = array();

        foreach ($parties as $partie){
            $data['parties'][]=['id'=>$partie->id,'partie'=>$partie->nom];
            $temp= array();
            foreach ($partie->elements as $element){
                $temp[$partie->id][]=['id'=>$element->id,'nom'=>$element->nom];
            }
            $data['elements'][] = $temp;
        }

        $data['test']=$parties;
        return $data;
    }
    // to create the form for 'grille' creation
    public function create(){
        $elements = ElementGrille::all('id','nom');//->count();
        $activites = Activite::select('id','nom')->where('statut',1)->get();
        $services = Service::select('id','nom','activite_id')->where('statut',1)->get();

        $servicesmap =array();
        foreach ($services as $service){
            $servicesmap[$service->activite_id][]=['id'=>$service->id,'service'=>$service->nom];
        }

        $data = array();
        $data['activites']=$activites;
        $data['services']=$servicesmap;
        $data['elements']=$elements;

        return $data;
    }

    //store Grille
    public function store(Request $request){

        $items = $request->input('items');
        $service_id = $request->input('service_id');
        // if(Grille::where('service_id', $service_id)->count()>0) return '-1';
        $grille = new Grille();
        $grille->service_id = $service_id;

        $grille->save();
        $coefficient = 100/((float)$request->input('totalNote'));
    // return $items;
        // add elements, 'bareme' and 'glossaire' to pivot table
        foreach ($items as $item){
           $note = 0;
           $note = (float)$item['bareme'];
           $note = $note*$coefficient;

          $grille->elements()->attach($item['id_element'], ['bareme' => $note,'ok'=>$item['g_ok'], 'pa'=>$item['g_pa'],'ko'=>$item['g_ko'],'si'=>$item['g_si'],'na'=>$item['g_na']]);
        }
        return  '1';

        /* $nom_grille = $request->input('nom');
        $service_id = $request->input('service_id');
        // testing if there's already a 'grille" linked to this service
        if(Grille::where('service_id', $service_id)->count()>0) return '-1';
        $grille = new Grille();
        $grille->nom = $nom_grille ;
        $grille->service_id = $service_id;
        $grille->save();
        // element et leur bareme
        $elemets_id = $request->input('element_id', []);
        $elemets_note = $request->input('element_note', []);


         for ($i=0; $i < count($elemets_id); $i++){

             if ($elemets_id[$i] != '') {
                 //save grille_element_grille into the pivot table
                 $grille->elements()->attach($elemets_id[$i], ['bareme' => $elemets_note[$i]]);
             }
         }

         return $grille->id; */

    }
    // to check if a service has a grid or not
    public function HasNotGrid($service_id){
        if(Grille::where('service_id', $service_id)->count()>0) return false;
        else return true;
    }
    public function gridsListe(){
        $grilles = Grille::all();
        $data = array();
        foreach( $grilles as $grille){
            $data[] = [
                'id'=> $grille->id,
                'service' => $grille->service->nom,
                'activite' => $grille->service->activite->nom,
                'created_at' => date('d/m/Y', strtotime($grille->created_at)),
                'updated_at' => date('d/m/Y', strtotime($grille->updated_at)),
            ];
        }
        return $data;
    }

    public function editGrid($id){
       
        $grille = Grille::find($id);
        if(!$grille) return null;

        $data = array();
        $data['title'] = ['service'=> $grille->service->nom,'activite' => $grille->service->activite->nom];
        
        $elpartmap = array();
        $i = 1;
        foreach($grille->elements as $element){
            $data['items'][] = [
                'id' => $i,
                'id_partie' => $element->partie->id,
                'id_element' => $element->id,
                'id_element_pivot' => $element->pivot->id,
                'g_ok' => $element->pivot->ok,
                'g_pa' => $element->pivot->pa,
                'g_ko' => $element->pivot->ko,
                'g_si' => $element->pivot->si,
                'g_na' => $element->pivot->na,
                'bareme'=>$element->pivot->bareme,
            ];
            $i++;
        }
        return $data;
    }
    public function updateGrid(Request $request){
        //dd($request);
        // grille id and items
        $items = $request->input('items');
        $grille = Grille::find($request->input('grille'));
        $coefficient = round(100/((float)$request->input('totalNote')), 2);
        //dd($coefficient);
        // add elements, 'bareme' and 'glossaire' to pivot table

        $currentItemsId = []; // this aaray will strore the new items pivot table ids: with this table we can delete od items

        $affected = DB::table('grille_element_grille')->where('id', 2)->update(['na' => 'from DB::']);

        foreach ($items as $item){

            // check if the element is a new one.
        //    $note = 0;
         $note = (float)$item['bareme'];
        //    $note =;
$note =  round($note*$coefficient, 2);
           if(isset($item['id_element_pivot'])){
            DB::table('grille_element_grille')->where('id', $item['id_element_pivot'])->update(['element_grille_id' => $item['id_element'],'bareme' => $note,'ok'=>$item['g_ok'], 'pa'=>$item['g_pa'],'ko'=>$item['g_ko'],'si'=>$item['g_si'],'na'=>$item['g_na']]);
            $currentItemsId[] = $item['id_element_pivot'];
           } else {
            $id = DB::table('grille_element_grille')->insertGetId(['grille_id' => $grille->id ,'element_grille_id' => $item['id_element'],'bareme' => $note,'ok'=>$item['g_ok'],'pa'=>$item['g_pa'],'ko'=>$item['g_ko'],'si'=>$item['g_si'],'na'=>$item['g_na']]);
            $currentItemsId[] = $id;
           }
        }
        // get the updated id and these wherenotin with those id and grille id
        return  1;
    }
    public function DeleteItem($GridId,$ItemId){
           
        $grille = Grille::find($GridId);
        foreach($grille->elements  as $element){
            if($element->id == $ItemId){
                $grille->elements()->detach($element);

            }
        }
      
    }

}
