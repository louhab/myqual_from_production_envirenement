<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Qualiticien;
use App\Admin;
use App\Evaluation;
use App\Agent;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
use Hash;

class RolesTest extends Controller
{
    public function moveQstoUsers() {
        $qualiticiens = Qualiticien::all();
        foreach($qualiticiens as $q) {
            $tempQ =  User::firstOrNew(['id'=> $q->id]);
	        $tempQ->id = $q->id;
            $tempQ->nom =  $q->nom;
            $tempQ->prenom =  $q->prenom;
            $tempQ->telephone =  $q->telephone;
            $tempQ->matricule =  $q->matricule;
            $tempQ->email =  $q->email;
            $tempQ->password =  $q->password;
            $role = Role::where('name','qualiticien')->first();
            $tempQ->assignRole($role);
            $tempQ->save();
        }
        return 'qualiticiens moved to users';
    }
    public function moveAdminsToUsers() {

        $admins = Admin::all();

        foreach($admins as $a) {
            $tempA = User::firstOrNew(['email'=> $a->email]);
            //$tempA->id = $a->id;
            $tempA->nom =  $a->nom;
            $tempA->prenom =  $a->prenom;
            $tempA->telephone =  $a->telephone;
            $tempA->matricule =  $a->matricule;
            $tempA->password =  $a->password;
            $role = Role::where('name','admin')->first();
            $tempA->assignRole($role);
            $tempA->save();
        }
        return 'admins moved to users';
    }
    /*
    public function moveQualiticienIdToUserIdOnevalTable() {
        $evals = Evaluation::all();
        foreach($evals as $e) {
            $e->user_qualiticien_id = $e->qualiticien_id ;
            $e->save();
        }
    }*/

    public function testmiddleware() {
        return 'passsed';
    }

   public function test(){

        $user = User::find(1);

        /*$role = Role::find(3);//where('name','admin')->get();*/
        //$role = Role::create(['guard_name' => 'api', 'name' => 'manager']);
        //$permissions = Permission::find([2,3,4,5,6,7,8,9,10]);
        //$role->syncPermissions($permissions);

        $user->assignRole([8]);
        return $user;
        /*

        $permissions = Permission::pluck('id','id')->all();
        $role->syncPermissions($permissions);
        $user->assignRole([$role->id]);

        */
   }

   public function createPermissionsAndRoles() {
        // create permissions
        $permissions = [
            'Auth',
            'eval-edit',
            'eval',
            'liste-eval',
            'myevals',
            'eval-edit',
            'reporting-g',
            'reporting-a',
            'reporting-s',
            'reporting-t',
            'users',
            'grilles',
            'activity',
            'homeadmin',
            'homequaliticien',
            'homeagent',
            'homeclient',
            'homesup',
            'myeval-agent',
            'add-tache',
            'mytask',
            'liste-tasks',
            'calibrage-evals',
            'edit-task',
            'calibrage-liste',
            'my-calib',
            'detail-calibrage',
            'rapport-tache',
            'escda',
            'my-evals-escda',
            'evals-escda',
            'elu-escda',
            'my-evals-elu-escda',
            'evals-elu-escda',
            'constat-add',
            'my-consta',
            'constat-liste', 
            'detailconstat',
            'constat-copie',
            'liste-constat-action',
            'reporting-c-g',
            'reporting-c-a',
            'reporting-c-s',
            'reporting-c-t',
            'reporting-item',
            'consolidation',
            'costumer-feedback',
            'costumer-feedback-deatils',
            'my-costumer-feedback',
            'reporting-calib',
            'my-esda-details',
            'add-apel',
            'detail-apel',
            'appel-list',
            'escda-la-redoute',
            'my-escda-la-redoute',
            'liste-escda-la-redoute',

        ];
        foreach ($permissions as $permission) {
            Permission::firstOrCreate(['name' => $permission, 'guard_name' => 'api']);
        }
     // $adminpermission = Permission::whereIn('name',['Auth','escda-la-redoute',  'detail-apel', 'appel-list', 'my-escda-la-redoute',  'liste-escda-la-redoute', 'add-apel', 'costumer-feedback', 'my-esda-details','reporting-calib', 'my-costumer-feedback','costumer-feedback-deatils' , 'evals-escda', 'evals-elu-escda','my-evals-elu-escda', 'my-evals-escda', 'consolidation','reporting-item','constat-copie','escda', 'elu-escda', 'reporting-c-g','reporting-c-a','reporting-c-s','reporting-c-t','constat-add',  'detailconstat','constat-liste' , 'my-consta','rapport-tache', 'detail-calibrage','my-calib','edit-task','calibrage-liste','calibrage-evals' ,'liste-tasks', 'mytask', 'add-tache','homeadmin','reporting-g','reporting-a','reporting-s','reporting-t','users','grilles','activity','eval','liste-eval','eval-edit','myevals','doc-list','doc-view','doc-add'])->get();
   // $qualticienpermission = Permission::whereIn('name',['Auth', 'liste-eval', 'escda-la-redoute',    'detail-apel', 'appel-list', 'my-escda-la-redoute',  'liste-escda-la-redoute', 'my-evals-escda', 'add-apel','reporting-calib', 'my-esda-details', 'my-evals-elu-escda',  'my-costumer-feedback', 'costumer-feedback-deatils','costumer-feedback','consolidation' ,'calibrage-liste' , 'reporting-item', 'constat-copie','escda', 'elu-escda', 'reporting-c-g','reporting-c-a','reporting-c-s','reporting-c-t' , 'constat-add' , 'detailconstat','my-consta','detail-calibrage','my-calib','edit-task', 'calibrage-evals', 'add-tache', 'mytask', 'homequaliticien','reporting-g','reporting-a','reporting-s','reporting-t','eval','eval-edit','myevals'])->get();
// $clientpermission = Permission::whereIn('name',['Auth'  , 'add-apel', 'detail-apel', 'appel-list','calibrage-liste', 'reporting-item','calibrage-evals','my-calib','homeclient','reporting-t', 'detail-calibrage' ,'reporting-a','reporting-s','liste-eval','myevals','eval','eval-edit'])->get();
 $suppermission = Permission::whereIn('name',['Auth', 'calibrage-liste' , 'reporting-item' , 'detail-apel', 'appel-list' , 'add-apel' ,'detailconstat', 'my-evals-escda','constat-copie' ,'liste-constat-action' ,'homesup','escda', 'detail-calibrage','reporting-a', 'my-calib' ,'calibrage-evals' ,'reporting-s','reporting-t','liste-eval','myevals','eval','eval-edit'])->get();
       // $agentpermission = Permission::whereIN('name',['Auth','my-esda-details','homeagent','myeval-agent','eval-edit',])->get();
        // create Roles and asign permissions
        $roles = [
        //   ['admin',$adminpermission],
// ['qualiticien',$qualticienpermission],
    
     
      ['superviseur',$suppermission],
//        ['client',$clientpermission],
      //   ['agent',$agentpermission],
        ];

        foreach ($roles as $role) {
            $r = Role::firstOrNew(['name' => $role[0], 'guard_name' => 'api']);
            $r->syncPermissions($role[1]);
	    $r->save();
        }
   }
   public function moveAgentsToUsers() {

    $agent = Agent::all();

    foreach($agent as $a) {
        $tempA = User::firstOrNew(['email'=> $a->email]);
        //$tempA->id = $a->id;
        $tempA->nom =  $a->nom;
        $tempA->prenom =  $a->prenom;
        $tempA->telephone =  $a->telephone;
        $tempA->matricule =  $a->matricule;
        $tempA->password =  password_hash('password',PASSWORD_DEFAULT);
        $role = Role::where('name','agent')->first();
        $tempA->assignRole($role);
        $tempA->save();
    }
    return 'agent moved to users';
}
public function assingRole(){
    $user =User::find(59);
    $role = Role::where('name','superviseur')->first();
    $user->assignRole($role);
}
public function getAgent() {
    $agent =Agent::find(778);

}


}
