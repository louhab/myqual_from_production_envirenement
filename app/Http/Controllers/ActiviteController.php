<?php

namespace App\Http\Controllers;

use App\Activite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ActiviteController extends Controller
{
    // return active activities
    public function index(){
        $activities = Activite::all();
    
        foreach($activities as $activite){
            $data[]=[
                'id'=>$activite->id,
                'nom'=>$activite->nom,
                'statut'=>$activite->statut,
                'statut2'=>$activite->statut,
                'created_at'=>$activite->created_at,
            ];
        }
        return $data;
    }

    //return all activities including none active ones
    public function allActivities(){
        // update : if user is a client or sup return only his activity
        $user = auth('api')->user();
        //$activiteId = (!empty($user->activite->toArray()) ? $user->activite[0]->id : null);

        if(empty($user->activite->toArray())) {
            $activiteId = null;
        } else {
            $activiteId = [];
           foreach($user->activite as $a) {
            $activiteId[] = $a->id;
           }
        }
        if($activiteId == null) {
            // not a client nor a sup
            $activities = Activite::all();
        } else {
            $activities = Activite::find($activiteId);
        }
        return $activities;
    }

    //store activite in db
    public function store(Request $request){
        $nom = $request->input('nom');

        if(Activite::where('nom',$nom)->exists()){
            return '-1';
        }
        $newact = Activite::create([
            'nom' => $nom ,
        ]);
        if($newact) return true;

    }

    // get an activity services
    public function getServices($id){
        $activite = Activite::find($id);
        $data = array();
        $data['activite']=['id'=>$id,'nom'=>$activite->nom,'statut'=>$activite->statut , 'created_at' => $activite->created_at];
        $data['services'] = $activite->services;
        //-feature to add- filter by status or do it in vue
        return $data;
    }

    //change activite status

    public function toggleStatus($id){
        $activite = Activite::find($id);
        if($activite->statut==1) {
            $activite->statut = 0 ;
            $activite->save();
            return 'activity disabled';
        }else{
            $activite->statut = 1 ;
            $activite->save();
            return 'activity enabled';
        }
    }
public function edit(Request $request){
  $activite =Activite::find($request->id);
  $activite->nom= $request->nom;
  $activite->statut = $request->statut;
  $activite->save();
  
}


}
