<?php

namespace App\Http\Controllers;

use App\NoteLaredouteEscda;
use Illuminate\Http\Request;

class NoteLaredouteEscdaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\NoteLaredouteEscda  $noteLaredouteEscda
     * @return \Illuminate\Http\Response
     */
    public function show(NoteLaredouteEscda $noteLaredouteEscda)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NoteLaredouteEscda  $noteLaredouteEscda
     * @return \Illuminate\Http\Response
     */
    public function edit(NoteLaredouteEscda $noteLaredouteEscda)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\NoteLaredouteEscda  $noteLaredouteEscda
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NoteLaredouteEscda $noteLaredouteEscda)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NoteLaredouteEscda  $noteLaredouteEscda
     * @return \Illuminate\Http\Response
     */
    public function destroy(NoteLaredouteEscda $noteLaredouteEscda)
    {
        //
    }
}
