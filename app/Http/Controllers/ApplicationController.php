<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use App\Calibrage;
use App\Service;
use App\Note;
use App\NoteCalibrage;
use Carbon\Carbon;

class ApplicationController extends Controller
{
    public function index()
    {
        return view('application');
    }
    public function pdf() {
        //





      
            $eval1 = Calibrage::find(73);
            $evals = Calibrage::where('client_id',$eval1->client_id)->where('agent_id',$eval1->agent_id)->get();
            $finale_calib =Calibrage::where('finale_calib',1)->where('client_id',$eval1->client_id)->where('agent_id',$eval1->agent_id)->first();
            $finale_calib_array=[
                'id'=>$finale_calib->id,
                'note'=>$finale_calib->note_global,
                'agent'=>$finale_calib->agent->nom.' '.$finale_calib->agent->prenom,
                'qualiticien'=>$finale_calib->qualiticien->nom.' '.$finale_calib->qualiticien->prenom,
                'service'=>$finale_calib->grille->service->nom,
                'client_id'=>$finale_calib->client_id,
                'date_appel'=>Carbon::createFromFormat('Y-m-d', $finale_calib->date_appel)->format('d/m/Y'),//$eval->date_appel,
                'date_eval'=> Carbon::createFromFormat('Y-m-d H:i:s', $finale_calib->created_at)->format('d/m/Y'),
            ];
            foreach($evals as $eval) {
                
                $data[] =[
                    'id'=>$eval->id,
                    'note'=>$eval->note_global,
                    'agent'=>$eval->agent->nom.' '.$eval->agent->prenom,
                    'qualiticien'=>$eval->qualiticien->nom.' '.$eval->qualiticien->prenom,
                    'service'=>$eval->grille->service->nom,
                    'client_id'=>$eval->client_id,
                    'date_appel'=>Carbon::createFromFormat('Y-m-d', $eval->date_appel)->format('d/m/Y'),//$eval->date_appel,
                    'date_eval'=> Carbon::createFromFormat('Y-m-d H:i:s', $eval->created_at)->format('d/m/Y'),
                ];
          
                

            }
            foreach($evals as $eval){
                $elements = self::create($eval->grille->service->id);
                foreach ($elements as &$item){
                    foreach ($item['elements'] as &$element){
                      $note = NoteCalibrage::select('note','comment','calibrages_id','element_id')->where(['calibrages_id'=>$eval->id,'element_id'=>$element['id']])->first();
                      $data2[$eval->id][] = [
                       'element_id'=>$note->element_id,
                        'note'         =>$note->note,
                        'comment'      =>$note->comment,
                        'calibrages_id'=>$note->calibrages_id,
                        'qualiticien'=> $eval->qualiticien->nom.' '.$eval->qualiticien->prenom,
                       ];
                 
                    }
                }
                $data3['elements'] = $elements;
            }

            return view('Calibrage.pdf3',["calibrages"=>$data,
        'notes'=>$data2,
        'elements'=>$data3,
        'finale_calib'=>$finale_calib_array
        ]);
        
  
        
    }


    public function create($id){

        $service = Service::find($id);  //service id
        // test if serve exists then if the 'grille' exists

        //preparing 'grille' elements
        $grille = $service->grille;

        $elpartmap = array();
        foreach($grille->elements as $element){
            $partie = $element->partie->nom;
            $el = ['id'=>$element->pivot->id,'nom'=>$element->nom,'g_ok'=>$element->pivot->ok,'g_pa'=>$element->pivot->pa,
            'g_ko'=>$element->pivot->ko,'g_si'=>$element->pivot->si,'g_na'=>$element->pivot->na,'bareme'=>$element->pivot->bareme];
            $testkey = null;

            foreach ($elpartmap as $key => $val) {
                if ($val['partie'] === $partie) {
                    $testkey = $key;
                    break;
                }
            }
            if(isset($testkey)){
                $elpartmap[$testkey]['elements'][] = $el;
            }else{
                $elpartmap[]=['partie'=>$partie,'elements'=>[$el]];
            }
        }

        //get all agents associated to this service
        // $agents = $service->agents; this should be sent with activities and services
         /* $agents = DB::table('agents')->select('id', 'nom','prenom','matricule')->where([
            ['statut','1'],
            ['service_id', $service->id],
        ])->get(); */

        return  $elpartmap;
    }
}
