<?php

namespace App\Http\Controllers;

use App\ConstatFile;
use Illuminate\Http\Request;

class ConstatFileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ConstatFile  $constatFile
     * @return \Illuminate\Http\Response
     */
    public function show(ConstatFile $constatFile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ConstatFile  $constatFile
     * @return \Illuminate\Http\Response
     */
    public function edit(ConstatFile $constatFile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ConstatFile  $constatFile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ConstatFile $constatFile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ConstatFile  $constatFile
     * @return \Illuminate\Http\Response
     */
    public function destroy(ConstatFile $constatFile)
    {
        //
    }
}
