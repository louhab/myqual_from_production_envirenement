<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\Exports\ServicesExport;
use Maatwebsite\Excel\Facades\Excel;

class ServiceController extends Controller
{
    //store service in db
    public function store(Request $request){
        $nom = $request->input('nom');
        $a_id = $request->input('a_id');

        if(Service::where('nom',$nom)->exists()){
            return '-1';
        }
        $new_ser = Service::create([
            'nom' => $nom ,
            'activite_id' => $a_id ,
        ]);
        if($new_ser) return true;

    }

    // return all active agents in a service
    public function getAgents($id)
    {
        $data = array();
        $service = Service::find($id);
        $data['service'] = ['id'=>$id,'nom'=>$service->nom,'statut'=>$service->statut , 'created_at' => $service->created_at];
        $data['agents'] = $service->agents;
        return $data;
    }

    public function exportServices()
    {
        return Excel::download(new ServicesExport, 'Services.xlsx');
    }
    public function edit(Request $request){
        $service =Service::find($request->id);
        $service->nom = $request->nom;
        $service->save();

    }
}
