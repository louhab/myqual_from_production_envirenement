<?php

namespace App\Http\Controllers;

use App\NoteEluEscda;
use Illuminate\Http\Request;

class NoteEluEscdaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\NoteEluEscda  $noteEluEscda
     * @return \Illuminate\Http\Response
     */
    public function show(NoteEluEscda $noteEluEscda)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NoteEluEscda  $noteEluEscda
     * @return \Illuminate\Http\Response
     */
    public function edit(NoteEluEscda $noteEluEscda)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\NoteEluEscda  $noteEluEscda
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NoteEluEscda $noteEluEscda)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NoteEluEscda  $noteEluEscda
     * @return \Illuminate\Http\Response
     */
    public function destroy(NoteEluEscda $noteEluEscda)
    {
        //
    }
}
