<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Evaluation;
use App\Note;
use App\Service;
use Carbon\Carbon;
use App\Escda;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use DateTime;

class AgentEvalController extends Controller
{

    public function myeval_agent(Request $req){
        $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
        if(isset($req->input('dateRange')[1])) {
            $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
        } else {
            $endDate = $startDate;
        }
        $user = auth('api')->user();

        // fetch by 'date_appel' if switchDate is true else fetch by created_at (=date_eval)
        if($req->switchDate == "true"){
            $date_param = 'date_appel';
        } else {
            $date_param = 'created_at';
        }
        $agent=DB::table('agents')->where('email',$req->agentEmail)->first();
      
            $evals = Evaluation::whereIn('site', $req->input('selectedSites'))->where('agent_id',$agent->id)->get();
            $ecdas = Escda::whereIn('site', $req->input('selectedSites'))->where('agent_id',$agent->id)->get();
            
        
     
        $data = array();
        foreach ($evals as $eval){
            $data[] = [
                'id'=>$eval->id,
                'note'=>$eval->note_global,
                'agent'=>$eval->agent->nom.' '.$eval->agent->prenom,
                'qualiticien'=>$eval->qualiticien->nom.' '.$eval->qualiticien->prenom,
                'service'=>$eval->grille->service->nom,
                'client_id'=>$eval->client_id,
                'date_appel'=>Carbon::createFromFormat('Y-m-d', $eval->date_appel)->format('d/m/Y'),//$eval->date_appel,
                'date_eval'=> Carbon::createFromFormat('Y-m-d H:i:s', $eval->created_at)->format('d/m/Y'),
                'escda'=>false
            ];
            if($ecdas->count() !==0 ) {
                foreach ($ecdas as $eval){
                    $data[] = [
                        'id'=>$eval->id,
                        'note'=>$eval->note_global,
                        'agent'=>$eval->agent->nom.' '.$eval->agent->prenom,
                        'qualiticien'=>$eval->qualiticien->nom.' '.$eval->qualiticien->prenom,
                        'service'=>$eval->grille->service->nom,
                        'client_id'=>$eval->client_id,
                        'date_appel'=>Carbon::createFromFormat('Y-m-d', $eval->date_appel)->format('d/m/Y'),//$eval->date_appel,
                        'date_eval'=> Carbon::createFromFormat('Y-m-d H:i:s', $eval->created_at)->format('d/m/Y'),
                        'escda'=>true
                    ];
                 
                }
            }
        }
        return $data;
    }
}
