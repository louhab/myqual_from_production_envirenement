<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Evaluation;
use App\Calibrage;
use App\Note;
use App\Service;
use App\User;
use App\Agent ;
use App\NoteCalibrage;
use Carbon\Carbon;
use App\Activite;
use App\Appel;
use App\Grille;
use DateTime;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\CalibrageExport;
use PDF;
use Illuminate\Support\Facades\DB;

class CalibrageController extends Controller
{


    public function store2(Request $request) {
        
    
     

        // return  $request->input('');

        $request->validate([
            // add validation
        ]);
        $val = new Calibrage();

        $serviceId = $request->input('serviceId');

        $items = $request->input('items');

        $val->grille_id = Service::find($serviceId)->grille->id;
        $val->client_id = $request->input('clientId');
        $val->indice_tel = $request->input('indice');
        $val->pcc = $request->input('pcc');
        $val->satcli = $request->input('satcli');
        $val->date_appel = $request->input('date');
        $val->oad = $request->input('oad');
        $val->exci = $request->input('exci');
        $val->synthese = $request->input('synthese');
        $val->agent_id = $request->input('agentId');
        $val->semaine = $request->input('weekofyear');
        $val->site = $val->agent->site;
        $val->finale_calib = 1;
        //if($request->input('role') == 'admin')  $val->qualiticien_id = 8 ;
        $val->qualiticien_id = $request->input('qualiticien');
        $val->save();

        $note_global = 0;
        $bareme_global = 0;
        $bareme_element_where_na =0;
        $bareme_element_where_AQ = 0;
        foreach ($items as $item){
            foreach ($item['elements'] as $element){ 
                $note_array[]= $element['note'];
                if($element['note'] == 'NA')  {
                    $bareme_element_where_na +=$element['bareme'];
                }
                if($element['note'] == 'AQ')  {
                    $bareme_element_where_AQ +=1;
                }
               
            }
        }
        $note_elements = [];
        $note_cal = 0;
        if( $request->input('activiteId')==11 ){
           
            foreach ($items as $item){
                foreach ($item['elements'] as $element){
                    
                    $note = new NoteCalibrage();
                    $note->calibrages_id = $val->id;
                    $note->element_id=$element['id'];
                    $note->note=$element['note'];
                    if( $note->note == 'EX' ){
                        $note_global += $element['bareme'];
                    }
                    if( $note->note == 'BON' ){
                        if($element['id']===1258 || $element['id']===1259){
                            
                            $note_global += ($element['bareme']*0.834);
                        }
                        else if($element['id']===1260){
                            $note_global += ($element['bareme']*0.6);
                        }
                        else {
                            $note_global += ($element['bareme']*0.8);
                        }
                        
                    }
                    if( $note->note == 'MOY' ){
                        if($element['id']===1260){
                            $note_global += ($element['bareme']*0.2); 
                        }
                        else if($element['id']===1259 || $element['id']===1258){
                            $note_global += ($element['bareme']*0.5);
                        }
                        else {
                            $note_global += ($element['bareme']*0.6);
                        }
                       
                    }
                    if( $note->note == 'MAU' ){
                        $note_global += ($element['bareme']*0);
                    }
                    if(isset($element['comment'])){
                        $note->comment=$element['comment'];
                    }
                    $note->save();
                    $bareme_global += $element['bareme'];
                }
            }
            
        }
        if(  $request->input('activiteId')==4 ) {
            foreach ($items as $item){
                foreach ($item['elements'] as $element){
                    $note = new NoteCalibrage();
                    $note->calibrages_id = $val->id;
                    $note->element_id=$element['id'];
                    $note->note=$element['note'];
                    if( $note->note == 'AQ' ||  $note->note == 'NA'){
                        $note_global += $element['bareme'];
                    }
                    if( $note->note == 'PA' ){
                        $note_global += ($element['bareme'])/2;
                    }
                    if(isset($element['comment'])){
                        $note->comment=$element['comment'];
                    }
                    $note->save();
                    $bareme_global += $element['bareme'];
                }
            }
    
            // applying malus here
            if($request->input('activiteId') == 7 ) {
                $note_global = $note_global - $request->input('malus')*(100/600);
            }
    
            if($request->input('sim')){
                $val->note_global = 0;
                $val->save();
                return 1;
            }
    
            $val->note_global = round($note_global, 2);
            $val->save();
            return 1;
      
        }
        else if($request->input('activiteId')==20) {


            foreach ($items as $item){
                foreach ($item['elements'] as $element){
                    $note = new NoteCalibrage();
                    $note->calibrages_id  = $val->id;
                    $note->element_id=$element['id'];
                    $note->note=$element['note'];
                            if($element['note'] != 'NA')  {
                                $note_elements [] = $element['note'];
                             }
                             if(isset($element['comment'])){
                                $note->comment=$element['comment'];
                            }
                            $note->save();
                            
                }}
       
    
            $note_cal = 100/ count($note_elements) ;
         
         
            foreach($note_elements as $no_cal) {
           
                if($no_cal == 'AQ') {
                      $note_global += $note_cal;
                }
                if($no_cal == 'PA') {
                    $note_global += ($note_cal/2) ;
                }
            }
            $bareme_global += $element['bareme'];
        }
      

else {
    foreach ($items as $item){
        foreach ($item['elements'] as $element){

            $note = new NoteCalibrage();
            $note->calibrages_id = $val->id;
            $note->element_id=$element['id'];
            $note->note=$element['note'];
         

            if( $note->note == 'AQ' ){
                if($bareme_element_where_na!=0){
                    $note_global += $element['bareme'] + ($bareme_element_where_na/$bareme_element_where_AQ);
                }
                else {
                    $note_global += $element['bareme'] ;
                }
                // note_global val 
            }
            if( $note->note == 'PA'  ){
                if($bareme_element_where_na!=0){
                $note_global += ($element['bareme']/2) ;
                }
                else {
                    $note_global += ($element['bareme']/2) ;
                }
            }
            if(isset($element['comment'])){
                $note->comment=$element['comment'];
            }
            $note->save();
            $bareme_global += $element['bareme'];
         
        }
    }
}

    
      

      


        // applying malus here
        if($request->input('activiteId') == 7 ) {

            $note_global = $note_global - $request->input('malus')*(100/600);
        }

        if($request->input('sim')){
            $val->note_global = 0;
            $val->save();
            return 1;
        }

        $val->note_global = round($note_global, 2);
        $val->save(); // note_global val 
        return 1;
     
}



    public function store(Request $request) {
        


        // return  $request->input('');

        $request->validate([
            // add validation
        ]);
        $val = new Calibrage();

        $serviceId = $request->input('serviceId');

        $items = $request->input('items');
        if($request->MotifAlpique !=null){
            $val->motif_alpique = $request->MotifAlpique['nom'] ;

        }
        else {
            $val->motif_alpique = null ;
        }
        $val->grille_id = Service::find($serviceId)->grille->id;
        $val->client_id = $request->input('clientId');
        $val->indice_tel = $request->input('indice');
        $val->pcc = $request->input('pcc');
        $val->satcli = $request->input('satcli');
        $val->date_appel = $request->input('date');
        $val->oad = $request->input('oad');
        $val->exci = $request->input('exci');
        $val->synthese = $request->input('synthese');
        $val->agent_id = $request->input('agentId');
        $val->semaine = $request->input('weekofyear');
        $val->site = $val->agent->site;
        $val->appel_id = $request->appel_id;
        
        //if($request->input('role') == 'admin')  $val->qualiticien_id = 8 ;
        $val->qualiticien_id = $request->input('qualiticien');
        $val->save();

        $note_global = 0;
        $bareme_global = 0;
        $bareme_element_where_na =0;
        $bareme_element_where_AQ = 0;
        foreach ($items as $item){
            foreach ($item['elements'] as $element){ 
                $note_array[]= $element['note'];
                if($element['note'] == 'NA')  {
                    $bareme_element_where_na +=$element['bareme'];
                }
                if($element['note'] == 'AQ')  {
                    $bareme_element_where_AQ +=1;
                }
               
            }
        }
        $note_elements = [];
        $note_cal = 0;
        if( $request->input('activiteId')==11 ){
           
            foreach ($items as $item){
                foreach ($item['elements'] as $element){
                    
                    $note = new NoteCalibrage();
                    $note->calibrages_id = $val->id;
                    $note->element_id=$element['id'];
                    $note->note=$element['note'];
                    if( $note->note == 'EX' ){
                        $note_global += $element['bareme'];
                    }
                    if( $note->note == 'BON' ){
                        if($element['id']===1258 || $element['id']===1259){
                            
                            $note_global += ($element['bareme']*0.834);
                        }
                        else if($element['id']===1260){
                            $note_global += ($element['bareme']*0.6);
                        }
                        else {
                            $note_global += ($element['bareme']*0.8);
                        }
                        
                    }
                    if( $note->note == 'MOY' ){
                        if($element['id']===1260){
                            $note_global += ($element['bareme']*0.2); 
                        }
                        else if($element['id']===1259 || $element['id']===1258){
                            $note_global += ($element['bareme']*0.5);
                        }
                        else {
                            $note_global += ($element['bareme']*0.6);
                        }
                       
                    }
                    if( $note->note == 'MAU' ){
                        $note_global += ($element['bareme']*0);
                    }
                    if(isset($element['comment'])){
                        $note->comment=$element['comment'];
                    }
                    $note->save();
                    $bareme_global += $element['bareme'];
                }
            }
            
        }
        else if(  $request->input('activiteId')==4 ) {
            foreach ($items as $item){
                foreach ($item['elements'] as $element){
                    $note = new NoteCalibrage();
                    $note->calibrages_id = $val->id;
                    $note->element_id=$element['id'];
                    $note->note=$element['note'];
                    if( $note->note == 'AQ' ||  $note->note == 'NA'){
                        $note_global += $element['bareme'];
                    }
                    if( $note->note == 'PA' ){
                        $note_global += ($element['bareme'])/2;
                    }
                    if(isset($element['comment'])){
                        $note->comment=$element['comment'];
                    }
                    $note->save();
                    $bareme_global += $element['bareme'];
                }
            }
    
            // applying malus here
            if($request->input('activiteId') == 7 ) {
                $note_global = $note_global - $request->input('malus')*(100/600);
            }
    
            if($request->input('sim')){
                $val->note_global = 0;
                $val->save();
                return 1;
            }
    
            $val->note_global = round($note_global, 2);
            $val->save();
            return 1;
      
        }
        else if($request->input('activiteId')==20 ||$request->input('activiteId')==25 ) {


            foreach ($items as $item){
                foreach ($item['elements'] as $element){
                    $note = new NoteCalibrage();
                    $note->calibrages_id  = $val->id;
                    $note->element_id=$element['id'];
                    $note->note=$element['note'];
                            if($element['note'] != 'NA')  {
                                $note_elements [] = $element['note'];
                             }
                             if(isset($element['comment'])){
                                $note->comment=$element['comment'];
                            }
                            $note->save();
                            
                }}
       
    
            $note_cal = 100/ count($note_elements) ;
         
         
            foreach($note_elements as $no_cal) {
           
                if($no_cal == 'AQ') {
                      $note_global += $note_cal;
                }
                if($no_cal == 'PA') {
                    $note_global += ($note_cal/2) ;
                }
            }
            $bareme_global += $element['bareme'];
        }
      

else {
    foreach ($items as $item){
        foreach ($item['elements'] as $element){

            $note = new NoteCalibrage();
            $note->calibrages_id = $val->id;
            $note->element_id=$element['id'];
            $note->note=$element['note'];
         

            if( $note->note == 'AQ' ){
                if($bareme_element_where_na!=0){
                    $note_global += $element['bareme'] + ($bareme_element_where_na/$bareme_element_where_AQ);
                }
                else {
                    $note_global += $element['bareme'] ;
                }
                // note_global val 
            }
            if( $note->note == 'PA'  ){
                if($bareme_element_where_na!=0){
                $note_global += ($element['bareme']/2) ;
                }
                else {
                    $note_global += ($element['bareme']/2) ;
                }
            }
            if(isset($element['comment'])){
                $note->comment=$element['comment'];
            }
            $note->save();
            $bareme_global += $element['bareme'];
         
        }
    }
}

    
 
        // applying malus here
        if($request->input('activiteId') == 7 ) {

            $note_global = $note_global - $request->input('malus')*(100/600);
        }

        if($request->input('sim')){
            $val->note_global = 0;
            $val->save();
            return 1;
        }

        $val->note_global = round($note_global, 2);
        $val->save(); // note_global val 
        return 1;
    }
  
    // $user->activite[0]->services[0]->grille->id
    public function getAllCalib(Request $req) {
            $user = auth('api')->user();
            $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
            if(isset($req->input('dateRange')[1])) {
                $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
            } else {
                $endDate = $startDate;
            }
            if($req->switchDate == "true"){
                $date_param = 'date_appel';
            } else {
                $date_param = 'created_at';
            }
            if (empty($user->activite->toArray())) {
                $evals = DB::table('calibrages')->whereBetween($date_param, [ $startDate, $endDate])->get();
                $evals = collect($evals)->groupBy('client_id')->groupBy('agent_id');  
            } 
            else {
                $activity = $user->activite[0];
                $grilles = $activity->services->map(function($service) {
                    if($service->grille){
                        return $service->grille->id;
                    }
                });         
                $evals = DB::table('calibrages')->whereBetween($date_param, [ $startDate, $endDate])->where('grille_id',$user->activite[0]->services[0]->grille->id)->get();
                $evals = collect($evals)->groupBy('client_id')->groupBy('agent_id');  
            }
           
        $data = array();
        foreach ($evals as $eval){
         foreach($eval as $key=> $calib ) {
            foreach($eval[$key] as  $ca){
                    $qualiticiens[$key][]=['qualiticien'=>User::find($ca->qualiticien_id)->nom.' '.User::find($ca->qualiticien_id)->nom];
                    $notes[$key][]=['note'=>$ca->note_global];
            }
            $data[]=[
                'id'=>$calib[0]->id,
                'client_id'=>$calib[0]->client_id ,
                    'qualiticiens'=>$qualiticiens[$key],
                    'notes'=>$notes[$key],
                    'agent'=>Agent::find($calib[0]->agent_id)->nom . ' '.Agent::find($calib[0]->agent_id)->prenom,
                    'activty'=>Grille::find($calib[0]->grille_id)->service->activite->nom,
                    'service'=>Grille::find($calib[0]->grille_id)->service->nom,
                    'date_appel'=>Carbon::createFromFormat('Y-m-d', $calib[0]->date_appel)->format('d/m/Y'),//$eval->date_appel,
                    'date_eval'=> Carbon::createFromFormat('Y-m-d H:i:s', $calib[0]->created_at)->format('d/m/Y'),
            ]; 
         }
        }
        return $data;
   
    }
    public function extraire(Request $req){
        $user = auth('api')->user();
        $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
        if(isset($req->input('dateRange')[1])) {
            $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
        } else {
            $endDate = $startDate;
        }
        if($req->switchDate == "true"){
            $date_param = 'date_appel';
        } else {
            $date_param = 'created_at';
        }
        if (empty($user->activite->toArray())) {
            $evals = Calibrage::all()->groupBy('client_id')->groupBy('agent_id');
        } 
        else {
            $activity = $user->activite[0];
            $grilles = $activity->services->map(function($service) {
                if($service->grille){
                    return $service->grille->id;
                }
            });         
            $evals = DB::table('calibrages')->whereBetween($date_param, [ $startDate, $endDate])->where('grille_id',$user->activite[0]->services[0]->grille->id)->get();
            $evals = collect($evals)->groupBy('client_id')->groupBy('agent_id');  
        }
        $data = array();
        foreach ($evals as $eval){
         foreach($eval as $key=> $calib ) {
            foreach($eval[$key] as  $ca){
                    $qualiticiens[$key][]=['qualiticien'=>User::find($ca->qualiticien_id)->nom.' '.User::find($ca->qualiticien_id)->nom];
                    $notes[$key][]=['note'=>$ca->note_global];
            }
            $data[]=[
                'id'=>$calib[0]->id,
                'client_id'=>$calib[0]->client_id ,
                    'qualiticiens'=>$qualiticiens[$key][0]['qualiticien'],
                    'notes'=>$notes[$key][0]['note'],
                    'agent'=>Agent::find($calib[0]->agent_id)->nom . ' '.Agent::find($calib[0]->agent_id)->prenom,
                    'activty'=>Grille::find($calib[0]->grille_id)->service->activite->nom,
                    'service'=>Grille::find($calib[0]->grille_id)->service->nom,
                    'date_appel'=>Carbon::createFromFormat('Y-m-d', $calib[0]->date_appel)->format('d/m/Y'),//$eval->date_appel,
                    'date_eval'=> Carbon::createFromFormat('Y-m-d H:i:s', $calib[0]->created_at)->format('d/m/Y'),
            ]; 
         }
        }
        
        return Excel::download(new CalibrageExport($data), 'Calibrage_liste.xlsx');
       
    }

    public function getCalibPdf ($id)
{

  
 


      
    $eval1 = Calibrage::find($id);
    $evals = Calibrage::where('client_id',$eval1->client_id)->where('agent_id',$eval1->agent_id)->get();
    $finale_calib =Calibrage::where('finale_calib',1)->where('client_id',$eval1->client_id)->where('agent_id',$eval1->agent_id)->first();
    $finale_calib_array=[
        'id'=>$finale_calib->id,
        'note'=>$finale_calib->note_global,
        'agent'=>$finale_calib->agent->nom.' '.$finale_calib->agent->prenom,
        'qualiticien'=>$finale_calib->qualiticien->nom.' '.$finale_calib->qualiticien->prenom,
        'service'=>$finale_calib->grille->service->nom,
        'client_id'=>$finale_calib->client_id,
        'date_appel'=>Carbon::createFromFormat('Y-m-d', $finale_calib->date_appel)->format('d/m/Y'),//$eval->date_appel,
        'date_eval'=> Carbon::createFromFormat('Y-m-d H:i:s', $finale_calib->created_at)->format('d/m/Y'),
    ];
    foreach($evals as $eval) {
        
        $data[] =[
            'id'=>$eval->id,
            'note'=>$eval->note_global,
            'agent'=>$eval->agent->nom.' '.$eval->agent->prenom,
            'qualiticien'=>$eval->qualiticien->nom.' '.$eval->qualiticien->prenom,
            'service'=>$eval->grille->service->nom,
            'client_id'=>$eval->client_id,
            'date_appel'=>Carbon::createFromFormat('Y-m-d', $eval->date_appel)->format('d/m/Y'),//$eval->date_appel,
            'date_eval'=> Carbon::createFromFormat('Y-m-d H:i:s', $eval->created_at)->format('d/m/Y'),
        ];
  
        

    }
    foreach($evals as $eval){
        $elements = self::create($eval->grille->service->id);
        foreach ($elements as &$item){
            foreach ($item['elements'] as &$element){
              $note = NoteCalibrage::select('note','comment','calibrages_id','element_id')->where(['calibrages_id'=>$eval->id,'element_id'=>$element['id']])->first();
            if($note !== null){
                $data2[$eval->id][] = [
                    'element_id'=>$note->element_id,
                     'note'         =>$note->note,
                     'comment'      =>$note->comment,
                     'calibrages_id'=>$note->calibrages_id,
                     'qualiticien'=> $eval->qualiticien->nom.' '.$eval->qualiticien->prenom,
                    ];
            }
            
         
            }
        }
        $data3['elements'] = $elements;
    }


    $orientation = 'landscape';
$customPaper =array(0,0,950,950);

    $pdf = PDF::loadView('calibrage.pdf',["calibrages"=>$data,
    'notes'=>$data2,
    'elements'=>$data3,
    'finale_calib'=>$finale_calib_array
    ] );

    return $pdf->download('calibrage.pdf');
}
public function getMycalib(Request $req){

    $startDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[0])->format('Y-m-d');
    if(isset($req->input('dateRange')[1])) {
        $endDate = DateTime::createFromFormat('d/m/Y', $req->input('dateRange')[1])->format('Y-m-d');
    } else {
        $endDate = $startDate;
    }
    $user = auth('api')->user();

    // fetch by 'date_appel' if switchDate is true else fetch by created_at (=date_eval)
    if($req->switchDate == "true"){
        $date_param = 'date_appel';
    } else {
        $date_param = 'created_at';
    }

    $evals = Calibrage::where('qualiticien_id', $user->id)->whereIn('site', $req->input('selectedSites'))->whereBetween($date_param, [ $startDate, $endDate])->get();

    $data = array();
    foreach ($evals as $eval){
        $data[] = [
            'id'=>$eval->id,
            'note'=>$eval->note_global,
            'agent'=>$eval->agent->nom.' '.$eval->agent->prenom,
            'qualiticien'=>$eval->qualiticien->nom.' '.$eval->qualiticien->prenom,
            'service'=>$eval->grille->service->nom,
            'activty'=>$eval->grille->service->activite->nom,
            'client_id'=>$eval->client_id,
            'date_appel'=>Carbon::createFromFormat('Y-m-d', $eval->date_appel)->format('d/m/Y'),//$eval->date_appel,
            'date_eval'=> Carbon::createFromFormat('Y-m-d H:i:s', $eval->created_at)->format('d/m/Y'),
        ];
    }
    return $data;
}
  
public function create($id){

    $service = Service::find($id); 

    $grille = $service->grille;

    $elpartmap = array();
    foreach($grille->elements as $element){
        $partie = $element->partie->nom;
        $el = ['id'=>$element->pivot->id,'nom'=>$element->nom,'g_ok'=>$element->pivot->ok,'g_pa'=>$element->pivot->pa,
        'g_ko'=>$element->pivot->ko,'g_si'=>$element->pivot->si,'g_na'=>$element->pivot->na,'bareme'=>$element->pivot->bareme];
        $testkey = null;

        foreach ($elpartmap as $key => $val) {
            if ($val['partie'] === $partie) {
                $testkey = $key;
                break;
            }
        }
        if(isset($testkey)){
            $elpartmap[$testkey]['elements'][] = $el;
        }else{
            $elpartmap[]=['partie'=>$partie,'elements'=>[$el]];
        }
    }

    return  $elpartmap;
}  
public function getcalib($id) {
    $eval = Calibrage::find($id);
    $elements = self::create($eval->grille->service->id);
$calibrages = Calibrage::where('agent_id',$eval->agent_id)->where('client_id',$eval->client_id)->get();
foreach($calibrages as $calib) {
    $qualiticiens[]=$calib->qualiticien;
    foreach ($elements as &$item){
        foreach ($item['elements'] as &$element){
            if(NoteCalibrage::where(['calibrages_id'=>$calib->id,'element_id'=>$element['id']])->first() !==null){
                $notes[] = NoteCalibrage::where(['calibrages_id'=>$calib->id,'element_id'=>$element['id']])->first();
            }
    
        }}
}


    foreach($calibrages as $calib) {
      
        $comments[]=[
            'satcli'=>$calib->satcli,
            'oad'=>$calib->oad,
            'pcc'=>$calib->pcc,
            'exci'=>$calib->exci,
            'synthese'=>$calib->synthese,
            'semaine'=>$calib->semaine,
            'qualticiens'=>$calib->qualiticien,
        ];
    }
    $data = array();
    if(Appel::find($eval->appel_id)){
        $appel =  Appel::find($eval->appel_id) ;
        $data['appel'] =asset(\Storage::url($appel->url)) ;
    }
    else {
        $data['appel'] =0 ;
    }
    $data['eval'] = $eval;
    $data['elements'] = $elements;
    $data['qualiticiens'] = $qualiticiens;
    $data['notes'] = $notes;
    $data['calibrages'] = $calibrages;
    $data['comments']=$comments;

   return $data;
}


public function deleteCalib(Request $request){
    NoteCalibrage::where('calibrages_id',$request->input('id'))->delete();
    Calibrage::find($request->input('id'))->delete();
}
public function consolidation(Request $req){
    
   $activity = Activite::find($req->activityId);
   $service = Service::find($req->service);
   
    $evals = Calibrage::where('date_appel', $req->date)->where('grille_id',$service->grille->id)->get();
    $data = [];
    foreach ($evals as $eval){
        $data[] = [
            'id'=>$eval->id,
            'note'=>$eval->note_global,
            'agent'=>$eval->agent->nom.' '.$eval->agent->prenom,
            'qualiticien'=>$eval->qualiticien->nom.' '.$eval->qualiticien->prenom,
            'service'=>$eval->grille->service->nom,
            'activty'=>$eval->grille->service->activite->nom,
            'client_id'=>$eval->client_id,
            'date_appel'=>Carbon::createFromFormat('Y-m-d', $eval->date_appel)->format('d/m/Y'),//$eval->date_appel,
            'date_eval'=> Carbon::createFromFormat('Y-m-d H:i:s', $eval->created_at)->format('d/m/Y'),
        ];
    }
    return $data;
}


public function consultEval($id){
    
    $eval = Calibrage::find($id);
    $elements = self::create($eval->grille->service->id);


    foreach ($elements as &$item){
        foreach ($item['elements'] as &$element){
          if(  $note = NoteCalibrage::select('note','comment')->where(['calibrages_id'=>$id,'element_id'=>$element['id']])->first()) {
     
            $element['note'] = $note->note;
            $element['comment'] = $note->comment;
          }
          else {
           
            $note = NoteCalibrage::select('note','comment')->where(['calibrages_id'=>$id])->firstOrFail();
        
        

            $element['note'] = $note->note;
            $element['comment'] = $note->comment;
          }

          
        }
    }
    $data = array();
    $data['eval'] = $eval;
    $data['elements'] = $elements;

   return $data;
}
public function editItem(Request $request){
   
    $note = NoteCalibrage::Where(['element_id'=>$request->input('data')['id'],'calibrages_id'=>$request->input('evalid')])->first();
    $eval = Calibrage::find($request->input('evalid'));
   
    $note->comment = $request->input('data')['comment'];
     if($note->note !== $request->input('data')['note']){
          if($request->input('data')['note'] =='AQ' && $note->note == 'PA' ){
            $eval->note_global = $eval->note_global+ $request->input('data')['bareme']/2;
            
          }
          if($request->input('data')['note'] =='NA' && $note->note == 'PA' ){
            $eval->note_global = $eval->note_global+ $request->input('data')['bareme']/2;
          
            }
          if($request->input('data')['note'] =='NAQ' && $note->note == 'PA' ){
            $eval->note_global = $eval->note_global - $request->input('data')['bareme']/2;
            
            }
          if($request->input('data')['note'] =='SI' && $note->note == 'PA' ){
            $eval->note_global = $eval->note_global - $request->input('data')['bareme']/2;
          
            }
          if($request->input('data')['note'] =='PA' && $note->note == 'AQ' ){
            $eval->note_global = $eval->note_global - $request->input('data')['bareme']/2;
           
            }

          if($request->input('data')['note'] =='NAQ' && $note->note == 'AQ' ){
            $eval->note_global = $eval->note_global - $request->input('data')['bareme'];
            
            } 
           if($request->input('data')['note'] =='SI' && $note->note == 'AQ' ){
            $eval->note_global = $eval->note_global - $request->input('data')['bareme'];
               
            }
           if($request->input('data')['note'] =='NA' && $note->note == 'AQ' ){
            $eval->note_global = $eval->note_global ;
                    
            }  

          if($request->input('data')['note'] =='AQ' && $note->note == 'NAQ' ){
            $eval->note_global = $eval->note_global + $request->input('data')['bareme'];
          
            }
          if($request->input('data')['note'] =='PA' && $note->note == 'NAQ' ){
            $eval->note_global = $eval->note_global + $request->input('data')['bareme']/2;
                
            }
          if($request->input('data')['note'] =='NA' && $note->note == 'NAQ' ){
            $eval->note_global = $eval->note_global + $request->input('data')['bareme'];
                    
            }
          if($request->input('data')['note'] =='SI' && $note->note == 'NAQ' ){
                $eval->note_global = $eval->note_global ;
                      
            }
         if($request->input('data')['note'] =='AQ' && $note->note == 'SI' ){
            $eval->note_global = $eval->note_global + $request->input('data')['bareme'];
            
            }
         if($request->input('data')['note'] =='PA' && $note->note == 'SI' ){
            $eval->note_global = $eval->note_global + $request->input('data')['bareme']/2;
              
            }
         if($request->input('data')['note'] =='NA' && $note->note == 'SI' ){
            $eval->note_global = $eval->note_global + $request->input('data')['bareme'];
                   
            }
         if($request->input('data')['note'] =='NAQ' && $note->note == 'SI' ){
            $eval->note_global = $eval->note_global ;
                       
            }
     }

  
     
      
    $note->note = $request->input('data')['note'];
       


    $eval->save();
    $note->save();
    return $eval->note_global;
}
public function deleteCalib2(Request $request){
    
    $calibrage= Calibrage::find($request->input('id'));
    $calibrages = Calibrage::where('agent_id',$calibrage->agent_id)->where('client_id',$calibrage->client_id)->get();
    foreach($calibrages as $calib){
        NoteCalibrage::where('calibrages_id',$calib->id)->delete();
        $calib->delete();

    }
   
   
}
}
