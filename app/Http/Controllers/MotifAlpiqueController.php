<?php

namespace App\Http\Controllers;

use App\MotifAlpique;
use Illuminate\Http\Request;

class MotifAlpiqueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $MotifAlpique = MotifAlpique::all();
        return $MotifAlpique;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MotifAlpique  $motifAlpique
     * @return \Illuminate\Http\Response
     */
    public function show(MotifAlpique $motifAlpique)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MotifAlpique  $motifAlpique
     * @return \Illuminate\Http\Response
     */
    public function edit(MotifAlpique $motifAlpique)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MotifAlpique  $motifAlpique
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MotifAlpique $motifAlpique)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MotifAlpique  $motifAlpique
     * @return \Illuminate\Http\Response
     */
    public function destroy(MotifAlpique $motifAlpique)
    {
        //
    }
}
