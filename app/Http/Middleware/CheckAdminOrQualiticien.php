<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAdminOrQualiticien
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guard('qualiticien-api')->check() || Auth::guard('admin-api')->check()) return $next($request);

        return response()->json([
            'message' => 'Unauthorized'
        ], 401);


    }
}
