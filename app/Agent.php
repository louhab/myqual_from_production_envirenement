<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Agent extends Authenticatable
{
    use HasApiTokens, Notifiable;
    protected $attributes = [
        'statut' => "1",
    ];

    protected $fillable = [
        'nom', 'prenom', 'telephone', 'matricule', 'email','service_id', 'password', 'statut', 'dateInte', 'site'
    ];

    protected $hidden = [
        'password',
    ];

    public function evaluations()
    {
        return $this->hasMany('App\Evaluation');
    }

    public function calibrages()
    {
        return $this->hasMany('App\Calibrage');
    }
    public function escdas()
    {
        return $this->hasMany('App\Escda');
    }
    public function appels()
    {
        return $this->hasMany('App\Appel');
    }
    public function constats()
    {
        return $this->hasMany('App\Constat');
    }
    public function eluescda()
    {
        return $this->hasMany('App\EluEscda.php');
    }
    public function historiques()
    {
        return $this->hasMany('App\HistoriqueAgent');
    }
    public function service()
    {
        return $this->belongsTo('App\Service','service_id');
    }
}
