<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MessageGoogle extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public  $data  = array();
 

    public function __construct($data)
    {  
        

           $this->data  = $data ;
       
     
      
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    // public function build()
    // {
    //     return $this->view('view.name');
    // }
    public function build()
    {

    //    if($this->data->niveauDimportance != 'Normal'){
    //     return $this->from("pole.dev@myopla.com") // L'expéditeur
    //     ->subject("Nouveau Constat Reçu  : Urgent  ") // Le sujet
    //     ->view('emails.message-google'); // La vue
    //    }
    //    else {
        return $this->from("pole.dev@myopla.com") // L'expéditeur
        ->subject("Nouveau Constat Reçu  :   ") // Le sujet
        ->view('emails.message-google'); // La vue
     //  }
      
    }
}
