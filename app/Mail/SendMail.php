<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;
    public $subject;
    public $content;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject,$content)
    {
        $this->subject =$subject;
        $this->content = $content;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from("pole.dev@myopla.com") // L'expéditeur
        ->subject($this->subject) // Le sujet
        ->view('emails.message-google-messanger')->with([
            'title'     => $this->subject, //this works without queue
            'content'     => $this->content, //this works without queue
        ]);;
    }
}
