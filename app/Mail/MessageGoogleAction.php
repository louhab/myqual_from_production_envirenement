<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MessageGoogleAction extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public  $data  = array();

    public function __construct($data)
    {
        $this->data  = $data ;
       
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from("pole.dev@myopla.com") // L'expéditeur
                    ->subject("Nouveau Constat action a été prise : ") // Le sujet
                    ->view('emails.message-google-action');
    }
}
