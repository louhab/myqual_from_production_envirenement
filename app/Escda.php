<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Escda extends Model
{
    

    public function grille()
    {
        return $this->belongsTo('App\Grille');
    }

    public function notes()
    {
        return $this->hasMany('App\NoteEscda');
    }

    public function agent()
    {
        return $this->belongsTo('App\Agent');
    }
    public function qualiticien()
    {
        return $this->belongsTo('App\User');
    }
}
