<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{   
    public $timestamps = false; 
    public function elementgrille()
    {
        return $this->belongsTo('App\ElementGrille');
    }
    public function evaluation()
    {
        return $this->belongsTo('App\Evaluation');
    }
    public function escda()
    {
        return $this->belongsTo('App\Escda');
    }
    
  
}
