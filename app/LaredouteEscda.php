<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LaredouteEscda extends Model
{
    
    public function grille()
    {
        return $this->belongsTo('App\Grille');
    }

    public function notes()
    {
        return $this->hasMany('App\NoteLaredouteEscda');
    }

    public function agent()
    {
        return $this->belongsTo('App\Agent');
    }
    public function qualiticien()
    {
        return $this->belongsTo('App\User');
    }
}
