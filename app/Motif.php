<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Motif extends Model
{
    public function sousmotifs()
    {
        return $this->hasMany('App\SousMotif');
    }
}
