<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected  $guard_name = 'api';
    protected $fillable = [
        'nom', 'prenom', 'telephone', 'matricule', 'email', 'password', 'statut',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    protected $attributes = [
        'statut' => "1",
    ];

    public function evaluations()
    {
        return $this->hasMany('App\Evaluation', 'qualiticien_id');
    }
    public function eluescda()
    {
        return $this->hasMany('App\EluEscda.php', 'qualiticien_id');
    }
    public function uservisites()
    {
        return $this->hasMany('App\UserVisite', 'user_id');
    }

    public function activite()
    {
        return $this->belongsToMany('App\Activite','activite_user','user_id','activite_id');
    }
    public function agents()
    {
        return $this->hasMany('App\Task_type');
    }
    public function constats()
    {
        return $this->hasMany('App\Constat');
    }
}
