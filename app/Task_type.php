<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task_type extends Model
{
    public function task()
    {
        return $this->belongsTo('App\Task');
    }
}
