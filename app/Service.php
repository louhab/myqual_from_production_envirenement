<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{

    protected $guarded = [];
    protected $attributes = [
        'statut' => "1",
    ];
    public function grille()
    {
        return $this->hasOne('App\Grille');
    }
    public function activite()
    {
        return $this->belongsTo('App\Activite');
    }
    public function agents()
    {
        return $this->hasMany('App\Agent');
    }
    public function constats()
    {
        return $this->hasMany('App\Constat');
    }

}
