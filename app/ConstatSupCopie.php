<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConstatSupCopie extends Model
{
    protected $guarded = [];
    public function constats()
    {
        return $this->hasMany('App\Constat');
    }
}
