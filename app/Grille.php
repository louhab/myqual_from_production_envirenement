<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grille extends Model
{



    public function evaluations()
    {
        return $this->hasMany('App\Evaluation');
    }
    public function calibrages()
    {
        return $this->hasMany('App\Calibrage');
    }
    public function escdas()
    {
        return $this->hasMany('App\Escda');
    }
    public function eluescdas()
    {
        return $this->hasMany('App\EluEscda.php');
    }

    public function service()
    {
        return $this->belongsTo('App\Service');
    }

    public function elements()
    {
        return $this->belongsToMany('App\ElementGrille','grille_element_grille','grille_id','element_grille_id')->withPivot('bareme','na','pa','si','ok','ko','id');;
    }
}
