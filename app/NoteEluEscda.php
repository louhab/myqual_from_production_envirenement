<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoteEluEscda extends Model
{
    public function eluescda()
    {
        return $this->belongsTo('App\EluEscda.php');
    }
}
