<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activite extends Model
{
    protected $attributes = [
        'statut' => "1",
    ];
    protected $guarded = [];
    public function services()
    {
        return $this->hasMany('App\Service')->where('statut','!=',0);
    }
    public function constats()
    {
        return $this->hasMany('App\Constat');
    }

    public function user()
    {
        return $this->belongsToMany('App\User','activite_user','activite_id','user_id');
    }
}
