<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoteLaredouteEscda extends Model
{
    public function evaluation()
    {
        return $this->belongsTo('App\LaredouteEscda');
    }
}
