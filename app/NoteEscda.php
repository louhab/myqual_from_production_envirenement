<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoteEscda extends Model
{
    public function evaluation()
    {
        return $this->belongsTo('App\Escda');
    }
}
