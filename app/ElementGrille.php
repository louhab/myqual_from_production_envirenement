<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ElementGrille extends Model
{
    protected $guarded = [];
    /**
     * Get the note for the element.
     **/
    public function notes()
    {
        return $this->hasMany('App\Note');
    }



    public function partie()
    {
        return $this->belongsTo('App\PartieGrille','partie_grille_id');
    }

    public function grilles()
    {
      //  return $this->belongsToMany('App\Grille');
        ////
        return $this->belongsToMany('App\Grille','grille_elementgrille','element_grille_id','grille_id')->withPivot('bareme','pa','na','si','ok','ko','id');

    }
}
