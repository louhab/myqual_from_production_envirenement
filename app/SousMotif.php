<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SousMotif extends Model
{
    public function motif()
    {
        return $this->belongsTo('App\Motif');
    }
}
