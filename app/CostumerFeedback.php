<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostumerFeedback extends Model
{
    public function activite()
    {
        return $this->belongsTo('App\Activite');
    }
    public function service()
    {
        return $this->belongsTo('App\Service');
    }
    public function agent()
    {
        return $this->belongsTo('App\Agent');
    }
    public function user(){
        return $this->belongsTo('App\User','qualiticien_id');
    }
}
