export default [
  {
    title: 'Accueil',
    route: 'home',
    icon: 'HomeIcon',
    resource: 'homeadmin',
  },
  {
    title: 'Accueil ',
    route: 'espace-qualiticien',
    icon: 'HomeIcon',
    resource: 'homequaliticien',
  },
  {
    title: 'Accueil   ',
    route: 'espace-sup',
    icon: 'HomeIcon',
    resource: 'homesup',
  },
  {
    title: 'Accueil  ',
    route: 'espace-client',
    icon: 'HomeIcon',
    resource: 'homeclient',
  },
  {
    title: 'Accueil.',
    route: 'espace-agent',
    icon: 'HomeIcon',
    resource: 'homeagent',
  },
  {
    title: 'Mes évaluations.',
    route: 'myeval-agent',
    icon: 'SlidersIcon',
    resource: 'myeval-agent',
  },
  {
    title: 'Reporting',
    icon: 'BarChart2Icon',
    children: [
      {
        title: 'Global',
        route: 'reporting-global',
        resource: 'reporting-g',
      },
      {
        title: 'Par Activité',
        route: 'reporting-activity',
        resource: 'reporting-a',
      },
      {
        title: 'Par Service',
        route: 'reporting-service',
        resource: 'reporting-s',
      },
      {
        title: 'Par Téléconseiller',
        route: 'reporting-agent',
        resource: 'reporting-t',
      },
      {
        title: 'Par Evaluateur',
        route: 'reporting-evaluateur',
        resource: 'reporting-a',
      },
      {
        title: 'Par Bloc',
        route: 'reporting-item',
        resource: 'reporting-item',
      },
      {
        title: 'Par Item',
        route: 'item-reporting',
        resource: 'reporting-item',
      }
    ],
  },
  {
    title: 'Calibrages ',
    icon: 'BarChart2Icon',
    children: [
      {
        title: 'Ajouter un enregsitrement',
        route: 'add-apel',
        resource: 'add-apel',
      },
      {
        title: 'Ajouter un calibrage',
        route: 'add-calibrag',
        resource: 'detail-apel',
      },
      {
        title: 'Liste des enregsitrements',
        route: 'list-apel',
        resource: 'appel-list',
      },

     
      {
        title: 'Mes calibrages',
        route: 'my-calib',
        resource: 'my-calib',
      },
      {
        title: 'Historique de calibrage',
        route: 'calibrage-liste2',
        resource: 'calibrage-liste',
      },
    ]
  },
  {
    title: 'Reporting des constats',
    icon: 'BarChart2Icon',
    children: [
      {
        title: 'Global',
        route: 'reporting-constat-global',
        resource: 'reporting-c-g',
      },
      {
        title: 'Par Activité',
        route: 'reporting-constat-activity',
        resource: 'reporting-c-a',
      },
      {
        title: 'Par Service',
        route: 'reporting-constat-service',
        resource: 'reporting-c-s',
      },

      {
        title: 'Par Téléconseiller',
        route: 'reporting-constat-agent',
        resource: 'reporting-c-t',
      },
    ],
  },
  {
    title: 'Reporting des calibrage',
    icon: 'BarChart2Icon',
    children: [
      {
        title: 'Par Evaluateur',
        route: 'reporting-calib-evaluateur',
        resource: 'reporting-calib',
      },
    ],
  },
  {
    title: 'Utilisateurs',
    icon: 'UsersIcon',
    children: [
      {
        title: 'Ajouter',
        route: 'ajouter-utilisateur',
        resource: 'users',
      },
      {
        title: 'Liste ',
        children: [
          {
            title: 'des admins',
            route: 'liste-admins',
            resource: 'users',
          },
          {
            title: 'des qualiticiens',
            route: 'liste-qualiticiens',
            resource: 'users',
          },
          {
            title: 'des superviseurs',
            route: 'liste-sup',
            resource: 'users',
          },
          {
            title: 'des agents',
            route: 'liste-agents',
            resource: 'users',
          },
          {
            title: 'des clients',
            route: 'liste-clients',
            resource: 'users',
          },
        ],
      },
    ],
  },
  {
    title: 'Grilles',
    icon: 'FileTextIcon',
    children: [
      {
        title: 'Créer une grille',
        route: 'creer-grille',
        resource: 'grilles',
      },
      {
        title: 'Ajouter des éléments',
        route: 'elements-grille',
        resource: 'grilles',
      },
      {
        title: 'Liste de grilles',
        route: 'grille-listes',
        resource: 'grilles',
      },
    ],
  },
  {
    title: 'Activités',
    route: 'Activity-list',
    icon: 'BriefcaseIcon',
    resource: 'activity',
  },
  {
    title: 'Évaluation',
    icon: 'SlidersIcon',
    children: [
      {
        title: 'Évaluer',
        route: 'evaluation',
        resource: 'eval',
      },
      {
        title: 'Mes évaluations',
        route: 'my-evals',
        resource: 'myevals',
      },
      {
        title: 'Liste des évaluations',
        route: 'liste-evaluation',
        resource: 'liste-eval',
      },
    ],
  },
  // {
  //   title: 'Les taches hebdomadaire',
  //   icon: 'PaperclipIcon',
  //   children: [
  //     {
  //       title: 'Tache',
  //       route: 'add-tache',
  //       resource: 'add-tache',
  //     },
  //     {
  //       title: 'Mes taches',
  //       route: 'mes-taches',
        
  //       resource: 'mytask',
  //     },
  //     {
  //       title: 'Liste des taches',
  //       route: 'liste-taches',
        
  //       resource: 'liste-tasks',
  //     },
  //     {
  //       title: 'Rapport des taches',
  //       route: 'rapport-tache',
        
  //       resource: 'rapport-tache',
  //     },
  //   ],
  // },
  // {
  //   title: 'Calibrage',
  //   icon: 'CompassIcon',
  //   children: [
  //     {
  //       title: 'Calibrage',
  //       route: 'calibrage-evals',
  //       resource: 'calibrage-evals',
  //     },
  //     {
  //       title: 'Mes calibrages',
  //       route: 'my-calib',
  //       resource: 'my-calib',
  //     },
  //     {
  //       title: 'Historique de calibrage',
  //       route: 'calibrage-liste',
  //       resource: 'calibrage-liste',
  //     },
  //     {
  //       title: 'Consolidation',
  //       route: 'consolidation',
  //       resource: 'consolidation',
  //     },
  //   ],
  // },
  {
    title: 'Documentation',
    icon: 'BookIcon',
    children: [
      {
        title: 'Liste des Docs',
        route: 'documents',
        resource: 'doc-list',
      },
      {
        title: 'Ajouter un Doc',
        route: 'add-doc',
        resource: 'doc-add',
      },
    ],
  },
  {
    title: 'Escda',
    icon: 'AwardIcon',
    children: [
      {
        title: "L'ÉLECTION DU SERVICE CLIENT DE L'ANNÉE",
        resource: 'escda',
      },
      {
        title: 'Escda cds ',
        icon: 'AwardIcon',
        children: [
          {
            title: 'Evaluer',
            route: 'escda',
            resource: 'escda',
          },
          {
            title: 'Mes evals ',
            route: 'myevals-escda',
            resource: 'my-evals-escda',
          },
          {
            title: 'liste des evals',
            route: 'liste-escda',
             resource: 'evals-escda',
          },
        ],
      },
      {
        title: 'ESCDA CYCLOCITY',
        icon: 'AwardIcon',
        children: [
          {
            title: 'Evaluer',
            route: 'elu-escda',
               resource: 'elu-escda',
          },
          {
            title: 'Mes evals ',
            route: 'myevals-elu-escda',
           resource: 'my-evals-elu-escda',
          },
          {
            title: 'liste des evals',
            route: 'liste-elu-escda',
              resource: 'evals-elu-escda',
          },
        ],
      },
      {
        title: 'Escda La redoute',
        icon: 'AwardIcon',
        children: [
          {
            title: 'Evaluer',
            route: 'escda-la-redoute',
            resource: 'escda',
          },
          {
            title: 'Mes evals ',
            route: 'myevals-escda-la-redoute',
            resource: 'my-evals-escda',
          },
          {
            title: 'liste des evals',
            route: 'liste-escda-la-redoute',
             resource: 'evals-escda',
          },
        ],
      },
    ],
  },
  // {
  //   title: 'Escda CDS',
  //   icon: 'AwardIcon',
  //   children: [
  //     {
  //       title: 'Escda',
  //       route: 'escda',
  //       resource: 'escda',
  //     },
  //     {
  //       title: ' Mes evaluations Escda',
  //       route: 'myevals-escda',
  //       resource: 'my-evals-escda',
  //     },
  //     {
  //       title: 'Liste Evaluations Escda',
  //       route: 'liste-escda',
  //       resource: 'evals-escda',
  //     },
  //   ],
  // },
  // {
  //   title: 'Escda La redoute',
  //   icon: 'AwardIcon',
  //   children: [
  //     {
  //       title: 'Escda la redoute',
  //       route: 'escda-la-redoute',
  //       resource: 'escda',
  //     },
  //     {
  //       title: ' Mes evaluations Escda la redoute',
  //       route: 'myevals-escda-la-redoute',
  //       resource: 'my-evals-escda',
  //     },
  //     {
  //       title: 'Liste Evaluations Escda la redoute ',
  //       route: 'liste-escda-la-redoute',
  //       resource: 'evals-escda',
  //     },
  //   ],
  // },
  // {
  //   title: 'ESCDA CYCLOCITY',
  //   icon: 'LayersIcon',
  //   children: [
  //     {
  //       title: 'Elu Escda',
  //       route: 'elu-escda',
  //       resource: 'elu-escda',
  //     },
  //     {
  //       title: ' Mes evaluations  Elu Escda',
  //       route: 'myevals-elu-escda',
  //       resource: 'my-evals-elu-escda',
  //     },
  //     {
  //       title: 'Liste Evaluations Elu  Escda',
  //       route: 'liste-elu-escda',
  //       resource: 'evals-elu-escda',
  //     },
  //   ],
  // },
  {
    title: 'Constats et Actions',
    icon: 'PackageIcon',
    children: [
      {
        title: 'Ajouter un constat',
        route: 'constat-add',
        resource: 'constat-add',
      },

   
      {
        title: 'Mes constats',
        route: 'my-consta',
        resource: 'my-consta',
      },
      {
        title: 'Constats en copie',
        route: 'my-consta-copie',
        resource: 'constat-copie',
      },
      {
        title: 'Liste des  constats',
        route: 'constat-liste',
        resource: 'constat-liste',
      },
      {
        title: 'Constats reçus',
        route: 'liste-constat-action',
        resource: 'liste-constat-action',
      },
      {
        title: 'Remontées Client DO',
        route: 'constat-remonte-client',
        resource: 'costumer-feedback',
      },
      {
        title: 'Global remontées ',
        route: 'my-costumer-feedbacks',
        resource: 'costumer-feedback',
      },
    ],
  },
  {
    title: 'Modules',
    route: 'modules',
    icon: 'BookIcon',
    resource: 'modules',
  },
  {
    title: 'Quizzes',
    route: 'quizzes',
    icon: 'CheckSquareIcon',
    resource: 'quizzes',
  },
  {
    title: 'Test Page',
    route: 'test',
    icon: 'EyeOffIcon',
    resource: 'basic',
  },
  {
    title: 'Test API',
    route: 'second-page',
    icon: 'DollarSignIcon',
    resource: 'basic',
  },

]
