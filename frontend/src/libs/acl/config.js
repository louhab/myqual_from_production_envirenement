export const initialAbility = [
  {
    action: 'read',
    subject: 'Auth',
  },
]

export const adminAbility = [
  {
    action: 'manage',
    subject: 'activity',
  },
  {
    action: 'manage',
    subject: 'homeadmin',
  },
  {
    action: 'manage',
    subject: 'eval',
  },
  {
    action: 'manage',
    subject: 'liste-eval',
  },
  {
    action: 'manage',
    subject: 'eval-edit',
  },
  {
    action: 'manage',
    subject: 'reporting',
  },
  {
    action: 'manage',
    subject: 'users',
  },
  {
    action: 'manage',
    subject: 'grilles',
  },
  {
    action: 'read',
    subject: 'Auth',
  },
]

export const qualiticienAbility = [
  {
    action: 'manage',
    subject: 'eval',
  },
  {
    action: 'manage',
    subject: 'eval-edit',
  },
  {
    action: 'manage',
    subject: 'myevals',
  },
  {
    action: 'manage',
    subject: 'reporting',
  },
  {
    action: 'manage',
    subject: 'homequaliticien',
  },
  {
    action: 'read',
    subject: 'Auth',
  },
]

export const agentAbility = [
  {
    action: 'manage',
    subject: 'homeagent',
  },
  {
    action: 'manage',
    subject: 'quizzes',
  },
  {
    action: 'manage',
    subject: 'modules',
  },
  {
    action: 'read',
    subject: 'Auth',
  },
]

export const clientAbility = [
  {
    action: 'manage',
    subject: 'eval',
  },
  {
    action: 'read',
    subject: 'Auth',
  },
]

export const _ = undefined
