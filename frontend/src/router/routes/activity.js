export default [
  {
    path: '/activity/list',
    name: 'Activity-list',
    component: () => import('@/views/activity/activities-list.vue'),
    meta: {
      resource: 'activity',
      pageTitle: 'Les activités',
      breadcrumb: [
        {
          text: 'Les activités',
          active: true,
        },
      ],
    },
  },
  {
    path: '/activity/:id',
    name: 'Activity-details',
    component: () => import('@/views/activity/activities-details.vue'),
    meta: {
      resource: 'activity',
      pageTitle: '',
      breadcrumb: [
        {
          text: 'Les activités',
          to: '/activity/list',
        },
        {
          text: 'Details d\'activité',
          active: true,
        },
      ],
    },

  },
  {
    path: '/service/:id',
    name: 'service-details',
    component: () => import('@/views/activity/service/service-details.vue'),
    meta: {
      resource: 'activity',
      pageTitle: '',
      breadcrumb: [
        {
          text: 'Les activités',
          to: '/activity/list',
        },
        {
          text: 'Details d\'activité',
          to: '/activity/list',
          // href: $router.back(),
        },
        {
          text: 'Details du service',
          active: true,
        },
      ],
    },
  },
]
