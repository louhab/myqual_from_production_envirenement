export default [
    {
      path: '/calibrage-evals',
      name: 'calibrage-evals',
      component: () => import('@/views/calibrage/CalibEvals.vue'),
      meta: {
        resource: 'calibrage-evals',
        breadcrumb: [
          {
            text: 'Calibrage',
            active: true,
          },
        ],
      },
    },

      {
        path: '/my-calib',
        name: 'my-calib',
        component: () => import('@/views/calibrage/MyCalib.vue'),
        meta: {
          resource: 'my-calib',
          breadcrumb: [
            {
              text: 'Liste de  mes calibrage',
              active: true,
            },
          ],
        },
      },

      {
        path: '/consolidation',
        name: 'consolidation',
        component: () => import('@/views/calibrage/Consolidation.vue'),
        meta: {
          resource: 'consolidation',
          breadcrumb: [
            {
              text: 'Detail calibrage',
              active: true,
            },
          ],
        },
      },

  ]
