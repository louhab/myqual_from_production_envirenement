
export default [
    {
      path: '/reporting/constat/global',
      name: 'reporting-constat-global',
      component: () => import('@/views/reportingConstat/Global.vue'),
      meta: {
        resource: 'reporting-c-g',
        pageTitle: 'Reporting Global',
        breadcrumb: [
          {
            text: 'Reporting',
            active: true,
          },
        ],
      },
    },
    {
      path: '/reporting/constat/activity',
      name: 'reporting-constat-activity',
      component: () => import('@/views/reportingConstat/Activity.vue'),
      meta: {
        resource: 'reporting-c-a',
        pageTitle: '',
        breadcrumb: [
          {
            text: 'Reporting',
            active: true,
          },
          {
            text: 'Par Activité',
            active: true,
          },
        ],
      },
    },
    {
      path: '/reporting/constat/service',
      name: 'reporting-constat-service',
      component: () => import('@/views/reportingConstat/Service.vue'),
      meta: {
        resource: 'reporting-c-s',
        pageTitle: '',
        breadcrumb: [
          {
            text: 'Reporting',
            active: true,
          },
          {
            text: 'Par Service',
            active: true,
          },
        ],
      },
    },
    {
      path: '/reporting/constat/agent',
      name: 'reporting-constat-agent',
      component: () => import('@/views/reportingConstat/Teleconseiller.vue'),
      meta: {
        resource: 'reporting-c-t',
        pageTitle: '',
        breadcrumb: [
          {
            text: 'Reporting',
            active: true,
          },
          {
            text: 'Par Téléconseiller',
            active: true,
          },
        ],
      },
    },
  ]
  