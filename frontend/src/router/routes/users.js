export default [
  {
    path: '/ajouter-utilisateur',
    name: 'ajouter-utilisateur',
    component: () => import('@/views/users/AddUser.vue'),
    meta: {
      resource: 'users',
      breadcrumb: [
        {
          text: 'Utilisateurs',
          active: true,
        },
        {
          text: 'Ajouter un utilisateur',
          active: true,
        },
      ],
    },
  },
  {
    path: '/liste-agents',
    name: 'liste-agents',
    component: () => import('@/views/users/AgentsListe.vue'),
    meta: {
      resource: 'users',
      breadcrumb: [
        {
          text: 'Utilisateurs',
          active: true,
        },
        {
          text: 'Liste des agents',
          active: true,
        },
      ],
    },
  },
  {
    path: '/modifier-agent/:id',
    name: 'modifier-agent',
    component: () => import('@/views/users/AgentEdit.vue'),
    meta: {
      resource: 'users',
      breadcrumb: [
        {
          text: 'Utilisateurs',
          active: true,
        },
        {
          text: 'Modifier un agent',
          active: true,
        },
      ],
    },
  },
  {
    path: '/liste-admins',
    name: 'liste-admins',
    component: () => import('@/views/users/AdminsListe.vue'),
    meta: {
      resource: 'users',
      breadcrumb: [
        {
          text: 'Utilisateurs',
          active: true,
        },
        {
          text: 'Liste des admins',
          active: true,
        },
      ],
    },
  },
  {
    path: '/modifier-utilisateur/:id',
    name: 'edit-user',
    component: () => import('@/views/users/EditUser.vue'),
    meta: {
      resource: 'users',
      breadcrumb: [
        {
          text: 'Utilisateurs',
          active: true,
        },
        {
          text: 'Modifier un utilisateur',
          active: true,
        },
      ],
    },
  },
  {
    path: '/liste-qualiticiens',
    name: 'liste-qualiticiens',
    component: () => import('@/views/users/QualiticiensListe.vue'),
    meta: {
      resource: 'users',
      breadcrumb: [
        {
          text: 'Utilisateurs',
          active: true,
        },
        {
          text: 'Liste des qualiticiens',
          active: true,
        },
      ],
    },
  },
  {
    path: '/liste-clients',
    name: 'liste-clients',
    component: () => import('@/views/users/ClientList.vue'),
    meta: {
      resource: 'users',
      breadcrumb: [
        {
          text: 'Utilisateurs',
          active: true,
        },
        {
          text: 'Liste des clients',
          active: true,
        },
      ],
    },
  },
  {
    path: '/liste-superviseur',
    name: 'liste-sup',
    component: () => import('@/views/users/SupList.vue'),
    meta: {
      resource: 'users',
      breadcrumb: [
        {
          text: 'Utilisateurs',
          active: true,
        },
        {
          text: 'Liste des superviseurs',
          active: true,
        },
      ],
    },
  },
]
