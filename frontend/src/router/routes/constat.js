export default [
    {
      path: '/constat-add',
      name: 'constat-add',
      component: () => import('@/views/constat/AddConstat.vue'),
      meta: {
        resource: 'constat-add',
        breadcrumb: [
          {
            text: 'Ajouter un nouveau constat',
            active: true,
          },
        ],
      },
    },

    {
        path: '/calibrage-liste',
        name: 'calibrage-liste',
        component: () => import('@/views/calibrage/ListeCalib.vue'),
        meta: {
          resource: 'calibrage-liste',
          breadcrumb: [
            {
              text: 'Liste des calibrage',
              active: true,
            },
          ],
        },
      },
      {
        path: '/my-consta',
        name: 'my-consta',
        component: () => import('@/views/constat/MyConstat.vue'),
        meta: {
          resource: 'my-consta',
          breadcrumb: [
            {
              text: 'Liste de  mes constats',
              active: true,
            },
          ],
        },
      },
      {
        path: '/my-consta-copie',
        name: 'my-consta-copie',
        component: () => import('@/views/constat/ConstatCopie.vue'),
        meta: {
          resource: 'constat-copie',
          breadcrumb: [
            {
              text: 'Liste de  mes constats en copie',
              active: true,
            },
          ],
        },
      },
      {
        path: '/constat-liste',
        name: 'constat-liste',
        component: () => import('@/views/constat/ConstatListe.vue'),
        meta: {
          resource: 'constat-liste',
          breadcrumb: [
            {
              text: 'Lites des  constats',
              active: true,
            },
          ],
        },
      },
      {
        path: '/constat-details/:id',
        name: 'constat-detail',
        component: () => import('@/views/constat/ConstatDetails.vue'),
        meta: {
          resource: 'detailconstat',
          breadcrumb: [
            {
              text: 'Detail Constat',
              active: true,
            },
          ],
        },
      },
      {
        path: '/liste-constat-action',
        name: 'liste-constat-action',
        component: () => import('@/views/constat/ListeConstatAction.vue'),
        meta: {
          resource: 'liste-constat-action',
          breadcrumb: [
            {
              text: 'Liste des constats reçus',
              active: true,
            },
          ],
        },
      },
      {
        path: '/constat-remonte-client',
        name: 'constat-remonte-client',
        component: () => import('@/views/constat/ConstatRemonteClient.vue'),
        meta: {
          resource: 'costumer-feedback',
          breadcrumb: [
            {
              text: 'Remontées Client DO',
              active: true,
            },
          ],
        },
      },
      {
        path: '/costumer-feedbacks-details/:id',
        name: 'costumer-feedback-deatils',
        component: () => import('@/views/constat/CustomerFeedbackDetails.vue'),
        meta: {
          resource: 'costumer-feedback-deatils',
          breadcrumb: [
            {
              text: 'Details du constat remonté client ',
              active: true,
            },
          ],
        },
      },
          {
        path: '/my-costumer-feedbacks',
        name: 'my-costumer-feedbacks',
        component: () => import('@/views/constat/MyCostumerFeedbacks.vue'),
        meta: {
          resource: 'my-costumer-feedback',
          breadcrumb: [
            {
              text: 'Mes constats remonté client ',
              active: true,
            },
          ],
        },
      },
      

  ]
