export default [
  {
    path: '/modules',
    name: 'modules',
    component: () => import('@/views/SecondPage.vue'),
    meta: {
      resource: 'modules',
    },
  },
  {
    path: '/quizzes',
    name: 'quizzes',
    component: () => import('@/views/SecondPage.vue'),
    meta: {
      resource: 'quizzes',
    },
  },
]
