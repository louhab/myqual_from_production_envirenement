export default [
    {
      path: '/add-tache',
      name: 'add-tache',
      component: () => import('@/views/tasks/AddTask.vue'),
      meta: {
        resource: 'add-tache',
        breadcrumb: [
          {
            text: 'Ajouter une tache ',
            active: true,
          },
        ],
      },
    },


    {
        path: '/mes-taches',
        name: 'mes-taches',
        component: () => import('@/views/tasks/Mytasks.vue'),
        meta: {
          resource: 'mytask',
          breadcrumb: [
            {
              text: 'Mes taches effectués ',
              active: true,
            },
          ],
        },
      },
      {
        path: '/rapport-tache',
        name: 'rapport-tache',
        component: () => import('@/views/tasks/RapportTasks.vue'),
        meta: {
          resource: 'rapport-tache',
          breadcrumb: [
            {
              text: 'Rapport des  Tache',
              active: true,
            },
          ],
        },
      },


      {
        path: '/edit-taches/:id',
        name: 'edit-taches',
        component: () => import('@/views/tasks/EditTask.vue'),
        meta: {
          resource: 'edit-task',
          breadcrumb: [
            {
              text: 'Modifier la tache ',
              active: true,
            },
          ],
        },
      },
 
      {
        path: '/liste-taches',
        name: 'liste-taches',
        component: () => import('@/views/tasks/TasksListe.vue'),
        meta: {
          resource: 'liste-tasks',
          breadcrumb: [
            {
              text: 'Les taches effectués  ',
              active: true,
            },
          ],
        },
      },
  ]
