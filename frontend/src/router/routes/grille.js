export default [
  {
    path: '/grille/creer',
    name: 'creer-grille',
    component: () => import('@/views/grille/grille-form.vue'),
    meta: {
      resource: 'grilles',
      pageTitle: '',
      breadcrumb: [
        {
          text: 'Création d\'une grille',
          active: true,
        },
      ],
    },
  },
  {
    path: '/grille/elements',
    name: 'elements-grille',
    component: () => import('@/views/grille/elements.vue'),
    meta: {
      resource: 'grilles',
      pageTitle: '',
      breadcrumb: [
        {
          text: 'l\'ajout des items',
          active: true,
        },
      ],

    },
  },
  {
    path: '/grille/grilles-liste',
    name: 'grille-listes',
    component: () => import('@/views/grille/edit/GrilleList.vue'),
    meta: {
      resource: 'grilles',
      pageTitle: 'Liste de grilles',
      breadcrumb: [
        {
          text: 'liste de grilles',
          active: true,
        },
      ],
    },
  },
  {
    path: '/grille/modifier/:id',
    name: 'edit-grille',
    component: () => import('@/views/grille/edit/EditGrille.vue'),
    meta: {
      resource: 'grilles',
      breadcrumb: [
        {
          text: 'Liste de grilles',
          active: true,
        },
        {
          text: 'modifier une grille',
          active: true,
        },
      ],
    },
  },
]
