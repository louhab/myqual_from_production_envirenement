export default [
    {
      path: '/add-apel',
      name: 'add-apel',
      component: () => import('@/views/calibVersion2/AddApel.vue'),
      meta: {
        resource: 'add-apel',
        breadcrumb: [
          {
            text: 'Ajouter un enregistrement',
            active: true,
          },
        ],
      },
    },
    {
      path: '/list-apel',
      name: 'list-apel',
      component: () => import('@/views/calibVersion2/ListeAppel.vue'),
      meta: {
        resource: 'appel-list',
        breadcrumb: [
          {
            text: 'Liste des Appels',
            active: true,
          },
        ],
      },
    },
    {
      path: '/detail-appel/:id',
      name: 'detail-appel',
      component: () => import('@/views/calibVersion2/DetailAppel.vue'),
      meta: {
        resource: 'detail-apel',
        breadcrumb: [
          {
            text: 'Eregistrement',
            active: true,
          },
        ],
      },
    },
    {
      path: '/calibrage/:id',
      name: 'detail-calibrage2',
      component: () => import('@/views/calibVersion2/DetaileCalibrage2.vue'),
      meta: {
        resource: 'detail-calibrage',
        breadcrumb: [
          {
            text: 'Detail calibrage',
            active: true,
          },
        ],
      },
    },
    {
      path: '/detail-calibrage/:id',
      name: 'detail-calibrage222',
      component: () => import('@/views/calibVersion2/detailsCalibrage222.vue'),
      meta: {
        resource: 'detail-calibrage',
        breadcrumb: [
          {
            text: 'Detail calibrage',
            active: true,
          },
        ],
      },
    },

    {
      path: '/calibrage-liste2',
      name: 'calibrage-liste2',
      component: () => import('@/views/calibVersion2/ListeCalib2.vue'),
      meta: {
        resource: 'calibrage-liste',
        breadcrumb: [
          {
            text: 'Liste des calibrage',
            active: true,
          },
        ],
      },
    },
    {
      path: '/add-calibrage',
      name: 'add-calibrag',
      component: () => import('@/views/calibVersion2/Eval2.vue'),
      meta: {
        resource: 'detail-apel',
        breadcrumb: [
          {
            text: 'Ajouter une calibrage',
            active: true,
          },
        ],
      },
    },

  ]
