export default [
    {
      path: '/escda',
      name: 'escda',
      component: () => import('@/views/escda/Escda.vue'),
      meta: {
        resource: 'escda',
        breadcrumb: [
          {
            text: "L'ÉLECTION DU SERVICE CLIENT DE L'ANNÉE ",
            active: true,
          },
        ],
      },
    },

    {
      path: '/myevals-escda',
      name: 'myevals-escda',
      component: () => import('@/views/escda/MyEvals.vue'),
      meta: {
        resource: 'my-evals-escda',
        breadcrumb: [
          {
            text: "Mes Evaluations ESCDA",
            active: true,
          },
        ],
      },
    },
    {
      path: '/liste-escda',
      name: 'liste-escda',
      component: () => import('@/views/escda/evalsListe.vue'),
      meta: {
        resource: 'evals-escda',
        breadcrumb: [
          {
            text: "Liste Evaluations ESCDA",
            active: true,
          },
        ],
      },
    },
    {
      path: '/eval-escda-details/:id',
      name: 'eval-escda-details',
      component: () => import('@/views/escda/evalDetails.vue'),
      meta: {
        resource: 'my-esda-details',
        breadcrumb: [
          {
            text: "Liste Evaluations ESCDA",
            active: true,
          },
        ],
      },
    },


  ]
