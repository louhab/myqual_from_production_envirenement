export default [
  {
    path: '/evaluer',
    name: 'evaluation',
    component: () => import('@/views/evaluation/eval.vue'),
    meta: {
      resource: 'eval',
      pageTitle: 'Évaluer',
      breadcrumb: [
        {
          text: 'Évaluations',
          active: true,
        },
      ],
    },
  },
  {
    path: '/evaluations',
    name: 'liste-evaluation',
    component: () => import('@/views/evaluation/evalsListe.vue'),
    meta: {
      resource: 'liste-eval',
      pageTitle: 'Liste Des Évaluations',
      breadcrumb: [
        {
          text: 'Évaluations',
          active: true,
        },
      ],
    },
  },
  {
    path: '/evaluation/:id',
    name: 'eval-details',
    component: () => import('@/views/evaluation/evalDetails.vue'),
    meta: {
      resource: 'eval-edit',
      breadcrumb: [
        {
          text: 'Listes des évaluations',
          to: '/evaluations',
        },
        {
          text: 'Évaluations',
          active: true,
        },
      ],
    },
  },
  {
    path: '/mes-evaluations',
    name: 'my-evals',
    component: () => import('@/views/evaluation/MyEvals.vue'),
    meta: {
      resource: 'myevals',
      breadcrumb: [
        {
          text: 'Mes Évaluations',
          active: true,
        },
      ],
    },
  },
  {
    path: '/myeval-agent',
    name: 'myeval-agent',
    component: () => import('@/views/evaluation/EvalAgent.vue'),
    meta: {
      resource: 'myeval-agent',
    },
  },
]
