export default [
    {
      path: '/documents',
      name: 'documents',
      component: () => import('@/views/ressource/List.vue'),
      meta: {
        resource: 'doc-list',
        breadcrumb: [
          {
            text: 'documents',
            active: true,
          },
        ],
      },
    },
    {
      path: '/ajouter-doc',
      name: 'add-doc',
      component: () => import('@/views/ressource/AddResource.vue'),
      meta: {
        resource: 'doc-add',
        breadcrumb: [
          {
            text: 'documents',
            active: true,
          },
          {
            text: 'ajouter un doc',
            active: true,
          },
        ],
      },
    },
    {
      path: '/doc/:id',
      name: 'view-doc',
      component: () => import('@/views/ressource/ResourceViewer.vue'),
      meta: {
        resource: 'doc-view',
        breadcrumb: [
          {
            text: 'documents',
            active: true,
          },
          {
            text: 'consulter un doc',
            active: true,
          },
        ],
      },
    },
  ]
