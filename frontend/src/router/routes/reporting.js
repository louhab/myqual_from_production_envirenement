export default [
  {
    path: '/reporting/global',
    name: 'reporting-global',
    component: () => import('@/views/reporting/Global.vue'),
    meta: {
      resource: 'reporting-g',
      pageTitle: 'Reporting Global',
      breadcrumb: [
        {
          text: 'Reporting',
          active: true,
        },
      ],
    },
  },
  {
    path: '/reporting/activity',
    name: 'reporting-activity',
    component: () => import('@/views/reporting/Activity.vue'),
    meta: {
      resource: 'reporting-a',
      pageTitle: '',
      breadcrumb: [
        {
          text: 'Reporting',
          active: true,
        },
        {
          text: 'Par Activité',
          active: true,
        },
      ],
    },
  },
  {
    path: '/reporting/service',
    name: 'reporting-service',
    component: () => import('@/views/reporting/Service.vue'),
    meta: {
      resource: 'reporting-s',
      pageTitle: '',
      breadcrumb: [
        {
          text: 'Reporting',
          active: true,
        },
        {
          text: 'Par Service',
          active: true,
        },
      ],
    },
  },
  {
    path: '/reporting/agent',
    name: 'reporting-agent',
    component: () => import('@/views/reporting/Teleconseiller.vue'),
    meta: {
      resource: 'reporting-t',
      pageTitle: '',
      breadcrumb: [
        {
          text: 'Reporting',
          active: true,
        },
        {
          text: 'Par Téléconseiller',
          active: true,
        },
      ],
    },
  },
  {
    path: '/reporting/evaluateur',
    name: 'reporting-evaluateur',
    component: () => import('@/views/reporting/Evaluateur.vue'),
    meta: {
      resource: 'reporting-s',
      pageTitle: '',
      breadcrumb: [
        {
          text: 'Reporting',
          active: true,
        },
        {
          text: 'Par Evaluateur',
          active: true,
        },
      ],
    },
  },
  {
    path: '/reporting/Item',
    name: 'reporting-item',
    component: () => import('@/views/reporting/ReportingParItem.vue'),
    meta: {
      resource: 'reporting-item',
      pageTitle: '',
      breadcrumb: [
        {
          text: 'Reporting',
          active: true,
        },
        {
          text: 'Par Item',
          active: true,
        },
      ],
    },
  },
  {
    path: '/reporting/calibrage/evaluateur',
    name: 'reporting-calib-evaluateur',
    component: () => import('@/views/reprtingCalib/Evaluateur.vue'),
    meta: {
      resource: 'reporting-calib',
      pageTitle: '',
      breadcrumb: [
        {
          text: 'Reporting calibrage',
          active: true,
        },
        {
          text: 'Reporting par evaluateur',
          active: true,
        },
      ],
    },
  },
  {
    path: '/reporting/item-reporting',
    name: 'item-reporting',
    component: () => import('@/views/reporting/ItemReporting.vue'),
    meta: {
      resource: 'reporting-item',
      pageTitle: '',
      breadcrumb: [
        {
          text: 'Reporting par item',
          active: true,
        },
        {
          text: 'Liste des agents impactés par item',
          active: true,
        },
      ],
    },
  },
]
