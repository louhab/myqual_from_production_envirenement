export default [
    {
      path: '/elu-escda',
      name: 'elu-escda',
      component: () => import('@/views/eluescda/Escda.vue'),
      meta: {
        resource: 'elu-escda',
        breadcrumb: [
          {
            text: "L'ÉLECTION DU SERVICE CLIENT DE L'ANNÉE ",
            active: true,
          },
        ],
      },
    },

    {
      path: '/myevals-elu-escda',
      name: 'myevals-elu-escda',
      component: () => import('@/views/eluescda/MyEvals.vue'),
      meta: {
        resource: 'my-evals-elu-escda',
        breadcrumb: [
          {
            text: "Mes Evaluations ESCDA",
            active: true,
          },
        ],
      },
    },
    {
      path: '/liste-elu-escda',
      name: 'liste-elu-escda',
      component: () => import('@/views/eluescda/evalsListe.vue'),
      meta: {
        resource: 'evals-elu-escda',
        breadcrumb: [
          {
            text: "Liste Evaluations ESCDA",
            active: true,
          },
        ],
      },
    },
    {
      path: '/eval-elu-escda-details/:id',
      name: 'eval-elu-escda-details',
      component: () => import('@/views/eluescda/evalDetails.vue'),
      meta: {
        resource: 'my-evals-elu-escda',
        breadcrumb: [
          {
            text: "Liste Evaluations ESCDA",
            active: true,
          },
        ],
      },
    },


  ]
