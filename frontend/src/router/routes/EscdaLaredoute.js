export default [
    {
      path: '/escda-la-redoute',
      name: 'escda-la-redoute',
      component: () => import('@/views/EscdalaRdoute/Escda.vue'),
      meta: {
        resource: 'escda',
        breadcrumb: [
          {
            text: "L'ÉLECTION DU SERVICE CLIENT DE L'ANNÉE ",
            active: true,
          },
        ],
      },
    },

    {
      path: '/myevals-escda-la-redoute',
      name: 'myevals-escda-la-redoute',
      component: () => import('@/views/EscdalaRdoute/MyEvals.vue'),
      meta: {
        resource: 'my-evals-escda',
        breadcrumb: [
          {
            text: "Mes Evaluations ESCDA",
            active: true,
          },
        ],
      },
    },
    {
      path: '/liste-escda-la-redouteescda-la-redoute',
      name: 'liste-escda-la-redoute',
      component: () => import('@/views/EscdalaRdoute/evalsListe.vue'),
      meta: {
        resource: 'evals-escda',
        breadcrumb: [
          {
            text: "Liste Evaluations ESCDA",
            active: true,
          },
        ],
      },
    },
    {
      path: '/eval-escda-details-la-redoute/:id',
      name: 'eval-escda-details-la-redoute',
      component: () => import('@/views/EscdalaRdoute/evalDetails.vue'),
      meta: {
        resource: 'my-esda-details',
        breadcrumb: [
          {
            text: "Liste Evaluations ESCDA",
            active: true,
          },
        ],
      },
    },


  ]
