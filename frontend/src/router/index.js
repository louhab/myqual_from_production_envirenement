import Vue from 'vue'
/* eslint-disable no-unused-vars */
import VueRouter from 'vue-router'
import { isUserLoggedIn, getUserData, getHomeRouteForLoggedInUser } from '@/auth/utils'
import { canNavigate } from '@/libs/acl/routeProtection'
import activity from './routes/activity'
import grille from './routes/grille'
import evaluation from './routes/eval'
import reporting from './routes/reporting'
import agent from './routes/agent'
import users from './routes/users'
import resource from './routes/resource'
import tasks from './routes/tasks'
import calibrage from './routes/calibrage'
import escda from './routes/escda'
import eluescda from './routes/eluescda'
import constat from './routes/constat'
import reportingConstat from './routes/reportingConstat'
import EscdaLaredoute  from './routes/EscdaLaredoute'
import CalibrageVersion2 from './routes/calibVersion2'


Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior() {
    return { x: 0, y: 0 }
  },
  routes: [
    {
      path: '/',
        name: 'home',
      component: () => import('@/views/Home.vue'),
      meta: {
        resource: 'homeadmin',
        pageTitle: 'Accueil',
        breadcrumb: [
          {
            text: 'Accueil',
            active: true,
          },
        ],
      },
    },
    {
      path: '/espace-qualiticien',
      name: 'espace-qualiticien',
      component: () => import('@/views/HomeQualiticien.vue'),
      meta: {
        resource: 'homequaliticien',
      },
    },
    {
      path: '/espace-client',
      name: 'espace-client',
      component: () => import('@/views/HomeClient.vue'),
      meta: {
        resource: 'homeclient',
      },
    },
    {
      path: '/espace-superviseur',
      name: 'espace-sup',
      component: () => import('@/views/HomeSup.vue'),
      meta: {
        resource: 'homesup',
      },
    },
    {
      path: '/espace-agent',
      name: 'espace-agent',
      component: () => import('@/views/HomeAgent.vue'),
      meta: {
        resource: 'homeagent',
      },
    },
    {
      path: '/test',
      name: 'test',
      component: () => import('@/views/ressource/List.vue'),
      meta: {
        pageTitle: 'Test Page',
        resource: 'Auth',
        breadcrumb: [
          {
            text: 'test page',
            active: true,
          },
        ],
      },
    },
    {
        path: '/testshow/:id',
        name: 'test',
        component: () => import('@/views/ressource/ResourceViewer.vue'),
        meta: {
          pageTitle: 'Test Page',
          resource: 'Auth',
          breadcrumb: [
            {
              text: 'test page',
              active: true,
            },
          ],
        },
      },
    {
      path: '/error/not-authorized',
      name: 'not-authorized',
      // ! Update import path
      component: () => import('@/views/NotAuthorized.vue'),
      meta: {
        layout: 'full',
        action: 'read',
        resource: 'Auth',
      },
    },
    {
      path: '/second-page',
      name: 'second-page',
      component: () => import('@/views/SecondPage.vue'),
      meta: {
        resource: 'basic',
        pageTitle: 'Second Page',
        breadcrumb: [
          {
            text: 'Second Page',
            active: true,
          },
        ],
      },
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('@/views/Login.vue'),
      meta: {
        layout: 'full',
        action: 'read',
        resource: 'Auth',
        redirectIfLoggedIn: true,
      },
    },
    {
      path: '/error-404',
      name: 'error-404',
      component: () => import('@/views/error/Error404.vue'),
      meta: {
        layout: 'full',
        action: 'read',
        resource: 'Auth',
      },
    },
    {
      path: '*',
      redirect: 'error-404',
    },
    ...activity,
    ...grille,
    ...evaluation,
    ...reporting,
    ...agent,
    ...users,
    ...resource,
    ...tasks,
    ...calibrage,
    ...escda,
    ...constat,
    ...reportingConstat,
    ...eluescda,
    ...EscdaLaredoute,
    ...CalibrageVersion2,
 
  ],
})

/*
router.beforeEach((to, from, next) => {
  const isLoggedIn = isUserLoggedIn()
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (isLoggedIn == null) {
      next({
        path: '/login',
      })
    } else {
      next()
    }
  } else {
    next()
  }
}) */

router.beforeEach((to, _, next) => {
  const isLoggedIn = isUserLoggedIn()
  const userData = getUserData()
  if (!canNavigate(to)) {
    // Redirect to login if not logged in
    if (!isLoggedIn) return next({ name: 'login' })

    // If logged in => not authorized
    return next(getHomeRouteForLoggedInUser(userData ? userData.role : null)) // next({ name: 'not-authorized' })
  }

  // Redirect if logged in
  if (to.meta.redirectIfLoggedIn && isLoggedIn) {
    next(getHomeRouteForLoggedInUser(userData ? userData.role : null))
  }

  return next()
})

export default router
