import Vue from 'vue'
import { ToastPlugin, ModalPlugin } from 'bootstrap-vue'
import VueCompositionAPI from '@vue/composition-api'

import router from './router'
import store from './store'
import App from './App.vue'
import CKEditor from 'ckeditor4-vue'
import { QuasarTiptapPlugin, RecommendedExtensions } from 'quasar-tiptap'



// Global Components
import './global-components'

// 3rd party plugins
import '@/libs/portal-vue'
import '@/libs/toastification'
import '@/libs/axios'
import '@/libs/vue-select'
import '@/libs/sweet-alerts'
import '@/libs/vue-html2pdf'
import '@/libs/acl'
Vue.component('editor', require('./views/Editor'));
// BSV Plugin Registration
Vue.use(ToastPlugin)
Vue.use(ModalPlugin)

// Composition API
Vue.use(VueCompositionAPI)

// import core styles
require('@core/scss/core.scss')

// import assets styles
require('@/assets/scss/style.scss')


Vue.use(CKEditor)

Vue.config.productionTip = true

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
