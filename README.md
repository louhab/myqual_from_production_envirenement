Myqual

------------------
Deploy Role&Permissions:

            - git stash && git pull
            - composer install to install the package.
            - check if php sodium ext is activated.
            - run migration.
            -  create roles and permission with the script.
            - move all qualiticien and admins to users table with the script ( two separated
               function / role will be assign automatically ).
            - php artisan passport:keys
            - npm run dev/prod
            - php artisan optimize::clear // optimize
            - check if everything is working if not update vendor/spatie/laravel-
              permission/src/Middlewares/RoleMiddleware.php
             add this lines at the beginning
             if(!$guard) {
                      $guard = 'api';
            } 
